<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class NivelService {

    var $conexion;

    function NivelService() {
        $this->conexion = new Datasource();
    }
    

    public function listarNivel($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $nivel_dao = new NivelDao();
        $lista_nivel = array();
        if (trim($object["nombre_nivel"]) != "")
            $filtro .= " AND n.nombre_nivel LIKE '%{$object["nombre_nivel"]}%' ";
        if (trim($object["id_nivel"]) != "")
            $filtro .= " AND n.id_nivel = {$object["id_nivel"]} ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT n.* FROM nivel n WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {

                $temp = array();
                $temp["idNivel"] = $row[0];
                $temp["nombreNivel"] = $row[1];
                $lista_nivel[] = $temp;
            }
        } else {
            $lista_nivel = $nivel_dao->loadAll($this->conexion);
        }

        //throw new Exception(print_r($lista_nivel, true));

        if (count($lista_nivel) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_nivel));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Nivel";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarNivel($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $nivel_dao = new NivelDao();
        $nivel = new Nivel();

        $nivel->setAll($object["id_nivel"], $object["nombre_nivel"]);

        if (trim($actualiza) == "true") {
            $result = $nivel_dao->save($this->conexion, $nivel);
            $mensaje = " El nivel fue Actualizada";
        } else if ($nivel_dao->load($this->conexion, $nivel)) {
            $return->success = false;
            $return->errorMessage = " Error el nivel ya se encuentra Creada";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $nivel_dao->create($this->conexion, $nivel);
            $mensaje = " El nivel fue Creada";
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el nivel";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarNivel($object) {                
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $nivel_dao = new NivelDao();
        $nivel = new Nivel();

        $nivel = $nivel_dao->getObject($this->conexion, $object["id_nivel"]);
        $result = $nivel_dao->delete($this->conexion, $nivel);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " El nivel fue eliminada del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el nivel";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE NivelService
//-------------------------------------------------
?>
