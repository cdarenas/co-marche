<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class JornadaService {

    var $conexion;

    function JornadaService() {
        $this->conexion = new Datasource();
    }

    public function listarJornada($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $jornada_dao = new JornadaDao();
        $lista_jornada = array();
        if (trim($object["nombre_jornada"]) != "")
            $filtro .= " AND j.nombre_jornada LIKE '%{$object["nombre_jornada"]}%' ";
        if (trim($object["id_jornada"]) != "")
            $filtro .= " AND j.id_jornada = {$object["id_jornada"]} ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT j.* FROM jornada j WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {

                $temp = array();
                $temp["idJornada"] = $row[0];
                $temp["nombreJornada"] = $row[1];
                $lista_jornada[] = $temp;
            }
        } else {
            $lista_jornada = $jornada_dao->loadAll($this->conexion);
        }

        //throw new Exception(print_r($lista_jornada, true));

        if (count($lista_jornada) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_jornada));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Jornada";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarJornada($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $jornada_dao = new JornadaDao();
        $jornada = new Jornada();

        $jornada->setAll($object["id_jornada"], $object["nombre_jornada"]);

        if (trim($actualiza) == "true") {
            $result = $jornada_dao->save($this->conexion, $jornada);
            $mensaje = " La jornada fue Actualizada";
        } else if ($jornada_dao->load($this->conexion, $jornada)) {
            $return->success = false;
            $return->errorMessage = " Error el jornada ya se encuentra Creada";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $jornada_dao->create($this->conexion, $jornada);
            $mensaje = " La jornada fue Creada";
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la jornada";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarJornada($object) {                
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $jornada_dao = new JornadaDao();
        $jornada = new Jornada();

        $jornada = $jornada_dao->getObject($this->conexion, $object["id_jornada"]);
        $result = $jornada_dao->delete($this->conexion, $jornada);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " La jornada fue eliminada del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la jornada";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE JornadaService
//-------------------------------------------------
?>
