<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class DocumentoService {

    var $conexion;

    function DocumentoService() {
        $this->conexion = new Datasource();
    }

    public function listarDocumento($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $documento_dao = new DocumentoDao();
        $lista_documento = array();
        if (trim($object["nombre_documento"]) != "")
            $filtro .= " AND n.nombre_documento LIKE '%{$object["nombre_documento"]}%' ";
        if (trim($object["id_documento"]) != "")
            $filtro .= " AND n.id_documento = {$object["id_documento"]} ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT n.* FROM documento n WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {

                $temp = array();
                $temp["idDocumento"] = $row[0];
                $temp["nombreDocumento"] = $row[1];
                $temp["tamanio"] = $row[2];
                $temp["fechaRegistro"] = $row[4];
                $temp["extension"] = $row[5];
                $lista_documento[] = $temp;
            }
        } else {
            $lista_documento = $documento_dao->loadAll($this->conexion);
        }

        //throw new Exception(print_r($lista_documento, true));

        if (count($lista_documento) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_documento));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Documento";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarDocumento($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $documento_dao = new DocumentoDao();
        $documento = new Documento();
        $carpeta = FILE_PATH_CARGADOS . "archivos_cargados";

        if (!file_exists($carpeta)) {
            mkdir($carpeta);
        }

        $carpeta = FILE_PATH_CARGADOS . "archivos_cargados/" . date("Ydm");

        if (!file_exists($carpeta)) {
            mkdir($carpeta);
        }

        foreach ($object["archivos"] as $key => $archivo) {
            if ($archivo["typeFile"] == "link"){
                $result = $documento_dao->save($this->conexion, $documento);
                    $mensaje .= "\n El documento {{$archivo["filename"]} fue Actualizado";
                continue;
            }
            $ruta = "$carpeta/{$archivo["filename"]}";
            $bytes = file_put_contents($ruta, base64_decode($archivo["bytes64"]));
            if ($bytes !== false && file_exists($ruta)) {
                $info = pathinfo($ruta);
                $archivo["lastModified"] = str_replace("/", "-", $archivo["lastModified"]);
                $archivo["lastModified"] = strtotime($archivo["lastModified"]);
                $archivo["lastModified"] = date("Y-m-d", $archivo["lastModified"]);
                $continue = false;
                $documento->setAll($object["id_documento"], $archivo["filename"], $archivo["size"], $ruta, $archivo["lastModified"], $info['extension']);

                $result = $this->conexion->execute("SELECT d.* FROM documento d WHERE d.nombre_documento = '{$archivo["filename"]}' and d.ruta = '$ruta'");
                while ($row = $this->conexion->nextRow($result) && trim($actualiza) != "true") {
                    $mensaje .= "\n El documento {$archivo["filename"]} fue Actualizado";
                    $continue = true;
                    break;
                }

                if ($continue) {
                    continue;
                }

                if (trim($actualiza) == "true") {
                    $result = $documento_dao->save($this->conexion, $documento);
                    $mensaje .= "\n El documento {$archivo["filename"]} fue Actualizado";
                } else {
                    $result = $documento_dao->create($this->conexion, $documento);
                    $mensaje .= "\n El documento {$archivo["filename"]} fue Creado";
                }
            }
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el documento";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarDocumentoInterno($object) {

        $result = false;
        $return = array();
        $documento_dao = new DocumentoDao();
        $documento = new Documento();
        $carpeta = FILE_PATH_CARGADOS . "archivos_cargados";

        if (!file_exists($carpeta)) {
            mkdir($carpeta);
        }

        $carpeta = FILE_PATH_CARGADOS . "archivos_cargados/" . date("Ydm");

        if (!file_exists($carpeta)) {
            mkdir($carpeta);
        }

        foreach ($object as $key => $archivo) {
            if ($archivo["typeFile"] == "link")
                continue;

            $ruta = "$carpeta/{$archivo["filename"]}";
            $bytes = file_put_contents($ruta, base64_decode($archivo["bytes64"]));

            if ($bytes !== false && file_exists($ruta)) {
                $info = pathinfo($ruta);
                $archivo["lastModified"] = str_replace("/", "-", $archivo["lastModified"]);
                $archivo["lastModified"] = strtotime($archivo["lastModified"]);
                $archivo["lastModified"] = date("Y-m-d", $archivo["lastModified"]);
                $continue = false;
                $documento->setAll("", $archivo["filename"], $archivo["size"], $ruta, $archivo["lastModified"], $info['extension']);

                $result = $this->conexion->execute("SELECT d.* FROM documento d WHERE d.nombre_documento = '{$archivo["filename"]}' and d.ruta = '$ruta'");
                while ($row = $this->conexion->nextRow($result)) {
                    $return[] = $row["id_documento"];
                    $continue = true;
                    break;
                }

                if ($continue) {
                    continue;
                }

                $result = $documento_dao->create($this->conexion, $documento);

                $result2 = $this->conexion->execute("SELECT LAST_INSERT_ID() as id_documento;");
                $row = $this->conexion->nextRow($result2);
                $return[] = $row["id_documento"];
            }
        }

        return $return;
    }

    public function eliminarDocumento($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $documento_dao = new DocumentoDao();
        $documento = new Documento();

        $result1 = $this->conexion->execute("SELECT d.ruta FROM documento d  WHERE d.id_documento = {$object["id_documento"]}");
        while ($row = $this->conexion->nextRow($result1)) {
            if (file_exists($row[0])) {
                unlink($row[0]);
            }
        }

        $documento = $documento_dao->getObject($this->conexion, $object["id_documento"]);
        $result = $documento_dao->delete($this->conexion, $documento);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " El documento fue eliminada del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el documento";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE DocumentoService
//-------------------------------------------------
?>
