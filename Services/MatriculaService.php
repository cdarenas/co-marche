<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class MatriculaService {

    var $conexion;

    function MatriculaService() {
        $this->conexion = new Datasource();
    }

    public function listarMatricula($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $matricula_dao = new MatriculaDao();
        $lista_matricula = array();
        if (trim($object["fecha_creacion"]) != "")
            $filtro .= " AND m.fecha_registro = '{$object["fecha_creacion"]}' ";
        if (trim($object["id_matricula"]) != "")
            $filtro .= " AND m.id_matricula = {$object["id_matricula"]} ";
        if (trim($object["id_cupo"]) != "")
            $filtro .= " AND m.id_cupo = '{$object["id_cupo"]}' ";
        if (trim($object["id_colegio"]) != "")
            $filtro .= " AND m.id_colegio = '{$object["id_colegio"]}' ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT m.*"
                    . ", CONCAT(us.nombres, ' ', us.apellidos) as alumno"
                    . ", CONCAT('Cod Cupo ', c.id_cupo) as cupo "
                    . ", n.nombre_nivel as nivel "
                    . ", j.nombre_jornada as jornada "
                    . ", s.nombre_sede as sede "
                    . ", col.nombre_colegio as colegio "
                    . "FROM matricula m "
                    . "LEFT JOIN cupo c ON m.id_cupo = c.id_cupo "
                    . "LEFT JOIN nivel n ON m.id_nivel = n.id_nivel "
                    . "LEFT JOIN jornada j ON m.id_jornada = j.id_jornada "
                    . "LEFT JOIN sede s ON m.id_sede = s.id_sede "
                    . "LEFT JOIN usuario us ON c.id_alumno = us.cedula "
                    . "LEFT JOIN colegio col ON m.id_colegio = col.id_colegio "
                    . "WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {
                $temp = array();
                $temp["idMatricula"] = $row[0];
                $temp["fechaRegistro"] = $row[1];
                $temp["idColegio"] = $row[2];
                $temp["idNivel"] = $row[3];
                $temp["idSede"] = $row[4];
                $temp["idJornada"] = $row[5];
                $temp["idCupo"] = $row[6];
                $temp["alumno"] = $row[7];
                $temp["cupo"] = $row[8];
                $temp["nivel"] = $row[9];
                $temp["jornada"] = $row[10];
                $temp["sede"] = $row[11];
                $temp["colegio"] = $row[12];

                $lista_matricula[] = $temp;
            }
        } else {
            $lista_matricula = $matricula_dao->loadAll($this->conexion);
        }

        //throw new Exception(print_r($lista_matricula, true));

        if (count($lista_matricula) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_matricula));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Matricula";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarMatricula($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $matricula_dao = new MatriculaDao();
        $matricula = new Matricula();

        $result = $this->conexion->execute("SELECT m.* FROM matricula m WHERE m.id_cupo = {$object["cupo"]}");
        while ($row = $this->conexion->nextRow($result) && trim($actualiza) != "true") {
            $mensaje .= "\n La Matricula con el nro de Cupo {$object["cupo"]} se encuentra ya Creado";
            $continue = true;
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
            return json_encode($return);
        }

        $matricula->setAll($object["id_matricula"], date("Y-m-d"), $object["colegio"], $object["nivel"], $object["sede"], $object["jornada"], $object["cupo"]);

        if (trim($actualiza) == "true") {
            $result = $matricula_dao->save($this->conexion, $matricula);
            $mensaje = " La matricula fue Actualizado";
        } else if ($matricula_dao->load($this->conexion, $matricula)) {
            $return->success = false;
            $return->errorMessage = " Error el matricula ya se encuentra Creada";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $matricula_dao->create($this->conexion, $matricula);
            $mensaje = " La matricula fue Creada";
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la matricula";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarMatricula($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $matricula_dao = new MatriculaDao();
        $matricula = new Matricula();

        $matricula = $matricula_dao->getObject($this->conexion, $object["id_matricula"]);
        $result = $matricula_dao->delete($this->conexion, $matricula);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " La Matricula fue eliminada del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de eliminar la matricula";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE MatriculaService
//-------------------------------------------------
?>
