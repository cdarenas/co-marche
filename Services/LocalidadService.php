<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class LocalidadService {

    var $conexion;

    function LocalidadService() {
        $this->conexion = new Datasource();
    }
    
    public function listarLocalidad($id_ciudad) {
        $return = new stdClass();
        $localidad_dao = new LocalidadDao();
        $localidad = new Localidad();
        $localidad->setAll(0, "", $id_ciudad);
        $lista_localidad = $localidad_dao->searchMatching($this->conexion, $localidad);


        if (count($lista_localidad) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_localidad));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar pefiles";
            $return->data = array();
        }

        return json_encode($return);
    }
   
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE LocalidadService
//-------------------------------------------------
?>
