<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class SedeService {

    var $conexion;

    function SedeService() {
        $this->conexion = new Datasource();
    }
    
    public function listarSede($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $sede_dao = new SedeDao();
        $lista_sede = array();
        if (trim($object["nombre_sede"]) != "")
            $filtro .= " AND s.nombre_sede LIKE '%{$object["nombre_sede"]}%' ";
        if (trim($object["id_sede"]) != "")
            $filtro .= " AND s.id_sede = {$object["id_sede"]} ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT s.* FROM sede s WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {

                $temp = array();
                $temp["idSede"] = $row[0];
                $temp["nombreSede"] = $row[1];
                $lista_sede[] = $temp;
            }
        } else {
            $lista_sede = $sede_dao->loadAll($this->conexion);
        }

        //throw new Exception(print_r($lista_sede, true));

        if (count($lista_sede) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_sede));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Sede";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarSede($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $sede_dao = new SedeDao();
        $sede = new Sede();

        $sede->setAll($object["id_sede"], $object["nombre_sede"]);

        if (trim($actualiza) == "true") {
            $result = $sede_dao->save($this->conexion, $sede);
            $mensaje = " La sede fue Actualizada";
        } else if ($sede_dao->load($this->conexion, $sede)) {
            $return->success = false;
            $return->errorMessage = " Error el sede ya se encuentra Creada";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $sede_dao->create($this->conexion, $sede);
            $mensaje = " La sede fue Creada";
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la sede";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarSede($object) {                
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $sede_dao = new SedeDao();
        $sede = new Sede();

        $sede = $sede_dao->getObject($this->conexion, $object["id_sede"]);
        $result = $sede_dao->delete($this->conexion, $sede);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " La sede fue eliminada del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la sede";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE SedeService
//-------------------------------------------------
?>
