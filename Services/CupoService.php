<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';
require_once AMFPHP_ROOTPATH . 'Services/DocumentoService.php';

class CupoService {

    var $conexion;

    function CupoService() {
        $this->conexion = new Datasource();
    }

    public function listarCupo($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $cupo_dao = new CupoDao();
        $lista_cupo = array();
        if (trim($object["fecha_creacion"]) != "")
            $filtro .= " AND c.fecha_registro = '{$object["fecha_creacion"]}' ";
        if (trim($object["id_cupo"]) != "")
            $filtro .= " AND c.id_cupo = {$object["id_cupo"]} ";
        if (trim($object["cedula"]) != "")
            $filtro .= " AND c.id_alumno = '{$object["cedula"]}' ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT c.*"
                    . ", CONCAT(u.nombres, ' ', u.apellidos) as alumno"
                    . ", n.nombre_nivel as nivel "
                    . ", j.nombre_jornada as jornada "
                    . ", e.nombre_estado as estado "
                    . "FROM cupo c "
                    . "LEFT JOIN usuario u ON c.id_alumno = u.cedula "
                    . "LEFT JOIN nivel n ON c.id_nivel = n.id_nivel "
                    . "LEFT JOIN jornada j ON c.id_jornada = j.id_jornada "
                    . "LEFT JOIN estado e ON c.id_estado = e.id_estado "
                    . "WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {
                $documentos = array();
                $result = $this->conexion->execute("SELECT cd.id_documento, d.nombre_documento, d.tamanio, d.fecha_registro FROM cupodocumento cd, documento d  WHERE cd.id_documento = d.id_documento AND cd.id_cupo = {$row[0]}");
                while ($row2 = $this->conexion->nextRow($result)) {
                    $temp2 = array();
                    $temp2["id_documento"] = $row2[0];
                    $temp2["nombre_documento"] = $row2[1];
                    $temp2["size"] = $row2[2];
                    $temp2["fecha_registro"] = $row2[3];
                    $documentos[] = $temp2;
                }

                $temp = array();
                $temp["idCupo"] = $row[0];
                $temp["fechaRegistro"] = date("Y-m-d", strtotime($row[1]));
                $temp["idAlumno"] = $row[2];
                $temp["idNivel"] = $row[3];
                $temp["idJornada"] = $row[4];
                $temp["idEstado"] = $row[5];
                $temp["alumno"] = $row[6];
                $temp["nivel"] = $row[7];
                $temp["jornada"] = $row[8];
                $temp["estado"] = $row[9];
                $temp["documentos"] = $documentos;

                $lista_cupo[] = $temp;
            }
        } else {
            $lista_cupo = $cupo_dao->loadAll($this->conexion);                   
        }

        //throw new Exception(print_r($lista_cupo, true));

        if (count($lista_cupo) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_cupo));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Cupo";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarCupo($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $cupo_dao = new CupoDao();
        $cupo = new Cupo();
        $documento = new DocumentoService();


        $result = $this->conexion->execute("SELECT c.* FROM cupo c WHERE c.id_alumno = {$object["alumno"]} and c.id_estado = 3");
        while ($row = $this->conexion->nextRow($result)) {
            $mensaje .= "\n El Cupo con el documento {$object["alumno"]} se encuentra ya Creado";
            $continue = true;
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
            return json_encode($return);
        }

        $documentos = implode(",", $documento->gestionarDocumentoInterno($object["archivos"]));
        $cupo->setAll($object["id_cupo"], date("Y-m-d"), $object["alumno"], $object["nivel"], $object["jornada"], $object["estado"]);

        if (trim($actualiza) == "true") {
            $result = $cupo_dao->save($this->conexion, $cupo);
            $mensaje = " La cupo fue Actualizado";

            if (stristr($documentos, ",")) {
                $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM cupodocumento where id_cupo = {$object["id_cupo"]}");
                $cupo_dao->databaseUpdate($this->conexion, "INSERT INTO cupodocumento (id_cupo, id_documento) "
                        . " SELECT {$object["id_cupo"]}, id_documento FROM documento where id_documento in ({$documentos});");
            }
        } else if ($cupo_dao->load($this->conexion, $cupo)) {
            $return->success = false;
            $return->errorMessage = " Error el cupo ya se encuentra Creada";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $cupo_dao->create($this->conexion, $cupo);
            $mensaje = " La cupo fue Creado";

            if (stristr($documentos, ",")) {
                $result2 = $this->conexion->execute("SELECT LAST_INSERT_ID() as id_cupo;");
                $row = $this->conexion->nextRow($result2);

                $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM cupodocumento where id_cupo = {$row["id_cupo"]}");
                $cupo_dao->databaseUpdate($this->conexion, "INSERT INTO cupodocumento (id_cupo, id_documento) "
                        . " SELECT {$row["id_cupo"]}, id_documento FROM documento where id_documento in ({$documentos});");
            }
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la cupo";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarCupo($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $cupo_dao = new CupoDao();
        $cupo = new Cupo();

        $cupo = $cupo_dao->getObject($this->conexion, $object["id_cupo"]);
        $result = $cupo_dao->delete($this->conexion, $cupo);

        $lista_documentos = array();
        $documentos = array();
        $result = $this->conexion->execute("SELECT cd.id_documento, d.ruta FROM cupodocumento cd, documento d  WHERE cd.id_documento = d.id_documento AND cd.id_cupo = {$object["id_cupo"]}");
        while ($row = $this->conexion->nextRow($result)) {
            $lista_documentos[] = $row[0];
            $documentos[] = $row[1];
        }

        $id_documentos = implode(",", $lista_documentos);
        if (count($lista_documentos) > 0) {
            $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM cupodocumento where id_cupo = {$object["id_cupo"]}");
            $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM documento where id_documento in ({$id_documentos})");

            foreach ($documentos as $i => $documento) {
                if (file_exists($documento)) {
                    unlink($documento);
                }
            }
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = " El documento fue eliminado del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la cupo";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarDocumentoCupo($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $cupo_dao = new CupoDao();

        $documentos = array();
        $result = $this->conexion->execute("SELECT d.ruta FROM documento d  WHERE d.id_documento = {$object["id_documento"]}");
        while ($row = $this->conexion->nextRow($result)) {
            $documentos[] = $row[0];
        }

        if (count($documentos) > 0) {
            $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM cupodocumento where id_documento = {$object["id_documento"]}");
            $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM documento where id_documento = {$object["id_documento"]}");

            foreach ($documentos as $i => $documento) {
                if (file_exists($documento)) {
                    unlink($documento);
                }
            }
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = " La cupo fue eliminada del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar la cupo";
            $return->data = array();
        }

        return json_encode($return);
    }

    
    public function gestionarCupoInterno($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $cupo_dao = new CupoDao();
        $cupo = new Cupo();
        $documento = new DocumentoService();


        $result = $this->conexion->execute("SELECT c.* FROM cupo c WHERE c.id_alumno = {$object["alumno"]} and c.id_estado = 3");
        while ($row = $this->conexion->nextRow($result)) {
            return false;
        }

        $documentos = implode(",", $documento->gestionarDocumentoInterno($object["archivos"]));
        $cupo->setAll($object["id_cupo"], date("Y-m-d"), $object["alumno"], $object["nivel"], $object["jornada"], $object["estado"]);

        if (trim($actualiza) == "true") {
            $result = $cupo_dao->save($this->conexion, $cupo);
            $mensaje = " La cupo fue Actualizado";

            if (stristr($documentos, ",")) {
                $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM cupodocumento where id_cupo = {$object["id_cupo"]}");
                $cupo_dao->databaseUpdate($this->conexion, "INSERT INTO cupodocumento (id_cupo, id_documento) "
                        . " SELECT {$object["id_cupo"]}, id_documento FROM documento where id_documento in ({$documentos});");
            }
        } else if ($cupo_dao->load($this->conexion, $cupo)) {
            return false;
        } else {
            $result = $cupo_dao->create($this->conexion, $cupo);
            $mensaje = " La cupo fue Creado";

            if (stristr($documentos, ",")) {
                $result2 = $this->conexion->execute("SELECT LAST_INSERT_ID() as id_cupo;");
                $row = $this->conexion->nextRow($result2);

                $cupo_dao->databaseUpdate($this->conexion, "DELETE FROM cupodocumento where id_cupo = {$row["id_cupo"]}");
                $cupo_dao->databaseUpdate($this->conexion, "INSERT INTO cupodocumento (id_cupo, id_documento) "
                        . " SELECT {$row["id_cupo"]}, id_documento FROM documento where id_documento in ({$documentos});");
            }
        }

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE CupoService
//-------------------------------------------------
?>
