<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class ParentescoService {

    var $conexion;

    function ParentescoService() {
        $this->conexion = new Datasource();
    }
    
    public function listarParentesco() {
        $return = new stdClass();
        $parentesco_dao = new ParentescoDao();
        $lista_parentesco = $parentesco_dao->loadAll($this->conexion);

        //throw new Exception(print_r($lista_parentesco, true));
        
        if (count($lista_parentesco) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_parentesco));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar parentesco ";
            $return->data = array();
        }

        return json_encode($return);
    }
   
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE ParentescoService
//-------------------------------------------------
?>
