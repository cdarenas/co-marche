<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class TipoDocumentoService {

    var $conexion;

    function TipoDocumentoService() {
        $this->conexion = new Datasource();
    }
    
    public function listarTipoDocumento() {
        $return = new stdClass();
        $tipo_documento_dao = new TipoDocumentoDao();
        $lista_tipo_documento = $tipo_documento_dao->loadAll($this->conexion);

        //throw new Exception(print_r($lista_tipo_documento, true));
        
        if (count($lista_tipo_documento) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_tipo_documento));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar tipo documento";
            $return->data = array();
        }

        return json_encode($return);
    }
   
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE TipoDocumentoService
//-------------------------------------------------
?>
