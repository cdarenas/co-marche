<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';
require_once AMFPHP_ROOTPATH . 'Services/CupoService.php';

class UsuarioService {

    var $conexion;

    function UsuarioService() {
        $this->conexion = new Datasource();
    }

    public function registrarUsuarioAdmin($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $login_dao = new LoginDao();
        $usuario_dao = new UsuarioDao();
        $login = new Login();
        $usuario = new Usuario();

        $usuario->setAll($object["cedula"], $object["nombres"], $object["apellidos"], $object["telefono"], $object["direccion"], $object["email"], $object["fecha_nacimiento"], date("Y-m-d"), "", $object["genero"], $object["id_tipo_documento"], $object["id_barrio"]);

        if ($usuario_dao->load($this->conexion, $usuario)) {
            $return->success = false;
            $return->errorMessage = " Error el usuario ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        }

        $usuario_dao->create($this->conexion, $usuario);

        $login->setAll(0, $object["cedula"], $object["id_perfil"], $object["usuario"], $object["contrasena"], date("Y-m-d"), "", 2);
        $login_dao->create($this->conexion, $login);

        $login_dao->databaseUpdate($this->conexion, "INSERT INTO loginpermiso(id_login, id_permiso)
                (select l.id_login, pp.id_permiso
                       from perfilpermiso pp , login l
                        where pp.id_perfil = l.id_perfil
                        and l.usuario = '" . $object["usuario"] . "')");

        if (count($lista_login) > 0) {
            $return->success = true;
            $return->errorMessage = " El usuario fue creado, debe esperar mientras el sistema activa el Usuario";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el usuario";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarUsuario($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $login_dao = new LoginDao();
        $usuario_dao = new UsuarioDao();
        $login = new Login();
        $usuario = new Usuario();

        $usuario->setAll($object["cedula"], $object["nombres"], $object["apellidos"], $object["telefono"], $object["direccion"], $object["email"], $object["fecha_nacimiento"], date("Y-m-d"), "", $object["genero"], $object["id_tipo_documento"], $object["id_barrio"]);

        if (trim($actualiza) == "true") {
            $result = $usuario_dao->save($this->conexion, $usuario);
            $mensaje = " EL usuario fue Actualizado";
        } else if ($usuario_dao->load($this->conexion, $usuario)) {
            $return->success = false;
            $return->errorMessage = " Error el usuario ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $usuario_dao->create($this->conexion, $usuario);
            $mensaje = " EL usuario fue Creado";

            $login->setAll(0, $object["cedula"], $object["id_perfil"], $object["usuario"], $object["contrasena"], date("Y-m-d"), "", 1);
            $login_dao->create($this->conexion, $login);

            $login_dao->databaseUpdate($this->conexion, "INSERT INTO loginpermiso(id_login, id_permiso)
                (select l.id_login, pp.id_permiso
                       from perfilpermiso pp , login l
                        where pp.id_perfil = l.id_perfil
                        and l.usuario = '" . $object["usuario"] . "')");
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el usuario";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarUsuarioColegio($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $login_dao = new LoginDao();
        $usuario_dao = new UsuarioDao();
        $usuario_colegio_dao = new UsuarioColegioDao();
        $login = new Login();
        $usuario = new Usuario();
        $usuario_colegio = new UsuarioColegio();

        $usuario->setAll($object["cedula"], $object["nombres"], $object["apellidos"], $object["telefono"], $object["direccion"], $object["email"], $object["fecha_nacimiento"], date("Y-m-d"), "", $object["genero"], $object["id_tipo_documento"], $object["id_barrio"]);

        if (trim($actualiza) == "true") {
            $result = $usuario_dao->save($this->conexion, $usuario);
            $usuario_colegio_dao->databaseUpdate($this->conexion, "DELETE FROM usuariocolegio WHERE cedula = {$object["cedula"]};");

            $usuario_colegio->setAll($object["id_colegio"], $object["cedula"]);
            $usuario_colegio_dao->create($this->conexion, $usuario_colegio);
            $mensaje = " EL usuario colegio fue Actualizado";
        } else if ($usuario_dao->load($this->conexion, $usuario)) {
            $return->success = false;
            $return->errorMessage = " Error el usuario ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $usuario_dao->create($this->conexion, $usuario);
            $mensaje = " EL usuario colegio fue Creado";

            $login->setAll(0, $object["cedula"], 3, $object["usuario"], $object["contrasena"], date("Y-m-d"), "", 1);
            $login_dao->create($this->conexion, $login);

            $login_dao->databaseUpdate($this->conexion, "INSERT INTO loginpermiso(id_login, id_permiso)
                (select l.id_login, pp.id_permiso
                       from perfilpermiso pp , login l
                        where pp.id_perfil = l.id_perfil
                        and l.usuario = '" . $object["usuario"] . "')");

            $usuario_colegio->setAll($object["id_colegio"], $object["cedula"]);
            $usuario_colegio_dao->create($this->conexion, $usuario_colegio);
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el usuario";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarAlumno($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $acudiente_dao = new AcudienteDao();
        $usuario_dao = new UsuarioDao();
        $alumno_dao = new AlumnoDao();
        $acudiente = new Acudiente();
        $usuario = new Usuario();
        $alumno = new Alumno();

        $usuario->setAll($object["cedula"], $object["nombres"], $object["apellidos"], $object["telefono"], $object["direccion"], $object["email"], $object["fecha_nacimiento"], date("Y-m-d"), "", $object["genero"], $object["id_tipo_documento"], $object["id_barrio"]);

        if (trim($actualiza) == "true") {
            $result = $usuario_dao->save($this->conexion, $usuario);
            $alumno_dao->databaseUpdate($this->conexion, "DELETE FROM alumno WHERE cedula = {$object["cedula"]};");
            $acudiente_dao->databaseUpdate($this->conexion, "DELETE FROM acudiente WHERE id_alumno = {$object["cedula"]};");

            $alumno->setAll($object["cedula"], $object["ultimo_anio_cursado"], $object["id_nivel"]);
            $alumno_dao->create($this->conexion, $alumno);

            $acudiente->setAll($object["cedula_acudiente"], $object["cedula"], $object["id_parentesco"]);
            $acudiente_dao->create($this->conexion, $acudiente);
            
            $mensaje = " EL Alumno fue Actualizado";
        } else if ($usuario_dao->load($this->conexion, $usuario)) {
            $return->success = false;
            $return->errorMessage = " Error el usuario ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $usuario_dao->create($this->conexion, $usuario);
            $mensaje = " EL Alumno fue Creado";

            $alumno->setAll($object["cedula"], $object["ultimo_anio_cursado"], $object["id_nivel"]);
            $alumno_dao->create($this->conexion, $alumno);

            $acudiente->setAll($object["cedula_acudiente"], $object["cedula"], $object["id_parentesco"]);
            $acudiente_dao->create($this->conexion, $acudiente);
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el usuario";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarUsuario($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $usuario_dao = new UsuarioDao();
        $usuario = new Usuario();

        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM loginpermiso WHERE id_login = (SELECT id_login FROM login WHERE cedula = {$object["cedula"]});");
        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM auditoria WHERE id_login = (SELECT id_login FROM login WHERE cedula = {$object["cedula"]});");
        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM login WHERE cedula = {$object["cedula"]};");
        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM acudiente WHERE id_padre = {$object["cedula"]};");
        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM acudiente WHERE id_alumno = {$object["cedula"]};");
        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM usuariocolegio WHERE cedula = {$object["cedula"]};");
        $usuario_dao->databaseUpdate($this->conexion, "DELETE FROM alumno WHERE cedula = {$object["cedula"]};");

        $usuario = $usuario_dao->getObject($this->conexion, $object["cedula"]);
        $result = $usuario_dao->delete($this->conexion, $usuario);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " El usuario fue eliminado del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el usuario";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function listarUsuario($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $usuario_dao = new UsuarioDao();
        $return = new stdClass();

        if (trim($object["cedula"]) != "")
            $filtro .= " AND u.cedula = {$object["cedula"]} ";
        if (trim($object["nombres"]) != "")
            $filtro .= " AND u.nombre LIKE '%{$object["nombres"]}%' ";
        if (trim($object["email"]) != "")
            $filtro .= " AND u.email LIKE '%{$object["email"]}%' ";

        $lista_usuario = array();
        $result = $this->conexion->execute("SELECT u.*, b.id_localidad, tp.nombre_tipo_documento FROM usuario u, barrio b, tipodocumento tp WHERE u.id_barrio = b.id_barrio AND u.id_tipo_documento = tp.id_tipo_documento $filtro ");

        while ($row = $this->conexion->nextRow($result)) {

            $temp = array();
            $temp["cedula"] = $row[0];
            $temp["nombres"] = $row[1];
            $temp["apellidos"] = $row[2];
            $temp["telefono"] = $row[3];
            $temp["direccion"] = $row[4];
            $temp["email"] = $row[5];
            $temp["fechaNacimiento"] = $row[6];
            $temp["fechaRegistro"] = $row[7];
            $temp["rutaFoto"] = $row[8];
            $temp["genero"] = $row[9];
            $temp["idTipoDocumento"] = $row[10];
            $temp["idBarrio"] = $row[11];
            $temp["idLocalidad"] = $row[12];
            $temp["nombreTipoDocumento"] = $row[13];
            $lista_usuario[] = $temp;
        }

        if (count($lista_usuario) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($lista_usuario);
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar usuarios";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function listarUsuarioColegio($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $usuario_dao = new UsuarioDao();
        $return = new stdClass();

        if (trim($object["nombres"]) != "")
            $filtro .= " AND u.nombres LIKE '%{$object["nombres"]}%' ";
        if (trim($object["email"]) != "")
            $filtro .= " AND u.email LIKE '%{$object["email"]}%' ";
        if (trim($object["colegio"]) != "")
            $filtro .= " AND uc.id_colegio = {$object["colegio"]} ";
        if (trim($object["cedula"]) != "")
            $filtro .= " AND uc.cedula = {$object["cedula"]} ";

        $lista_usuario = array();
        $result = $this->conexion->execute("SELECT u.*, b.id_localidad, uc.id_colegio, c.nombre_colegio FROM usuario u, barrio b, usuariocolegio uc, colegio c WHERE u.id_barrio = b.id_barrio AND u.cedula = uc.cedula AND c.id_colegio = uc.id_colegio $filtro ");

        while ($row = $this->conexion->nextRow($result)) {

            $temp = array();
            $temp["cedula"] = $row[0];
            $temp["nombres"] = $row[1];
            $temp["apellidos"] = $row[2];
            $temp["telefono"] = $row[3];
            $temp["direccion"] = $row[4];
            $temp["email"] = $row[5];
            $temp["fechaNacimiento"] = $row[6];
            $temp["fechaRegistro"] = $row[7];
            $temp["rutaFoto"] = $row[8];
            $temp["genero"] = $row[9];
            $temp["idTipoDocumento"] = $row[10];
            $temp["idBarrio"] = $row[11];
            $temp["idLocalidad"] = $row[12];
            $temp["idColegio"] = $row[13];
            $temp["nombreColegio"] = $row[14];
            $lista_usuario[] = $temp;
        }

        if (count($lista_usuario) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($lista_usuario);
        } else {
            $return->success = false;
            $return->errorMessage = " No hay usuarios disponibles";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function listarUsuarioAlumno($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $usuario_dao = new UsuarioDao();
        $return = new stdClass();

        if (trim($object["nombres"]) != "")
            $filtro .= " AND ual.nombres LIKE '%{$object["nombres"]}%' ";
        if (trim($object["email"]) != "")
            $filtro .= " AND ual.email LIKE '%{$object["email"]}%' ";
        if (trim($object["cedula"]) != "")
            $filtro .= " AND ual.cedula = {$object["cedula"]} ";

        $lista_usuario = array();
        $result = $this->conexion->execute("SELECT ual.*, b.id_localidad, al.ultimo_anio_cursado, n.nombre_nivel, CONCAT(uac.nombres, ' ', uac.apellidos) as nombre_acudiente, ac.id_padre, al.id_nivel, p.id_parentesco "
                . "FROM usuario ual, barrio b, alumno al, acudiente ac, usuario uac, nivel n, parentesco p "
                . "WHERE ual.id_barrio = b.id_barrio "
                . "AND ual.cedula = al.cedula "
                . "AND al.id_nivel = n.id_nivel "
                . "AND ac.id_alumno = al.cedula "
                . "AND uac.cedula = ac.id_padre "
                . "AND p.id_parentesco = ac.id_parentesco "
                . "$filtro ");

        while ($row = $this->conexion->nextRow($result)) {

            $temp = array();
            $temp["cedula"] = $row[0];
            $temp["nombres"] = $row[1];
            $temp["apellidos"] = $row[2];
            $temp["telefono"] = $row[3];
            $temp["direccion"] = $row[4];
            $temp["email"] = $row[5];
            $temp["fechaNacimiento"] = $row[6];
            $temp["fechaRegistro"] = $row[7];
            $temp["rutaFoto"] = $row[8];
            $temp["genero"] = $row[9];
            $temp["idTipoDocumento"] = $row[10];
            $temp["idBarrio"] = $row[11];
            $temp["idLocalidad"] = $row[12];
            $temp["ultimoAnioCursado"] = $row[13];
            $temp["nombreNivel"] = $row[14];
            $temp["nombreAcudiente"] = $row[15];
            $temp["idPadre"] = $row[16];
            $temp["idNivel"] = $row[17];
            $temp["idParentesco"] = $row[18];
            $lista_usuario[] = $temp;
        }

        if (count($lista_usuario) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($lista_usuario);
        } else {
            $return->success = false;
            $return->errorMessage = " No hay usuarios disponibles";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function listarUsuarioAcudiente($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $usuario_dao = new UsuarioDao();
        $return = new stdClass();

        if (trim($object["nombres"]) != "")
            $filtro .= " AND uac.nombres LIKE '%{$object["nombres"]}%' ";
        if (trim($object["email"]) != "")
            $filtro .= " AND uac.email LIKE '%{$object["email"]}%' ";
        if (trim($object["cedula"]) != "")
            $filtro .= " AND uac.cedula = {$object["cedula"]} ";

        $lista_usuario = array();
        $result = $this->conexion->execute("SELECT uac.*, b.id_localidad "
                . "FROM usuario uac, barrio b, login l "
                . "WHERE uac.id_barrio = b.id_barrio "
                . "AND uac.cedula = l.cedula "
                . "AND l.id_perfil = 1"
                . "$filtro ");

        while ($row = $this->conexion->nextRow($result)) {

            $temp = array();
            $temp["cedula"] = $row[0];
            $temp["nombres"] = $row[1];
            $temp["apellidos"] = $row[2];
            $temp["telefono"] = $row[3];
            $temp["direccion"] = $row[4];
            $temp["email"] = $row[5];
            $temp["fechaNacimiento"] = $row[6];
            $temp["fechaRegistro"] = $row[7];
            $temp["rutaFoto"] = $row[8];
            $temp["genero"] = $row[9];
            $temp["idTipoDocumento"] = $row[10];
            $temp["idBarrio"] = $row[11];
            $temp["idLocalidad"] = $row[12];
            $lista_usuario[] = $temp;
        }

        if (count($lista_usuario) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($lista_usuario);
        } else {
            $return->success = false;
            $return->errorMessage = " No hay usuarios disponibles";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function registrarUsuarioCupo($object) {
        $object = $this->objectToArray($object);
        $result_usuario = false;
        $result_cupo = false;
        $return = new stdClass();
        $usuario_dao = new UsuarioDao();
        $usuario = new Usuario();
        $acudiente_dao = new AcudienteDao();
        $alumno_dao = new AlumnoDao();
        $acudiente = new Acudiente();
        $alumno = new Alumno();
        $cupoService = new CupoService();

        $usuario->setAll($object["cedula"], $object["nombres"], $object["apellidos"], $object["telefono"], $object["direccion"], $object["email"], $object["fecha_nacimiento"], date("Y-m-d"), "", $object["genero"], $object["id_tipo_documento"], $object["id_barrio"]);

        if ($usuario_dao->load($this->conexion, $usuario)) {
            $return->success = false;
            $return->errorMessage = " Error el usuario ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        } else {
            $result_usuario = $usuario_dao->create($this->conexion, $usuario);
            $mensaje = " EL Alumno fue Creado";

            $alumno->setAll($object["cedula"], $object["ultimo_anio_cursado"], $object["id_nivel"]);
            $alumno_dao->create($this->conexion, $alumno);

            $acudiente->setAll($object["cedula_acudiente"], $object["cedula"], $object["id_parentesco"]);
            $acudiente_dao->create($this->conexion, $acudiente);
        }


        $cupo = new stdClass();
        $cupo->id_cupo = "";
        $cupo->alumno = $object["cedula"];
        $cupo->nivel = $object["id_nivel"];
        $cupo->jornada = $object["id_jornada"];
        $cupo->estado = "3";
        $cupo->archivos = $object["archivos"];

        $result_cupo = $cupoService->gestionarCupoInterno($cupo, false);

        if ($result_usuario && $result_cupo) {
            $return->success = true;
            $return->errorMessage = " El usuario fue creado, y su cupo fue asignado";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el usuario";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE UsuarioService
//-------------------------------------------------
?>
