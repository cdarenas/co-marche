<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class EstadoService {

    var $conexion;

    function EstadoService() {
        $this->conexion = new Datasource();
    }
    
    public function listarEstado($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $estado_dao = new EstadoDao();
        $estado = new Estado();
        $estado->setAll(0, "", $object["id_tabla"]);
        $lista_estado = $estado_dao->searchMatching($this->conexion, $estado);
        
        if (count($lista_estado) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_estado));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar estado";
            $return->data = array();
        }

        return json_encode($return);
    }
   
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE EstadoService
//-------------------------------------------------
?>
