<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class LoginService {

    var $conexion;

    function LoginService() {
        $this->conexion = new Datasource();
    }

    public function listarLogin($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $login_dao = new LoginDao();
        $lista_login = array();

        if (trim($object["id_login"]) != "")
            $filtro .= " AND l.id_login = {$object["id_login"]} ";
        if (trim($object["cedula"]) != "")
            $filtro .= " AND l.cedula = {$object["cedula"]} ";
        if (trim($object["id_perfil"]) != "")
            $filtro .= " AND l.id_perfil = {$object["id_perfil"]} ";
        if (trim($object["id_estado"]) != "")
            $filtro .= " AND l.id_estado = {$object["id_estado"]} ";
        if (trim($object["usuario"]) != "")
            $filtro .= " AND l.usuario LIKE '%{$object["usuario"]}%' ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT l.*"
                    . ", CONCAT(us.nombres, ' ', us.apellidos) as nombres"
                    . ", p.nombre_perfil as perfil "
                    . ", e.nombre_estado as estado "
                    . "FROM login l "
                    . "LEFT JOIN perfil p ON l.id_perfil = p.id_perfil "
                    . "LEFT JOIN usuario us ON l.cedula = us.cedula "
                    . "LEFT JOIN estado e ON l.id_estado = e.id_estado "
                    . "WHERE 1=1 $filtro ");

            while ($row = $this->conexion->nextRow($result)) {

                $temp = array();
                $temp["idLogin"] = $row[0];
                $temp["cedula"] = $row[1];
                $temp["idPerfil"] = $row[2];
                $temp["usuario"] = $row[3];
                $temp["contrasena"] = $row[4];
                $temp["fechaConexion"] = $row[5];
                $temp["ipConexion"] = $row[6];
                $temp["idEstado"] = $row[7];
                $temp["nombres"] = $row[8];
                $temp["perfil"] = $row[9];
                $temp["estado"] = $row[10];

                $lista_login[] = $temp;
            }
        } else {
            $lista_login = $login_dao->loadAll($this->conexion);
        }

        //throw new Exception(print_r($lista_login, true));

        if (count($lista_login) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_login));
        } else {
            $return->success = false;
            $return->errorMessage = " No se encontro ninguna Login";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function autenticarUsuario($usuario, $contrasena) {
        $return = new stdClass();
        $login_dao = new LoginDao();
        $login = new Login();
        $login->setAll(0, 0, 0, $usuario, $contrasena, "", "", 1);
        $lista_login = $login_dao->searchMatching($this->conexion, $login);


        if (count($lista_login) > 0) {
            $login = $lista_login[0];
            $login->setFechaConexion(date("Y-m-d"));
            $login_dao->save($this->conexion, $login);

            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_login[0]));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de autentificar el Usuario (Usuario No Existe)";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function recuperarPassword($email) {

        $return = new stdClass();
        $lista_login = array();
        $lista_login["envio"] = true;

        if (count($lista_login) > 0) {
            $return->success = true;
            $return->errorMessage = " Se le envio al correo la contraseña";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de recuperar password";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function listarMenu($usuario) {
        $return = new stdClass();
        $lista_menu = array();
        $sql = "select l.nombre_modulo, l.icono, sl.nombre_sub_modulo, sl.url, sl.id_html, p.nombre_permiso
from modulo m, submodulo sm, permiso p, loginpermiso lp, login l
where p.id_sub_modulo = sl.id_sub_modulo
and sl.id_modulo = l.id_modulo
and lp.id_permiso = p.id_permiso
and lp.id_login = l.id_login
and l.usuario = '" . $usuario . "'";

        $result = $this->conexion->execute($sql);

        while ($row = $this->conexion->nextRow($result)) {

            $lista_menu[$row[0]]["icon"] = $row[1];
            $lista_menu[$row[0]]["data"][$row[2]]["id"] = $row[4];
            $lista_menu[$row[0]]["data"][$row[2]]["url"] = $row[3];

            if (!isset($lista_menu[$row[0]]["data"][$row[2]]["permisos"])) {
                $lista_menu[$row[0]]["data"][$row[2]]["permisos"] = array();
            }

            $lista_menu[$row[0]]["data"][$row[2]]["permisos"][] = $row[5];
        }

        if (count($lista_menu) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($lista_menu);
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar el Menu";
            $return->data = array();
        }

        return json_encode($return);
    }
    
    
    public function gestionarLogin($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $login_dao = new LoginDao();
        $login = new Login();
        
        $login->setAll($object["id_login"], $object["id_usuario"], $object["id_perfil"], $object["usuario"], $object["contrasena"], date("Y-m-d"), "", $object["id_estado"]);

        if (trim($actualiza) == "true") {
            $result = $login_dao->save($this->conexion, $login);
            $mensaje = "El login fue Actualizado";
        } else if ($login_dao->load($this->conexion, $login)) {
            $return->success = false;
            $return->errorMessage = " Error el login ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $login_dao->create($this->conexion, $login);
            $mensaje = " El login fue Creado";
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el login";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarLogin($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $login_dao = new LoginDao();
        $login = new Login();

        $login = $login_dao->getObject($this->conexion, $object["id_login"]);
        $result = $login_dao->delete($this->conexion, $login);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " El Login fue eliminado del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de eliminar login";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE LoginService
//-------------------------------------------------
?>
