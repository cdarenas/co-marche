<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class ColegioService {

    var $conexion;

    function ColegioService() {
        $this->conexion = new Datasource();
    }

    public function listarColegio($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $colegio_dao = new ColegioDao();

        if (trim($object["nombre_colegio"]) != "")
            $filtro .= " AND c.nombre_colegio LIKE '%{$object["nombre_colegio"]}%' ";
        if (trim($object["localidad"]) != "")
            $filtro .= " AND b.id_localidad = {$object["localidad"]} ";
        if (trim($object["id_colegio"]) != "")
            $filtro .= " AND c.id_colegio = {$object["id_colegio"]} ";

        $lista_colegio = array();
        $result = $this->conexion->execute("SELECT c.*, b.id_localidad FROM colegio c, barrio b WHERE c.id_barrio = b.id_barrio $filtro ");

        while ($row = $this->conexion->nextRow($result)) {

            $array_sede = array();
            $array_jornada = array();
            $array_nivel = array();
            
            $result_sede = $this->conexion->execute("SELECT sc.id_sede FROM sedecolegio sc WHERE sc.id_colegio = $row[0];");
            while ($row_sede = $this->conexion->nextRow($result_sede)) {
                $array_sede[] = $row_sede[0];
            }

            $result_jornada = $this->conexion->execute("SELECT sc.id_jornada FROM jornadacolegio sc WHERE sc.id_colegio = $row[0];");
            while ($row_jornada = $this->conexion->nextRow($result_jornada)) {
                $array_jornada[] = $row_jornada[0];
            }

            $result_nivel = $this->conexion->execute("SELECT sc.id_nivel FROM nivelcolegio sc WHERE sc.id_colegio = $row[0];");
            while ($row_nivel = $this->conexion->nextRow($result_nivel)) {
                $array_nivel[] = $row_nivel[0];
            }

            $temp = array();
            $temp["idColegio"] = $row[0];
            $temp["nombreColegio"] = $row[1];
            $temp["direccion"] = $row[2];
            $temp["telefono"] = $row[3];
            $temp["fax"] = $row[4];
            $temp["email"] = $row[5];
            $temp["nombreRector"] = $row[6];
            $temp["idEstado"] = $row[7];
            $temp["idBarrio"] = $row[8];
            $temp["idLocalidad"] = $row[9];
            $temp["idSede"] = $array_sede;
            $temp["idJornada"] = $array_jornada;
            $temp["idNivel"] = $array_nivel;
            $lista_colegio[] = $temp;
        }

        if (count($lista_colegio) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_colegio));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar colegios";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function gestionarColegio($object, $actualiza) {
        $result = false;
        $mensaje = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $colegio_dao = new ColegioDao();
        $colegio = new Colegio();

        $colegio->setAll($object["id_colegio"], $object["nombre_colegio"], $object["direccion"], $object["telefono"], $object["fax"], $object["email"], $object["nombre_rector"], $object["id_estado"], $object["id_barrio"]);

        if (trim($actualiza) == "true") {
            $result = $colegio_dao->save($this->conexion, $colegio);

            $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM jornadacolegio where id_colegio = {$object["id_colegio"]}");
            $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM sedecolegio where id_colegio = {$object["id_colegio"]}");
            $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM nivelcolegio where id_colegio = {$object["id_colegio"]}");

            $colegio_dao->databaseUpdate($this->conexion, "INSERT INTO jornadacolegio (id_colegio, id_jornada) "
                    . " SELECT {$object["id_colegio"]}, id_jornada FROM jornada where id_jornada in ({$object["array_jornada"]});");
            $colegio_dao->databaseUpdate($this->conexion, "INSERT INTO sedecolegio (id_colegio, id_sede) "
                    . " SELECT {$object["id_colegio"]}, id_sede FROM sede where id_sede in ({$object["array_sede"]});");
            $colegio_dao->databaseUpdate($this->conexion, "INSERT INTO nivelcolegio (id_colegio, id_nivel) "
                    . " SELECT {$object["id_colegio"]}, id_nivel FROM nivel where id_nivel in ({$object["array_nivel"]});");

            $mensaje = " EL colegio colegio fue Actualizado";
        } else if ($colegio_dao->load($this->conexion, $colegio)) {
            $return->success = false;
            $return->errorMessage = " Error el colegio ya se encuentra Creado";
            $return->data = array();
            return json_encode($return);
        } else {
            $result = $colegio_dao->create($this->conexion, $colegio);
            $mensaje = " EL colegio colegio fue Creado";
            
            $result2 = $this->conexion->execute("SELECT LAST_INSERT_ID() as id_colegio;");
            $row = $this->conexion->nextRow($result2);

            $colegio_dao->databaseUpdate($this->conexion, "INSERT INTO jornadacolegio (id_colegio, id_jornada) "
                    . "SELECT {$row["id_colegio"]}, id_jornada FROM jornada where id_jornada in ({$object["array_jornada"]});");
            $colegio_dao->databaseUpdate($this->conexion, "INSERT INTO sedecolegio (id_colegio, id_sede) "
                    . "SELECT {$row["id_colegio"]}, id_sede FROM sede where id_sede in ({$object["array_sede"]});");
            $colegio_dao->databaseUpdate($this->conexion, "INSERT INTO nivelcolegio (id_colegio, id_nivel) "
                    . "SELECT {$row["id_colegio"]}, id_nivel FROM nivel where id_nivel in ({$object["array_nivel"]});");
        }

        if ($result) {
            $return->success = true;
            $return->errorMessage = $mensaje;
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el colegio";
            $return->data = array();
        }

        return json_encode($return);
    }

    public function eliminarColegio($object) {
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $colegio_dao = new ColegioDao();
        $colegio = new Colegio();

        $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM jornadacolegio WHERE id_colegio = {$object["id_colegio"]};");
        $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM sedecolegio WHERE id_colegio = {$object["id_colegio"]};");
        $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM nivelcolegio WHERE id_colegio = {$object["id_colegio"]};");
        $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM matricula WHERE id_colegio = {$object["id_colegio"]};");
        $colegio_dao->databaseUpdate($this->conexion, "DELETE FROM usuariocolegio WHERE id_colegio = {$object["id_colegio"]};");

        $colegio = $colegio_dao->getObject($this->conexion, $object["id_colegio"]);
        $result = $colegio_dao->delete($this->conexion, $colegio);

        if ($result) {
            $return->success = true;
            $return->errorMessage = " El colegio fue eliminado del sistema";
            $return->data = array();
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de registrar el colegio";
            $return->data = array();
        }

        return json_encode($return);
    }

    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE ColegioService
//-------------------------------------------------
?>
