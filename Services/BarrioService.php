<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class BarrioService {

    var $conexion;

    function BarrioService() {
        $this->conexion = new Datasource();
    }
    
    public function listarBarrio($id_localidad) {
        $return = new stdClass();
        $barrio_dao = new BarrioDao();
        $barrio = new Barrio();
        $barrio->setAll(0, "", $id_localidad);
        $lista_barrio = $barrio_dao->searchMatching($this->conexion, $barrio);


        if (count($lista_barrio) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_barrio));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar pefiles";
            $return->data = array();
        }

        return json_encode($return);
    }
   
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE BarrioService
//-------------------------------------------------
?>
