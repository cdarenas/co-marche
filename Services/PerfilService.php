<?php

require_once AMFPHP_ROOTPATH . 'ClassLoader.php';

class PerfilService {

    var $conexion;

    function PerfilService() {
        $this->conexion = new Datasource();
    }
    
    public function listarPerfil($object) {
        $filtro = "";
        $object = $this->objectToArray($object);
        $return = new stdClass();
        $perfil_dao = new PerfilDao();
        $lista_perfil = array();
        if (trim($object["nombre_perfil"]) != "")
            $filtro .= " AND p.nombre_perfil LIKE '%{$object["nombre_perfil"]}%' ";
        if (trim($object["id_perfil"]) != "")
            $filtro .= " AND p.id_perfil = {$object["id_perfil"]} ";

        if (trim($filtro) != "") {

            $result = $this->conexion->execute("SELECT p.* FROM perfil p WHERE 1=1 $filtro ");
            while ($row = $this->conexion->nextRow($result)) {

                $temp = array();
                $temp["idPerfil"] = $row[0];
                $temp["nombrePerfil"] = $row[1];
                $lista_perfil[] = $temp;
            }
        } else {
            $lista_perfil = $perfil_dao->loadAll($this->conexion);
        }
        if (count($lista_perfil) > 0) {
            $return->success = true;
            $return->errorMessage = "";
            $return->data = $this->codificar_utf8($this->objectToArray($lista_perfil));
        } else {
            $return->success = false;
            $return->errorMessage = " Error al momento de listar pefiles";
            $return->data = array();
        }

        return json_encode($return);
    }
   
    private function codificar_utf8($result) {
        if (is_array($result)) {
            foreach ((array) $result as $key => $value) {
                $result[$key] = $this->codificar_utf8($value);
            }
            return $result;
        } else if (is_string($result)) {
            return utf8_encode($result);
        }

        return "";
    }

    private function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }

}

//-------------------------------------------------
// FINAL DE LA CLASE PerfilService
//-------------------------------------------------
?>
