<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Parentesco {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idParentesco;
    var $nombreParentesco;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Parentesco() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdParentesco() {
        return $this->idParentesco;
    }

    function setIdParentesco($idParentescoIn) {
        $this->idParentesco = $idParentescoIn;
    }

    function getNombreParentesco() {
        return $this->nombreParentesco;
    }

    function setNombreParentesco($nombreParentescoIn) {
        $this->nombreParentesco = $nombreParentescoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idParentescoIn, $nombreParentescoIn) {
        $this->idParentesco = $idParentescoIn;
        $this->nombreParentesco = $nombreParentescoIn;
    }

    /**
     * hasEqualMapping-method will compare two Parentesco instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdParentesco() != $this->idParentesco) {
            return(false);
        }
        if ($valueObject->getNombreParentesco() != $this->nombreParentesco) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Parentesco, mapping to table parentesco\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idParentesco = " . $this->idParentesco . "\n";
        $out = $out . "nombreParentesco = " . $this->nombreParentesco . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Parentesco();

        $cloned->setIdParentesco($this->idParentesco);
        $cloned->setNombreParentesco($this->nombreParentesco);

        return $cloned;
    }

}

?>
