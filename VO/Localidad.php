<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Localidad {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idLocalidad;
    var $nombreLocalidad;
    var $idCiudad;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Localidad() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdLocalidad() {
        return $this->idLocalidad;
    }

    function setIdLocalidad($idLocalidadIn) {
        $this->idLocalidad = $idLocalidadIn;
    }

    function getNombreLocalidad() {
        return $this->nombreLocalidad;
    }

    function setNombreLocalidad($nombreLocalidadIn) {
        $this->nombreLocalidad = $nombreLocalidadIn;
    }

    function getIdCiudad() {
        return $this->idCiudad;
    }

    function setIdCiudad($idCiudadIn) {
        $this->idCiudad = $idCiudadIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idLocalidadIn, $nombreLocalidadIn, $idCiudadIn) {
        $this->idLocalidad = $idLocalidadIn;
        $this->nombreLocalidad = $nombreLocalidadIn;
        $this->idCiudad = $idCiudadIn;
    }

    /**
     * hasEqualMapping-method will compare two Localidad instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdLocalidad() != $this->idLocalidad) {
            return(false);
        }
        if ($valueObject->getNombreLocalidad() != $this->nombreLocalidad) {
            return(false);
        }
        if ($valueObject->getIdCiudad() != $this->idCiudad) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Localidad, mapping to table localidad\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idLocalidad = " . $this->idLocalidad . "\n";
        $out = $out . "nombreLocalidad = " . $this->nombreLocalidad . "\n";
        $out = $out . "idCiudad = " . $this->idCiudad . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Localidad();

        $cloned->setIdLocalidad($this->idLocalidad);
        $cloned->setNombreLocalidad($this->nombreLocalidad);
        $cloned->setIdCiudad($this->idCiudad);

        return $cloned;
    }

}

?>
