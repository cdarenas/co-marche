<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Colegio {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idColegio;
    var $nombreColegio;
    var $direccion;
    var $telefono;
    var $fax;
    var $email;
    var $nombreRector;
    var $idEstado;
    var $idBarrio;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Colegio() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdColegio() {
        return $this->idColegio;
    }

    function setIdColegio($idColegioIn) {
        $this->idColegio = $idColegioIn;
    }

    function getNombreColegio() {
        return $this->nombreColegio;
    }

    function setNombreColegio($nombreColegioIn) {
        $this->nombreColegio = $nombreColegioIn;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function setDireccion($direccionIn) {
        $this->direccion = $direccionIn;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setTelefono($telefonoIn) {
        $this->telefono = $telefonoIn;
    }

    function getFax() {
        return $this->fax;
    }

    function setFax($faxIn) {
        $this->fax = $faxIn;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($emailIn) {
        $this->email = $emailIn;
    }

    function getNombreRector() {
        return $this->nombreRector;
    }

    function setNombreRector($nombreRectorIn) {
        $this->nombreRector = $nombreRectorIn;
    }

    function getIdEstado() {
        return $this->idEstado;
    }

    function setIdEstado($idEstadoIn) {
        $this->idEstado = $idEstadoIn;
    }

    function getIdBarrio() {
        return $this->idBarrio;
    }

    function setIdBarrio($idBarrioIn) {
        $this->idBarrio = $idBarrioIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idColegioIn, $nombreColegioIn, $direccionIn, $telefonoIn, $faxIn, $emailIn, $nombreRectorIn, $idEstadoIn, $idBarrioIn) {
        $this->idColegio = $idColegioIn;
        $this->nombreColegio = $nombreColegioIn;
        $this->direccion = $direccionIn;
        $this->telefono = $telefonoIn;
        $this->fax = $faxIn;
        $this->email = $emailIn;
        $this->nombreRector = $nombreRectorIn;
        $this->idEstado = $idEstadoIn;
        $this->idBarrio = $idBarrioIn;
    }

    /**
     * hasEqualMapping-method will compare two Colegio instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdColegio() != $this->idColegio) {
            return(false);
        }
        if ($valueObject->getNombreColegio() != $this->nombreColegio) {
            return(false);
        }
        if ($valueObject->getDireccion() != $this->direccion) {
            return(false);
        }
        if ($valueObject->getTelefono() != $this->telefono) {
            return(false);
        }
        if ($valueObject->getFax() != $this->fax) {
            return(false);
        }
        if ($valueObject->getEmail() != $this->email) {
            return(false);
        }
        if ($valueObject->getNombreRector() != $this->nombreRector) {
            return(false);
        }
        if ($valueObject->getIdEstado() != $this->idEstado) {
            return(false);
        }
        if ($valueObject->getIdBarrio() != $this->idBarrio) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Colegio, mapping to table colegio\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idColegio = " . $this->idColegio . "\n";
        $out = $out . "nombreColegio = " . $this->nombreColegio . "\n";
        $out = $out . "direccion = " . $this->direccion . "\n";
        $out = $out . "telefono = " . $this->telefono . "\n";
        $out = $out . "fax = " . $this->fax . "\n";
        $out = $out . "email = " . $this->email . "\n";
        $out = $out . "nombreRector = " . $this->nombreRector . "\n";
        $out = $out . "idEstado = " . $this->idEstado . "\n";
        $out = $out . "idBarrio = " . $this->idBarrio . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Colegio();

        $cloned->setIdColegio($this->idColegio);
        $cloned->setNombreColegio($this->nombreColegio);
        $cloned->setDireccion($this->direccion);
        $cloned->setTelefono($this->telefono);
        $cloned->setFax($this->fax);
        $cloned->setEmail($this->email);
        $cloned->setNombreRector($this->nombreRector);
        $cloned->setIdEstado($this->idEstado);
        $cloned->setIdBarrio($this->idBarrio);

        return $cloned;
    }

}

?>