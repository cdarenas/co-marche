<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idLogin;
    var $cedula;
    var $idPerfil;
    var $usuario;
    var $contrasena;
    var $fechaConexion;
    var $ipConexion;
    var $idEstado;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Login() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdLogin() {
        return $this->idLogin;
    }

    function setIdLogin($idLoginIn) {
        $this->idLogin = $idLoginIn;
    }

    function getCedula() {
        return $this->cedula;
    }

    function setCedula($cedulaIn) {
        $this->cedula = $cedulaIn;
    }

    function getIdPerfil() {
        return $this->idPerfil;
    }

    function setIdPerfil($idPerfilIn) {
        $this->idPerfil = $idPerfilIn;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setUsuario($usuarioIn) {
        $this->usuario = $usuarioIn;
    }

    function getContrasena() {
        return $this->contrasena;
    }

    function setContrasena($contrasenaIn) {
        $this->contrasena = $contrasenaIn;
    }

    function getFechaConexion() {
        return $this->fechaConexion;
    }

    function setFechaConexion($fechaConexionIn) {
        $this->fechaConexion = $fechaConexionIn;
    }

    function getIpConexion() {
        return $this->ipConexion;
    }

    function setIpConexion($ipConexionIn) {
        $this->ipConexion = $ipConexionIn;
    }

    function getIdEstado() {
        return $this->idEstado;
    }

    function setIdEstado($idEstadoIn) {
        $this->idEstado = $idEstadoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idLoginIn, $cedulaIn, $idPerfilIn, $usuarioIn, $contrasenaIn, $fechaConexionIn, $ipConexionIn, $idEstadoIn) {
        $this->idLogin = $idLoginIn;
        $this->cedula = $cedulaIn;
        $this->idPerfil = $idPerfilIn;
        $this->usuario = $usuarioIn;
        $this->contrasena = $contrasenaIn;
        $this->fechaConexion = $fechaConexionIn;
        $this->ipConexion = $ipConexionIn;
        $this->idEstado = $idEstadoIn;
    }

    /**
     * hasEqualMapping-method will compare two Login instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdLogin() != $this->idLogin) {
            return(false);
        }
        if ($valueObject->getCedula() != $this->cedula) {
            return(false);
        }
        if ($valueObject->getIdPerfil() != $this->idPerfil) {
            return(false);
        }
        if ($valueObject->getUsuario() != $this->usuario) {
            return(false);
        }
        if ($valueObject->getContrasena() != $this->contrasena) {
            return(false);
        }
        if ($valueObject->getFechaConexion() != $this->fechaConexion) {
            return(false);
        }
        if ($valueObject->getIpConexion() != $this->ipConexion) {
            return(false);
        }
        if ($valueObject->getIdEstado() != $this->idEstado) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Login, mapping to table login\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idLogin = " . $this->idLogin . "\n";
        $out = $out . "cedula = " . $this->cedula . "\n";
        $out = $out . "idPerfil = " . $this->idPerfil . "\n";
        $out = $out . "usuario = " . $this->usuario . "\n";
        $out = $out . "contrasena = " . $this->contrasena . "\n";
        $out = $out . "fechaConexion = " . $this->fechaConexion . "\n";
        $out = $out . "ipConexion = " . $this->ipConexion . "\n";
        $out = $out . "idEstado = " . $this->idEstado . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Login();

        $cloned->setIdLogin($this->idLogin);
        $cloned->setCedula($this->cedula);
        $cloned->setIdPerfil($this->idPerfil);
        $cloned->setUsuario($this->usuario);
        $cloned->setContrasena($this->contrasena);
        $cloned->setFechaConexion($this->fechaConexion);
        $cloned->setIpConexion($this->ipConexion);
        $cloned->setIdEstado($this->idEstado);

        return $cloned;
    }

}

?>