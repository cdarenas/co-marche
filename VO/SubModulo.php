<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SubModulo {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idSubModulo;
    var $nombreSubModulo;
    var $idModulo;
    var $url;
    var $idHtml;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function SubModulo() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdSubModulo() {
        return $this->idSubModulo;
    }

    function setIdSubModulo($idSubModuloIn) {
        $this->idSubModulo = $idSubModuloIn;
    }

    function getNombreSubModulo() {
        return $this->nombreSubModulo;
    }

    function setNombreSubModulo($nombreSubModuloIn) {
        $this->nombreSubModulo = $nombreSubModuloIn;
    }

    function getIdModulo() {
        return $this->idModulo;
    }

    function setIdModulo($idModuloIn) {
        $this->idModulo = $idModuloIn;
    }

    function getUrl() {
        return $this->url;
    }

    function setUrl($urlIn) {
        $this->url = $urlIn;
    }

    function getIdHtml() {
        return $this->idHtml;
    }

    function setIdHtml($idHtmlIn) {
        $this->idHtml = $idHtmlIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idSubModuloIn, $nombreSubModuloIn, $idModuloIn, $urlIn, $idHtmlIn) {
        $this->idSubModulo = $idSubModuloIn;
        $this->nombreSubModulo = $nombreSubModuloIn;
        $this->idModulo = $idModuloIn;
        $this->url = $urlIn;
        $this->idHtml = $idHtmlIn;
    }

    /**
     * hasEqualMapping-method will compare two SubModulo instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdSubModulo() != $this->idSubModulo) {
            return(false);
        }
        if ($valueObject->getNombreSubModulo() != $this->nombreSubModulo) {
            return(false);
        }
        if ($valueObject->getIdModulo() != $this->idModulo) {
            return(false);
        }
        if ($valueObject->getUrl() != $this->url) {
            return(false);
        }
        if ($valueObject->getIdHtml() != $this->idHtml) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass SubModulo, mapping to table submodulo\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idSubModulo = " . $this->idSubModulo . "\n";
        $out = $out . "nombreSubModulo = " . $this->nombreSubModulo . "\n";
        $out = $out . "idModulo = " . $this->idModulo . "\n";
        $out = $out . "url = " . $this->url . "\n";
        $out = $out . "idHtml = " . $this->idHtml . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new SubModulo();

        $cloned->setIdSubModulo($this->idSubModulo);
        $cloned->setNombreSubModulo($this->nombreSubModulo);
        $cloned->setIdModulo($this->idModulo);
        $cloned->setUrl($this->url);
        $cloned->setIdHtml($this->idHtml);

        return $cloned;
    }

}
