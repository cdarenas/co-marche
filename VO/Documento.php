<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Documento {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idDocumento;
    var $nombreDocumento;
    var $tamanio;
    var $ruta;
    var $fechaRegistro;
    var $extension;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Documento() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdDocumento() {
        return $this->idDocumento;
    }

    function setIdDocumento($idDocumentoIn) {
        $this->idDocumento = $idDocumentoIn;
    }

    function getNombreDocumento() {
        return $this->nombreDocumento;
    }

    function setNombreDocumento($nombreDocumentoIn) {
        $this->nombreDocumento = $nombreDocumentoIn;
    }

    function getTamanio() {
        return $this->tamanio;
    }

    function setTamanio($tamanioIn) {
        $this->tamanio = $tamanioIn;
    }

    function getRuta() {
        return $this->ruta;
    }

    function setRuta($rutaIn) {
        $this->ruta = $rutaIn;
    }

    function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    function setFechaRegistro($fechaRegistroIn) {
        $this->fechaRegistro = $fechaRegistroIn;
    }

    function getExtension() {
        return $this->extension;
    }

    function setExtension($extensionIn) {
        $this->extension = $extensionIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idDocumentoIn, $nombreDocumentoIn, $tamanioIn, $rutaIn, $fechaRegistroIn, $extensionIn) {
        $this->idDocumento = $idDocumentoIn;
        $this->nombreDocumento = $nombreDocumentoIn;
        $this->tamanio = $tamanioIn;
        $this->ruta = $rutaIn;
        $this->fechaRegistro = $fechaRegistroIn;
        $this->extension = $extensionIn;
    }

    /**
     * hasEqualMapping-method will compare two Documento instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdDocumento() != $this->idDocumento) {
            return(false);
        }
        if ($valueObject->getNombreDocumento() != $this->nombreDocumento) {
            return(false);
        }
        if ($valueObject->getTamanio() != $this->tamanio) {
            return(false);
        }
        if ($valueObject->getRuta() != $this->ruta) {
            return(false);
        }
        if ($valueObject->getFechaRegistro() != $this->fechaRegistro) {
            return(false);
        }
        if ($valueObject->getExtension() != $this->extension) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Documento, mapping to table documento\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idDocumento = " . $this->idDocumento . "\n";
        $out = $out . "nombreDocumento = " . $this->nombreDocumento . "\n";
        $out = $out . "tamanio = " . $this->tamanio . "\n";
        $out = $out . "ruta = " . $this->ruta . "\n";
        $out = $out . "fechaRegistro = " . $this->fechaRegistro . "\n";
        $out = $out . "extension = " . $this->extension . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Documento();

        $cloned->setIdDocumento($this->idDocumento);
        $cloned->setNombreDocumento($this->nombreDocumento);
        $cloned->setTamanio($this->tamanio);
        $cloned->setRuta($this->ruta);
        $cloned->setFechaRegistro($this->fechaRegistro);
        $cloned->setExtension($this->extension);

        return $cloned;
    }

}

?>