<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LoginPermiso {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idLogin;
    var $id_permiso;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function LoginPermiso() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdLogin() {
        return $this->idLogin;
    }

    function setIdLogin($idLoginIn) {
        $this->idLogin = $idLoginIn;
    }

    function getId_permiso() {
        return $this->id_permiso;
    }

    function setId_permiso($id_permisoIn) {
        $this->id_permiso = $id_permisoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idLoginIn, $id_permisoIn) {
        $this->idLogin = $idLoginIn;
        $this->id_permiso = $id_permisoIn;
    }

    /**
     * hasEqualMapping-method will compare two LoginPermiso instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdLogin() != $this->idLogin) {
            return(false);
        }
        if ($valueObject->getId_permiso() != $this->id_permiso) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass LoginPermiso, mapping to table loginpermiso\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idLogin = " . $this->idLogin . "\n";
        $out = $out . "id_permiso = " . $this->id_permiso . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new LoginPermiso();

        $cloned->setIdLogin($this->idLogin);
        $cloned->setId_permiso($this->id_permiso);

        return $cloned;
    }

}

?>
