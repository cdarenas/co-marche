<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usuario {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $cedula;
    var $nombres;
    var $apellidos;
    var $telefono;
    var $direccion;
    var $email;
    var $fechaNaciminento;
    var $fechaRegistro;
    var $rutaFoto;
    var $genero;
    var $idTipoDocumento;
    var $idBarrio;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Usuario() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getCedula() {
        return $this->cedula;
    }

    function setCedula($cedulaIn) {
        $this->cedula = $cedulaIn;
    }

    function getNombres() {
        return $this->nombres;
    }

    function setNombres($nombresIn) {
        $this->nombres = $nombresIn;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function setApellidos($apellidosIn) {
        $this->apellidos = $apellidosIn;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setTelefono($telefonoIn) {
        $this->telefono = $telefonoIn;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function setDireccion($direccionIn) {
        $this->direccion = $direccionIn;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($emailIn) {
        $this->email = $emailIn;
    }

    function getFechaNaciminento() {
        return $this->fechaNaciminento;
    }

    function setFechaNaciminento($fechaNaciminentoIn) {
        $this->fechaNaciminento = $fechaNaciminentoIn;
    }

    function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    function setFechaRegistro($fechaRegistroIn) {
        $this->fechaRegistro = $fechaRegistroIn;
    }

    function getRutaFoto() {
        return $this->rutaFoto;
    }

    function setRutaFoto($rutaFotoIn) {
        $this->rutaFoto = $rutaFotoIn;
    }

    function getGenero() {
        return $this->genero;
    }

    function setGenero($generoIn) {
        $this->genero = $generoIn;
    }

    function getIdTipoDocumento() {
        return $this->idTipoDocumento;
    }

    function setIdTipoDocumento($idTipoDocumentoIn) {
        $this->idTipoDocumento = $idTipoDocumentoIn;
    }

    function getIdBarrio() {
        return $this->idBarrio;
    }

    function setIdBarrio($idBarrioIn) {
        $this->idBarrio = $idBarrioIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($cedulaIn, $nombresIn, $apellidosIn, $telefonoIn, $direccionIn, $emailIn, $fechaNaciminentoIn, $fechaRegistroIn, $rutaFotoIn, $generoIn, $idTipoDocumentoIn, $idBarrioIn) {
        $this->cedula = $cedulaIn;
        $this->nombres = $nombresIn;
        $this->apellidos = $apellidosIn;
        $this->telefono = $telefonoIn;
        $this->direccion = $direccionIn;
        $this->email = $emailIn;
        $this->fechaNaciminento = $fechaNaciminentoIn;
        $this->fechaRegistro = $fechaRegistroIn;
        $this->rutaFoto = $rutaFotoIn;
        $this->genero = $generoIn;
        $this->idTipoDocumento = $idTipoDocumentoIn;
        $this->idBarrio = $idBarrioIn;
    }

    /**
     * hasEqualMapping-method will compare two Usuario instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getCedula() != $this->cedula) {
            return(false);
        }
        if ($valueObject->getNombres() != $this->nombres) {
            return(false);
        }
        if ($valueObject->getApellidos() != $this->apellidos) {
            return(false);
        }
        if ($valueObject->getTelefono() != $this->telefono) {
            return(false);
        }
        if ($valueObject->getDireccion() != $this->direccion) {
            return(false);
        }
        if ($valueObject->getEmail() != $this->email) {
            return(false);
        }
        if ($valueObject->getFechaNaciminento() != $this->fechaNaciminento) {
            return(false);
        }
        if ($valueObject->getFechaRegistro() != $this->fechaRegistro) {
            return(false);
        }
        if ($valueObject->getRutaFoto() != $this->rutaFoto) {
            return(false);
        }
        if ($valueObject->getGenero() != $this->genero) {
            return(false);
        }
        if ($valueObject->getIdTipoDocumento() != $this->idTipoDocumento) {
            return(false);
        }
        if ($valueObject->getIdBarrio() != $this->idBarrio) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Usuario, mapping to table usuario\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "cedula = " . $this->cedula . "\n";
        $out = $out . "nombres = " . $this->nombres . "\n";
        $out = $out . "apellidos = " . $this->apellidos . "\n";
        $out = $out . "telefono = " . $this->telefono . "\n";
        $out = $out . "direccion = " . $this->direccion . "\n";
        $out = $out . "email = " . $this->email . "\n";
        $out = $out . "fechaNaciminento = " . $this->fechaNaciminento . "\n";
        $out = $out . "fechaRegistro = " . $this->fechaRegistro . "\n";
        $out = $out . "rutaFoto = " . $this->rutaFoto . "\n";
        $out = $out . "genero = " . $this->genero . "\n";
        $out = $out . "idTipoDocumento = " . $this->idTipoDocumento . "\n";
        $out = $out . "idBarrio = " . $this->idBarrio . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Usuario();

        $cloned->setCedula($this->cedula);
        $cloned->setNombres($this->nombres);
        $cloned->setApellidos($this->apellidos);
        $cloned->setTelefono($this->telefono);
        $cloned->setDireccion($this->direccion);
        $cloned->setEmail($this->email);
        $cloned->setFechaNaciminento($this->fechaNaciminento);
        $cloned->setFechaRegistro($this->fechaRegistro);
        $cloned->setRutaFoto($this->rutaFoto);
        $cloned->setGenero($this->genero);
        $cloned->setIdTipoDocumento($this->idTipoDocumento);
        $cloned->setIdBarrio($this->idBarrio);

        return $cloned;
    }

}
