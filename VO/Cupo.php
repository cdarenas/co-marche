<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cupo {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idCupo;
    var $fechaRegistro;
    var $idAlumno;
    var $idNivel;
    var $idJornada;
    var $idEstado;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Cupo() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdCupo() {
        return $this->idCupo;
    }

    function setIdCupo($idCupoIn) {
        $this->idCupo = $idCupoIn;
    }

    function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    function setFechaRegistro($fechaRegistroIn) {
        $this->fechaRegistro = $fechaRegistroIn;
    }

    function getIdAlumno() {
        return $this->idAlumno;
    }

    function setIdAlumno($idAlumnoIn) {
        $this->idAlumno = $idAlumnoIn;
    }

    function getIdNivel() {
        return $this->idNivel;
    }

    function setIdNivel($idNivelIn) {
        $this->idNivel = $idNivelIn;
    }

    function getIdJornada() {
        return $this->idJornada;
    }

    function setIdJornada($idJornadaIn) {
        $this->idJornada = $idJornadaIn;
    }

    function getIdEstado() {
        return $this->idEstado;
    }

    function setIdEstado($idEstadoIn) {
        $this->idEstado = $idEstadoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idCupoIn, $fechaRegistroIn, $idAlumnoIn, $idNivelIn, $idJornadaIn, $idEstadoIn) {
        $this->idCupo = $idCupoIn;
        $this->fechaRegistro = $fechaRegistroIn;
        $this->idAlumno = $idAlumnoIn;
        $this->idNivel = $idNivelIn;
        $this->idJornada = $idJornadaIn;
        $this->idEstado = $idEstadoIn;
    }

    /**
     * hasEqualMapping-method will compare two Cupo instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdCupo() != $this->idCupo) {
            return(false);
        }
        if ($valueObject->getFechaRegistro() != $this->fechaRegistro) {
            return(false);
        }
        if ($valueObject->getIdAlumno() != $this->idAlumno) {
            return(false);
        }
        if ($valueObject->getIdNivel() != $this->idNivel) {
            return(false);
        }
        if ($valueObject->getIdJornada() != $this->idJornada) {
            return(false);
        }
        if ($valueObject->getIdEstado() != $this->idEstado) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Cupo, mapping to table cupo\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idCupo = " . $this->idCupo . "\n";
        $out = $out . "fechaRegistro = " . $this->fechaRegistro . "\n";
        $out = $out . "idAlumno = " . $this->idAlumno . "\n";
        $out = $out . "idNivel = " . $this->idNivel . "\n";
        $out = $out . "idJornada = " . $this->idJornada . "\n";
        $out = $out . "idEstado = " . $this->idEstado . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Cupo();

        $cloned->setIdCupo($this->idCupo);
        $cloned->setFechaRegistro($this->fechaRegistro);
        $cloned->setIdAlumno($this->idAlumno);
        $cloned->setIdNivel($this->idNivel);
        $cloned->setIdJornada($this->idJornada);
        $cloned->setIdEstado($this->idEstado);

        return $cloned;
    }

}

?>