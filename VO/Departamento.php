<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Departamento {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idDepartamento;
    var $nombreDepartamento;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Departamento() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdDepartamento() {
        return $this->idDepartamento;
    }

    function setIdDepartamento($idDepartamentoIn) {
        $this->idDepartamento = $idDepartamentoIn;
    }

    function getNombreDepartamento() {
        return $this->nombreDepartamento;
    }

    function setNombreDepartamento($nombreDepartamentoIn) {
        $this->nombreDepartamento = $nombreDepartamentoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idDepartamentoIn, $nombreDepartamentoIn) {
        $this->idDepartamento = $idDepartamentoIn;
        $this->nombreDepartamento = $nombreDepartamentoIn;
    }

    /**
     * hasEqualMapping-method will compare two Departamento instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdDepartamento() != $this->idDepartamento) {
            return(false);
        }
        if ($valueObject->getNombreDepartamento() != $this->nombreDepartamento) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Departamento, mapping to table departamento\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idDepartamento = " . $this->idDepartamento . "\n";
        $out = $out . "nombreDepartamento = " . $this->nombreDepartamento . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Departamento();

        $cloned->setIdDepartamento($this->idDepartamento);
        $cloned->setNombreDepartamento($this->nombreDepartamento);

        return $cloned;
    }

}

?>