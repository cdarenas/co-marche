<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SedeColegio {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idSede;
    var $idColegio;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function SedeColegio() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdSede() {
        return $this->idSede;
    }

    function setIdSede($idSedeIn) {
        $this->idSede = $idSedeIn;
    }

    function getIdColegio() {
        return $this->idColegio;
    }

    function setIdColegio($idColegioIn) {
        $this->idColegio = $idColegioIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idSedeIn, $idColegioIn) {
        $this->idSede = $idSedeIn;
        $this->idColegio = $idColegioIn;
    }

    /**
     * hasEqualMapping-method will compare two SedeColegio instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdSede() != $this->idSede) {
            return(false);
        }
        if ($valueObject->getIdColegio() != $this->idColegio) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass SedeColegio, mapping to table sedecolegio\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idSede = " . $this->idSede . "\n";
        $out = $out . "idColegio = " . $this->idColegio . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new SedeColegio();

        $cloned->setIdSede($this->idSede);
        $cloned->setIdColegio($this->idColegio);

        return $cloned;
    }

}
