<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Modulo {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $id_modulo;
    var $nombreModulo;
    var $icono;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Modulo() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getId_modulo() {
        return $this->id_modulo;
    }

    function setId_modulo($id_moduloIn) {
        $this->id_modulo = $id_moduloIn;
    }

    function getNombreModulo() {
        return $this->nombreModulo;
    }

    function setNombreModulo($nombreModuloIn) {
        $this->nombreModulo = $nombreModuloIn;
    }

    function getIcono() {
        return $this->icono;
    }

    function setIcono($iconoIn) {
        $this->icono = $iconoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($id_moduloIn, $nombreModuloIn, $iconoIn) {
        $this->id_modulo = $id_moduloIn;
        $this->nombreModulo = $nombreModuloIn;
        $this->icono = $iconoIn;
    }

    /**
     * hasEqualMapping-method will compare two Modulo instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getId_modulo() != $this->id_modulo) {
            return(false);
        }
        if ($valueObject->getNombreModulo() != $this->nombreModulo) {
            return(false);
        }
        if ($valueObject->getIcono() != $this->icono) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Modulo, mapping to table modulo\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "id_modulo = " . $this->id_modulo . "\n";
        $out = $out . "nombreModulo = " . $this->nombreModulo . "\n";
        $out = $out . "icono = " . $this->icono . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Modulo();

        $cloned->setId_modulo($this->id_modulo);
        $cloned->setNombreModulo($this->nombreModulo);
        $cloned->setIcono($this->icono);

        return $cloned;
    }

}

?>
