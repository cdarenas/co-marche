<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Auditoria {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idAuditoria;
    var $fechaRegistro;
    var $metodo;
    var $ipCliente;
    var $idLogin;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Auditoria() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdAuditoria() {
        return $this->idAuditoria;
    }

    function setIdAuditoria($idAuditoriaIn) {
        $this->idAuditoria = $idAuditoriaIn;
    }

    function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    function setFechaRegistro($fechaRegistroIn) {
        $this->fechaRegistro = $fechaRegistroIn;
    }

    function getMetodo() {
        return $this->metodo;
    }

    function setMetodo($metodoIn) {
        $this->metodo = $metodoIn;
    }

    function getIpCliente() {
        return $this->ipCliente;
    }

    function setIpCliente($ipClienteIn) {
        $this->ipCliente = $ipClienteIn;
    }

    function getIdLogin() {
        return $this->idLogin;
    }

    function setIdLogin($idLoginIn) {
        $this->idLogin = $idLoginIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idAuditoriaIn, $fechaRegistroIn, $metodoIn, $ipClienteIn, $idLoginIn) {
        $this->idAuditoria = $idAuditoriaIn;
        $this->fechaRegistro = $fechaRegistroIn;
        $this->metodo = $metodoIn;
        $this->ipCliente = $ipClienteIn;
        $this->idLogin = $idLoginIn;
    }

    /**
     * hasEqualMapping-method will compare two Auditoria instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdAuditoria() != $this->idAuditoria) {
            return(false);
        }
        if ($valueObject->getFechaRegistro() != $this->fechaRegistro) {
            return(false);
        }
        if ($valueObject->getMetodo() != $this->metodo) {
            return(false);
        }
        if ($valueObject->getIpCliente() != $this->ipCliente) {
            return(false);
        }
        if ($valueObject->getIdLogin() != $this->idLogin) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Auditoria, mapping to table audiotria\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idAuditoria = " . $this->idAuditoria . "\n";
        $out = $out . "fechaRegistro = " . $this->fechaRegistro . "\n";
        $out = $out . "metodo = " . $this->metodo . "\n";
        $out = $out . "ipCliente = " . $this->ipCliente . "\n";
        $out = $out . "idLogin = " . $this->idLogin . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {

        $cloned = new Auditoria();
        $cloned->setIdAuditoria($this->idAuditoria);
        $cloned->setFechaRegistro($this->fechaRegistro);
        $cloned->setMetodo($this->metodo);
        $cloned->setIpCliente($this->ipCliente);
        $cloned->setIdLogin($this->idLogin);

        return $cloned;
    }
}
?>