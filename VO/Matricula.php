<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Matricula {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idMatricula;
    var $fechaRegistro;
    var $idColegio;
    var $idNivel;
    var $idSede;
    var $idJornada;
    var $idCupo;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Matricula() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdMatricula() {
        return $this->idMatricula;
    }

    function setIdMatricula($idMatriculaIn) {
        $this->idMatricula = $idMatriculaIn;
    }

    function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    function setFechaRegistro($fechaRegistroIn) {
        $this->fechaRegistro = $fechaRegistroIn;
    }

    function getIdColegio() {
        return $this->idColegio;
    }

    function setIdColegio($idColegioIn) {
        $this->idColegio = $idColegioIn;
    }

    function getIdNivel() {
        return $this->idNivel;
    }

    function setIdNivel($idNivelIn) {
        $this->idNivel = $idNivelIn;
    }

    function getIdSede() {
        return $this->idSede;
    }

    function setIdSede($idSedeIn) {
        $this->idSede = $idSedeIn;
    }

    function getIdJornada() {
        return $this->idJornada;
    }

    function setIdJornada($idJornadaIn) {
        $this->idJornada = $idJornadaIn;
    }

    function getIdCupo() {
        return $this->idCupo;
    }

    function setIdCupo($idCupoIn) {
        $this->idCupo = $idCupoIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idMatriculaIn, $fechaRegistroIn, $idColegioIn, $idNivelIn, $idSedeIn, $idJornadaIn, $idCupoIn) {
        $this->idMatricula = $idMatriculaIn;
        $this->fechaRegistro = $fechaRegistroIn;
        $this->idColegio = $idColegioIn;
        $this->idNivel = $idNivelIn;
        $this->idSede = $idSedeIn;
        $this->idJornada = $idJornadaIn;
        $this->idCupo = $idCupoIn;
    }

    /**
     * hasEqualMapping-method will compare two Matricula instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdMatricula() != $this->idMatricula) {
            return(false);
        }
        if ($valueObject->getFechaRegistro() != $this->fechaRegistro) {
            return(false);
        }
        if ($valueObject->getIdColegio() != $this->idColegio) {
            return(false);
        }
        if ($valueObject->getIdNivel() != $this->idNivel) {
            return(false);
        }
        if ($valueObject->getIdSede() != $this->idSede) {
            return(false);
        }
        if ($valueObject->getIdJornada() != $this->idJornada) {
            return(false);
        }
        if ($valueObject->getIdCupo() != $this->idCupo) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Matricula, mapping to table matricula\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idMatricula = " . $this->idMatricula . "\n";
        $out = $out . "fechaRegistro = " . $this->fechaRegistro . "\n";
        $out = $out . "idColegio = " . $this->idColegio . "\n";
        $out = $out . "idNivel = " . $this->idNivel . "\n";
        $out = $out . "idSede = " . $this->idSede . "\n";
        $out = $out . "idJornada = " . $this->idJornada . "\n";
        $out = $out . "idCupo = " . $this->idCupo . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Matricula();

        $cloned->setIdMatricula($this->idMatricula);
        $cloned->setFechaRegistro($this->fechaRegistro);
        $cloned->setIdColegio($this->idColegio);
        $cloned->setIdNivel($this->idNivel);
        $cloned->setIdSede($this->idSede);
        $cloned->setIdJornada($this->idJornada);
        $cloned->setIdCupo($this->idCupo);

        return $cloned;
    }

}

?>