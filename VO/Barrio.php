<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Barrio {

    /**
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    var $idBarrio;
    var $nombreBarrio;
    var $idLocalidad;

    /**
     * Constructors. DaoGen generates two constructors by default.
     * The first one takes no arguments and provides the most simple
     * way to create object instance. The another one takes one
     * argument, which is the primary key of the corresponding table.
     */
    function Barrio() {
        
    }

    /**
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */
    function getIdBarrio() {
        return $this->idBarrio;
    }

    function setIdBarrio($idBarrioIn) {
        $this->idBarrio = $idBarrioIn;
    }

    function getNombreBarrio() {
        return $this->nombreBarrio;
    }

    function setNombreBarrio($nombreBarrioIn) {
        $this->nombreBarrio = $nombreBarrioIn;
    }

    function getIdLocalidad() {
        return $this->idLocalidad;
    }

    function setIdLocalidad($idLocalidadIn) {
        $this->idLocalidad = $idLocalidadIn;
    }

    /**
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */
    function setAll($idBarrioIn, $nombreBarrioIn, $idLocalidadIn) {
        $this->idBarrio = $idBarrioIn;
        $this->nombreBarrio = $nombreBarrioIn;
        $this->idLocalidad = $idLocalidadIn;
    }

    /**
     * hasEqualMapping-method will compare two Barrio instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    function hasEqualMapping($valueObject) {

        if ($valueObject->getIdBarrio() != $this->idBarrio) {
            return(false);
        }
        if ($valueObject->getNombreBarrio() != $this->nombreBarrio) {
            return(false);
        }
        if ($valueObject->getIdLocalidad() != $this->idLocalidad) {
            return(false);
        }

        return true;
    }

    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    function toString() {
        $out = "";
        $out = $out . "\nclass Barrio, mapping to table barrio\n";
        $out = $out . "Persistent attributes: \n";
        $out = $out . "idBarrio = " . $this->idBarrio . "\n";
        $out = $out . "nombreBarrio = " . $this->nombreBarrio . "\n";
        $out = $out . "idLocalidad = " . $this->idLocalidad . "\n";
        return $out;
    }

    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    function clones() {
        $cloned = new Barrio();

        $cloned->setIdBarrio($this->idBarrio);
        $cloned->setNombreBarrio($this->nombreBarrio);
        $cloned->setIdLocalidad($this->idLocalidad);

        return $cloned;
    }

}

?>
