<?php

/**
 *  This file is part of amfPHP
 *
 * LICENSE
 *
 * This source file is subject to the license that is bundled
 * with this package in the file license.txt.
 */
/**
 * include this to include amfphp
 * note: this list could be generated. In the meantime maintain it manually. 
 * It would be nice to do this alphabetically, It seems however that an interface must be loaded before a class, so do as possible
 *
 * @author Ariel Sommeria-klein
 * @package Amfphp
 *
 */
if (!defined('AMFPHP_ROOTPATH')){
    define('AMFPHP_ROOTPATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);
}
if (!defined('AMFPHP_VERSION')){
    define('AMFPHP_VERSION', '2.2.2');
}
if (!defined('FILE_PATH_CARGADOS')){
    define('FILE_PATH_CARGADOS', 'C:/secretaria/');
}

//core/common
require_once AMFPHP_ROOTPATH . 'Core/Common/ClassFindInfo.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/IDeserializer.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/IExceptionHandler.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/IDeserializedRequestHandler.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/ISerializer.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/IVoConverter.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/ServiceRouter.php';
require_once AMFPHP_ROOTPATH . 'Core/Common/ServiceCallParameters.php';

//core/amf
require_once AMFPHP_ROOTPATH . 'Core/Amf/Constants.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Deserializer.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Handler.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Header.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Message.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Packet.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Serializer.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Util.php';

//core/Amf/types
require_once AMFPHP_ROOTPATH . 'Core/Amf/Types/ByteArray.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Types/Undefined.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Types/Date.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Types/Vector.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Types/Xml.php';
require_once AMFPHP_ROOTPATH . 'Core/Amf/Types/XmlDocument.php';

//core
require_once AMFPHP_ROOTPATH . 'Core/Config.php';
require_once AMFPHP_ROOTPATH . 'Core/Exception.php';
require_once AMFPHP_ROOTPATH . 'Core/Gateway.php';
require_once AMFPHP_ROOTPATH . 'Core/FilterManager.php';
require_once AMFPHP_ROOTPATH . 'Core/HttpRequestGatewayFactory.php';
require_once AMFPHP_ROOTPATH . 'Core/PluginManager.php';

//vo
require_once AMFPHP_ROOTPATH . 'VO/Acudiente.php';
require_once AMFPHP_ROOTPATH . 'VO/Alumno.php';
require_once AMFPHP_ROOTPATH . 'VO/Auditoria.php';
require_once AMFPHP_ROOTPATH . 'VO/Barrio.php';
require_once AMFPHP_ROOTPATH . 'VO/Ciudad.php';
require_once AMFPHP_ROOTPATH . 'VO/Colegio.php';
require_once AMFPHP_ROOTPATH . 'VO/Cupo.php';
require_once AMFPHP_ROOTPATH . 'VO/Departamento.php';
require_once AMFPHP_ROOTPATH . 'VO/Documento.php';
require_once AMFPHP_ROOTPATH . 'VO/Estado.php';
require_once AMFPHP_ROOTPATH . 'VO/JornadaColegio.php';
require_once AMFPHP_ROOTPATH . 'VO/Jornada.php';
require_once AMFPHP_ROOTPATH . 'VO/Localidad.php';
require_once AMFPHP_ROOTPATH . 'VO/Login.php';
require_once AMFPHP_ROOTPATH . 'VO/LoginPermiso.php';
require_once AMFPHP_ROOTPATH . 'VO/Matricula.php';
require_once AMFPHP_ROOTPATH . 'VO/Modulo.php';
require_once AMFPHP_ROOTPATH . 'VO/NivelColegio.php';
require_once AMFPHP_ROOTPATH . 'VO/Nivel.php';
require_once AMFPHP_ROOTPATH . 'VO/Parentesco.php';
require_once AMFPHP_ROOTPATH . 'VO/Perfil.php';
require_once AMFPHP_ROOTPATH . 'VO/PerfilPermiso.php';
require_once AMFPHP_ROOTPATH . 'VO/Permiso.php';
require_once AMFPHP_ROOTPATH . 'VO/Sede.php';
require_once AMFPHP_ROOTPATH . 'VO/SedeColegio.php';
require_once AMFPHP_ROOTPATH . 'VO/SubModulo.php';
require_once AMFPHP_ROOTPATH . 'VO/Tabla.php';
require_once AMFPHP_ROOTPATH . 'VO/TipoDocumento.php';
require_once AMFPHP_ROOTPATH . 'VO/Usuario.php';
require_once AMFPHP_ROOTPATH . 'VO/UsuarioColegio.php';

//dao
require_once AMFPHP_ROOTPATH . 'DAO/AcudienteDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/AlumnoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/AuditoriaDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/BarrioDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/CiudadDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/ColegioDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/CupoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/DepartamentoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/DocumentoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/EstadoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/JornadaColegioDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/JornadaDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/LocalidadDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/LoginDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/LoginPermisoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/MatriculaDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/ModuloDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/NivelColegioDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/NivelDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/ParentescoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/PerfilDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/PerfilPermisoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/PermisoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/SedeDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/SedeColegioDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/SubModuloDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/TablaDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/TipoDocumentoDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/UsuarioDao.php';
require_once AMFPHP_ROOTPATH . 'DAO/UsuarioColegioDao.php';

//bd
require_once AMFPHP_ROOTPATH . 'BD/Datasource.php';
?>
