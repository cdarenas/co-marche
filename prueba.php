<?php

$method = $_POST;
$actualiza = json_decode(htmlspecialchars($method['obj_json'], ENT_NOQUOTES));
$object_json = json_decode(htmlspecialchars($method['obj_json'], ENT_NOQUOTES));
$funcion = trim(htmlspecialchars($method['funcion'], ENT_NOQUOTES));

if ($funcion == "autenticarUsuario") {
    autenticarUsuario();
} else if ($funcion == "registrarUsuario1") {
    registrarUsuario1();
} else if ($funcion == "recuperarPassword") {
    recuperarPassword();
} else if ($funcion == "cargarMenu") {
    cargarMenu();
} else if ($funcion == "listarUsuarios") {
    listarUsuarios();
} else if ($funcion == "validarUsuarioAcudiente") {
    validarUsuarioAcudiente();
} else if ($funcion == "gestionarUsuarioAcudiente") {
    gestionarUsuarioAcudiente();
} else if ($funcion == "cargarTipoDocumento") {
    cargarTipoDocumento();
} else if ($funcion == "cargarLocalidad") {
    cargarLocalidad();
} else if ($funcion == "cargarBarrio") {
    cargarBarrio();
} else if ($funcion == "listarCupo") {
    listarCupo();
} else if ($funcion == "buscarUsuario") {
    buscarUsuario();
} else if ($funcion == "cargarPerfil") {
    cargarPerfil();
} else if ($funcion == "cargarColegio") {
    cargarColegio();
}/* else if ($funcion == "") {
  ();
  } */ else {
    $return = new stdClass();
    $return->success = false;
    $return->errorMessage = " El momento al realizar la transacion";
    $json = json_encode($return);
    echo $json;
}

function autenticarUsuario() {
    global $object_json;

    $obj = array();
    $obj["idLogin"] = "1";
    $obj["id_localidad"] = "1012340763";
    $obj["idPerfil"] = "4";
    $obj["usuario"] = "admin";
    $obj["contrasena"] = "hola*123";
    $obj["fechaConexion"] = "07/05/2016";
    $obj["ipConexion"] = "";
    $obj["idEstado"] = "1";
    //exit(var_dump($object_json));

    $return = new stdClass();


    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " Error al momento de autentificar el Usuario (Usuario No Existe)";
        $return->data = array();
    }

//Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

//Return the json string to the JavaScript
    echo $json;
}

function registrarUsuario1() {
    global $object_json;

    $obj = array();
    $obj["id_login"] = "1";
    $obj["cedula"] = "1012340763";
    $obj["id_perfil"] = "4";
    $obj["usuario"] = "admin";
    $obj["contrasena"] = "hola*123";
    $obj["fecha_conexion"] = "07/05/2016";
    $obj["ipConexion"] = "";
    $obj["idEstado"] = "1";
    //exit(var_dump($object_json));

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " Error al momento de autentificar Usuario (Usuario No Existe)";
        $return->data = array();
    }

//Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

//Return the json string to the JavaScript
    echo $json;
}

function recuperarPassword() {
    global $object_json;

    $obj = array();
    $return = new stdClass();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = " Se le envio al correo la contraseña";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El correo ingresado no corresponde al correo registrado";
        $return->data = $obj;
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function cargarMenu() {
    global $object_json;

    $obj = array();

    $sub_mod = array();
    $sub_mod["Gestion Perfil"]["id"] = "itemPerfil";
    $sub_mod["Gestion Perfil"]["url"] = "../Admin/Seguridad/gestionPerfil.html";
    $sub_mod["Gestion Perfil"]["permisos"] = array("Listar Perfil", "Crear Perfil", "Inactivar Perfil", "Editar Perfil");
    $sub_mod["Gestion Login"]["id"] = "itemLogin";
    $sub_mod["Gestion Login"]["url"] = "../Admin/Seguridad/gestionLogin.html";
    $sub_mod["Gestion Login"]["permisos"] = array("Listar Login", "Crear Login", "Inactivar Login", "Editar Login", "Listar Audiotria");
    $sub_mod["Gestion Paginas"]["id"] = "itemPaginas";
    $sub_mod["Gestion Paginas"]["url"] = "../Admin/Seguridad/gestionPaginas.html";
    $sub_mod["Gestion Paginas"]["permisos"] = array("Listar Paginas", "Crear Paginas", "Inactivar Paginas", "Editar Paginas");
    $sub_mod["Gestion Submodulo"]["id"] = "itemSub";
    $sub_mod["Gestion Submodulo"]["url"] = "../Admin/Seguridad/gestionSubModulo.html";
    $sub_mod["Gestion Submodulo"]["permisos"] = array("Listar Submodulo", "Crear Submodulo", "Inactivar Submodulo", "Editar Submodulo");
    $sub_mod["Gestion Modulo"]["id"] = "itemModulo";
    $sub_mod["Gestion Modulo"]["url"] = "../Admin/Seguridad/gestionModulo.html";
    $sub_mod["Gestion Modulo"]["permisos"] = array("Listar Modulo", "Crear Modulo", "Inactivar Modulo", "Editar Modulo");

    $obj["Modulo Seguridad"]["icon"] = "fa fa-key";
    $obj["Modulo Seguridad"]["data"] = $sub_mod;

    $sub_mod = array();
    $sub_mod["Gestion Usuario"]["id"] = "itemUsuario";
    $sub_mod["Gestion Usuario"]["url"] = "../Admin/Usuario/gestionUsuario.html";
    $sub_mod["Gestion Usuario"]["permisos"] = array("Listar Usuario", "Crear Usuario", "Inactivar Usuario", "Editar Usuario");
    $sub_mod["Gestion Usuario Colegio"]["id"] = "itemUsuColegio";
    $sub_mod["Gestion Usuario Colegio"]["url"] = "../Admin/Usuario/gestionUsuarioColegio.html";
    $sub_mod["Gestion Usuario Colegio"]["permisos"] = array("Listar Usuario Colegio", "Crear Usuario Colegio", "Inactivar Usuario Colegio", "Editar Usuario Colegio");
    $sub_mod["Gestion Usuario Alumno"]["id"] = "itemAlumno";
    $sub_mod["Gestion Usuario Alumno"]["url"] = "../Admin/Usuario/gestionAlumno.html";
    $sub_mod["Gestion Usuario Alumno"]["permisos"] = array("Listar Usuario Alumno", "Crear Usuario Alumno", "Inactivar Usuario Usuario", "Editar Usuario Usuario");
    $sub_mod["Gestion Usuario Acudiente"]["id"] = "itemAcudiente";
    $sub_mod["Gestion Usuario Acudiente"]["url"] = "../Admin/Usuario/gestionAcudiente.html";
    $sub_mod["Gestion Usuario Acudiente"]["permisos"] = array("Listar Usuario Acudiente", "Crear Usuario Acudiente", "Inactivar Usuario Acudiente", "Editar Usuario Acudiente");

    $obj["Modulo Usuario"]["icon"] = "fa fa-users";
    $obj["Modulo Usuario"]["data"] = $sub_mod;

    $sub_mod = array();
    $sub_mod["Gestion Colegio"]["id"] = "itemColegio";
    $sub_mod["Gestion Colegio"]["url"] = "../Admin/Colegio/gestionColegio.html";
    $sub_mod["Gestion Colegio"]["permisos"] = array("Listar Colegio", "Crear Colegio", "Inactivar Colegio", "Editar Colegio");
    $sub_mod["Gestion Jornada"]["id"] = "itemJornada";
    $sub_mod["Gestion Jornada"]["url"] = "../Admin/Colegio/gestionJornada.html";
    $sub_mod["Gestion Jornada"]["permisos"] = array("Listar Jornada", "Crear Jornada", "Inactivar Jornada", "Editar Jornada");
    $sub_mod["Gestion Sede"]["id"] = "itemSede";
    $sub_mod["Gestion Sede"]["url"] = "../Admin/Colegio/gestionSede.html";
    $sub_mod["Gestion Sede"]["permisos"] = array("Listar Sede", "Crear Sede", "Inactivar Sede", "Editar Sede");
    $sub_mod["Gestion Nivel"]["id"] = "itemNivel";
    $sub_mod["Gestion Nivel"]["url"] = "../Admin/Colegio/gestionNivel.html";
    $sub_mod["Gestion Nivel"]["permisos"] = array("Listar Nivel", "Crear Nivel", "Inactivar Nivel", "Editar Nivel");

    $obj["Modulo Colegio"]["icon"] = "fa fa-star";
    $obj["Modulo Colegio"]["data"] = $sub_mod;

    $sub_mod = array();
    $sub_mod["Gestion Matricula"]["id"] = "itemMatricula";
    $sub_mod["Gestion Matricula"]["url"] = "../Admin/Matricula/gestionMatricula.html";
    $sub_mod["Gestion Matricula"]["permisos"] = array("Listar Matricula", "Crear Matricula", "Inactivar Matricula", "Editar Matricula");
    $sub_mod["Gestion Cupo"]["id"] = "itemCupo";
    $sub_mod["Gestion Cupo"]["url"] = "../Admin/Matricula/gestionCupo.html";
    $sub_mod["Gestion Cupo"]["permisos"] = array("Listar Cupo", "Crear Cupo", "Inactivar Cupo", "Editar Cupo");
    $sub_mod["Gestion Documento"]["id"] = "itemDocumento";
    $sub_mod["Gestion Documento"]["url"] = "../Admin/Matricula/gestionDocumento.html";
    $sub_mod["Gestion Documento"]["permisos"] = array("Listar Documento", "Crear Documento", "Inactivar Documento", "Editar Documento");

    $obj["Modulo Matricula"]["icon"] = "fa fa-file";
    $obj["Modulo Matricula"]["data"] = $sub_mod;

    $return = new stdClass();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function listarUsuarios() {
    global $object_json;

    $obj = array();

    $usuario = array();
    $usuario["id_tipo_documento"] = "2";
    $usuario["cedula"] = "123456";
    $usuario["nombres"] = "Prueba 1";
    $usuario["apellidos"] = "Prueba 11";
    $usuario["telefono"] = "1245789";
    $usuario["direccion"] = "Calle 8 # 78 -12 ";
    $usuario["email"] = "prueba1@prueba1.com";
    $usuario["fecha_nacimiento"] = "12/05/2016";
    $usuario["fecha_registro"] = "12/05/2016";
    $usuario["id_barrio"]["id_barrio"] = "2";
    $usuario["id_barrio"]["id_localidad"] = "1";
    $obj[] = $usuario;

    $usuario = array();
    $usuario["id_tipo_documento"] = "2";
    $usuario["cedula"] = "123456";
    $usuario["nombres"] = "Prueba 2";
    $usuario["apellidos"] = "Prueba 21";
    $usuario["telefono"] = "1245789";
    $usuario["direccion"] = "Calle 8 # 78 -12 ";
    $usuario["email"] = "prueba2@prueba2.com";
    $usuario["fecha_nacimiento"] = "12/05/2016";
    $usuario["fecha_registro"] = "12/05/2016";
    $usuario["id_barrio"]["id_barrio"] = "2";
    $usuario["id_barrio"]["id_localidad"] = "1";
    ;
    $obj[] = $usuario;

    $usuario = array();
    $usuario["id_tipo_documento"] = "2";
    $usuario["cedula"] = "123456";
    $usuario["nombres"] = "Prueba 3";
    $usuario["apellidos"] = "Prueba 31";
    $usuario["telefono"] = "1245789";
    $usuario["direccion"] = "Calle 8 # 78 -12 ";
    $usuario["email"] = "prueba 3@prueba3.com";
    $usuario["fecha_nacimiento"] = "12/05/2016";
    $usuario["fecha_registro"] = "12/05/2016";
    $usuario["id_barrio"]["id_barrio"] = "2";
    $usuario["id_barrio"]["id_localidad"] = "1";
    $obj[] = $usuario;


    $usuario = array();
    $usuario["id_tipo_documento"] = "2";
    $usuario["cedula"] = "123456";
    $usuario["nombres"] = "Prueba 4";
    $usuario["apellidos"] = "Prueba 41";
    $usuario["telefono"] = "1245789";
    $usuario["direccion"] = "Calle 8 # 78 -12 ";
    $usuario["email"] = "prueba 4@prueba4.com";
    $usuario["fecha_nacimiento"] = "12/05/2016";
    $usuario["fecha_registro"] = "12/05/2016";
    $usuario["id_barrio"]["id_barrio"] = "2";
    $usuario["id_barrio"]["id_localidad"] = "1";
    $obj[] = $usuario;

    $usuario = array();
    $usuario["id_tipo_documento"] = "2";
    $usuario["cedula"] = "123456";
    $usuario["nombres"] = "Prueba 5";
    $usuario["apellidos"] = "Prueba 51";
    $usuario["telefono"] = "1245789";
    $usuario["direccion"] = "Calle 8 # 78 -12 ";
    $usuario["email"] = "prueba5@prueba5.com";
    $usuario["fecha_nacimiento"] = "12/05/2016";
    $usuario["fecha_registro"] = "12/05/2016";
    $usuario["id_barrio"]["id_barrio"] = "2";
    $usuario["id_barrio"]["id_localidad"] = "1";
    $obj[] = $usuario;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function validarUsuarioAcudiente() {
    global $object_json;

    $obj = array();

    $usuario = array();
    $usuario["id_login"] = "1";
    $usuario["cedula"] = "123456";
    $usuario["id_perfil"] = "4";
    $usuario["usuario"] = "admin";
    $usuario["contrasena"] = "hola*123";
    $usuario["fecha_conexion"] = "07/05/2016";
    $usuario["ipConexion"] = "";
    $usuario["idEstado"] = "1";
    $obj[] = $usuario;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function gestionarUsuarioAcudiente() {
    global $object_json, $actualiza;

    $obj = array();

    $usuario = array();
    $usuario["id_login"] = "1";
    $usuario["cedula"] = "123456";
    $usuario["id_perfil"] = "4";
    $usuario["usuario"] = "admin";
    $usuario["contrasena"] = "hola*123";
    $usuario["fecha_conexion"] = "07/05/2016";
    $usuario["ipConexion"] = "";
    $usuario["idEstado"] = "1";
    $obj[] = $usuario;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = $actualiza == "true" ? "Actualizo la informacion Exitosamente" : "Se guardo el usuario exitosamente";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function cargarTipoDocumento() {
    global $object_json;

    $obj = array();

    $tipo_documento = array();
    $tipo_documento["id_tipo_documento"] = "1";
    $tipo_documento["nombre_tipo_documento"] = "Cedula Ciudadania";
    $obj[] = $tipo_documento;

    $tipo_documento = array();
    $tipo_documento["id_tipo_documento"] = "2";
    $tipo_documento["nombre_tipo_documento"] = "Cedula Extranjeria";
    $obj[] = $tipo_documento;

    $tipo_documento = array();
    $tipo_documento["id_tipo_documento"] = "3";
    $tipo_documento["nombre_tipo_documento"] = "Pasaporte";
    $obj[] = $tipo_documento;


    $tipo_documento = array();
    $tipo_documento["id_tipo_documento"] = "4";
    $tipo_documento["nombre_tipo_documento"] = "NIT";
    $obj[] = $tipo_documento;

    $tipo_documento = array();
    $tipo_documento["id_tipo_documento"] = "5";
    $tipo_documento["nombre_tipo_documento"] = "RUT";
    $obj[] = $tipo_documento;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function cargarLocalidad() {
    global $object_json;

    $obj = array();

    $localidad = array();
    $localidad["id_localidad"] = "1";
    $localidad["nombre_localidad"] = "Usaquén";
    $obj[] = $localidad;

    $localidad = array();
    $localidad["id_localidad"] = "2";
    $localidad["nombre_localidad"] = "Chapinero";
    $obj[] = $localidad;

    $localidad = array();
    $localidad["id_localidad"] = "3";
    $localidad["nombre_localidad"] = "Santa Fe";
    $obj[] = $localidad;


    $localidad = array();
    $localidad["id_localidad"] = "4";
    $localidad["nombre_localidad"] = "San Cristobal";
    $obj[] = $localidad;

    $localidad = array();
    $localidad["id_localidad"] = "5";
    $localidad["nombre_localidad"] = "Usme";
    $obj[] = $localidad;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function cargarBarrio() {
    global $object_json;

    $obj = array();

    $barrio = array();
    $barrio["id_barrio"] = "1";
    $barrio["nombre_barrio"] = "Canaima";
    $barrio["id_localidad"] = "1";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "2";
    $barrio["nombre_barrio"] = "La Floresta de la Sabana";
    $barrio["id_localidad"] = "1";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "3";
    $barrio["nombre_barrio"] = "Torca";
    $barrio["id_localidad"] = "1";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "4";
    $barrio["nombre_barrio"] = "Alto de Serrezuela";
    $barrio["id_localidad"] = "1";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "5";
    $barrio["nombre_barrio"] = "Balcones de Vista Hermosa";
    $barrio["id_localidad"] = "1";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "136";
    $barrio["nombre_barrio"] = "Chico Reservado";
    $barrio["id_localidad"] = "2";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "137";
    $barrio["nombre_barrio"] = "BellaVista";
    $barrio["id_localidad"] = "2";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "138";
    $barrio["nombre_barrio"] = "Chico Alto";
    $barrio["id_localidad"] = "2";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "139";
    $barrio["nombre_barrio"] = "El Nogal";
    $barrio["id_localidad"] = "2";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "140";
    $barrio["nombre_barrio"] = "El Refugio";
    $barrio["id_localidad"] = "2";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "186";
    $barrio["nombre_barrio"] = "La Merced";
    $barrio["id_localidad"] = "3";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "187";
    $barrio["nombre_barrio"] = "Parque Central Bavaria";
    $barrio["id_localidad"] = "3";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "188";
    $barrio["nombre_barrio"] = "Sagrado Corazon";
    $barrio["id_localidad"] = "3";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "189";
    $barrio["nombre_barrio"] = "San Diego";
    $barrio["id_localidad"] = "3";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "190";
    $barrio["nombre_barrio"] = "San Martin";
    $barrio["id_localidad"] = "3";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "224";
    $barrio["nombre_barrio"] = "Aguas Claras";
    $barrio["id_localidad"] = "4";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "225";
    $barrio["nombre_barrio"] = "San Luis";
    $barrio["id_localidad"] = "4";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "226";
    $barrio["nombre_barrio"] = "Altos De Zipa";
    $barrio["id_localidad"] = "4";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "227";
    $barrio["nombre_barrio"] = "Sur America";
    $barrio["id_localidad"] = "4";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "228";
    $barrio["nombre_barrio"] = "Amapolas";
    $barrio["id_localidad"] = "4";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "426";
    $barrio["nombre_barrio"] = "Buenos Aires";
    $barrio["id_localidad"] = "5";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "427";
    $barrio["nombre_barrio"] = "Alfonso Lopez Sector Charala";
    $barrio["id_localidad"] = "5";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "428";
    $barrio["nombre_barrio"] = "Costa Rica";
    $barrio["id_localidad"] = "5";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "429";
    $barrio["nombre_barrio"] = "Antonio Jose de Sucre";
    $barrio["id_localidad"] = "5";
    $obj[] = $barrio;

    $barrio = array();
    $barrio["id_barrio"] = "430";
    $barrio["nombre_barrio"] = "Doña Liliana";
    $barrio["id_localidad"] = "5";
    $obj[] = $barrio;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function listarCupo() {
    global $object_json;

    $obj = array();

    $cupo = array();
    $cupo["nro_cupo"] = "1";
    $cupo["documento"] = "123456";
    $cupo["nombres"] = "Prueba 1";
    $cupo["apellidos"] = "Prueba 11";
    $cupo["fecha_solicitud"] = "15/05/2015";
    $cupo["nivel"] = "Nivel Octavo";
    $cupo["jornada"] = "Jornada Dirurna";
    $cupo["estado"] = "Solicitado";
    $obj[] = $cupo;

    $cupo = array();
    $cupo["nro_cupo"] = "2";
    $cupo["documento"] = "123456";
    $cupo["nombres"] = "Prueba 2";
    $cupo["apellidos"] = "Prueba 22";
    $cupo["fecha_solicitud"] = "15/05/2015";
    $cupo["nivel"] = "Nivel Septimo";
    $cupo["jornada"] = "Jornada Dirurna";
    $cupo["estado"] = "Solicitado";
    $obj[] = $cupo;

    $cupo = array();
    $cupo["nro_cupo"] = "3";
    $cupo["documento"] = "123456";
    $cupo["nombres"] = "Prueba 3";
    $cupo["apellidos"] = "Prueba 33";
    $cupo["fecha_solicitud"] = "15/05/2015";
    $cupo["nivel"] = "Nivel Noveno";
    $cupo["jornada"] = "Jornada Dirurna";
    $cupo["estado"] = "Solicitado";
    $obj[] = $cupo;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function buscarUsuario() {
    global $object_json;

    $obj = array();
    $usuario = array();
    $usuario["cedula"] = "123456";
    $usuario["nombres"] = "Prueba 1";
    $usuario["apellidos"] = "Prueba 11";
    $usuario["telefono"] = "1245789";
    $usuario["direccion"] = "Calle 8 # 78 -12 ";
    $usuario["email"] = "prueba1@prueba1.com";
    $usuario["fecha_nacimiento"] = "12/05/2016";
    $usuario["fecha_registro"] = "12/05/2016";
    $usuario["id_barrio"]["nombre_barrio"] = "Sin Barrio";
    $usuario["id_tipo_documento"]["nombre_tipo_documento"] = "Cedula Ciudadania";
    $obj[] = $usuario;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = "El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function cargarPerfil() {
    global $object_json;

    $obj = array();

    $perfil = array();
    $perfil["id_perfil"] = "1";
    $perfil["nombre_perfil"] = "Acudiente";
    $obj[] = $perfil;

    $perfil = array();
    $perfil["id_perfil"] = "2";
    $perfil["nombre_perfil"] = "Secretaria";
    $obj[] = $perfil;

    $perfil = array();
    $perfil["id_perfil"] = "3";
    $perfil["nombre_perfil"] = "Colegio";
    $obj[] = $perfil;

    $perfil = array();
    $perfil["id_perfil"] = "4";
    $perfil["nombre_perfil"] = "Administrador";
    $obj[] = $perfil;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

function cargarColegio() {
    global $object_json;

    $obj = array();

    $colegio = array();
    $colegio["id_colegio"] = "1";
    $colegio["nombre_colegio"] = "Colegio 1";
    $colegio["direccion"] = "Calle 35 # 12 45";
    $colegio["telefono"] = "7845122";
    $colegio["fax"] = "8956321";
    $colegio["email"] = "colegio1@pruebacolegio1.com";
    $colegio["nombre_rector"] = "Juan Martinez";
    $colegio["id_estado"] = "1";
    $colegio["id_barrio"] = "1";
    $obj[] = $colegio;

    $colegio = array();
    $colegio["id_colegio"] = "2";
    $colegio["nombre_colegio"] = "Colegio 2";
    $colegio["direccion"] = "Calle 35 # 12 45";
    $colegio["telefono"] = "7845122";
    $colegio["fax"] = "8956321";
    $colegio["email"] = "colegio1@pruebacolegio1.com";
    $colegio["nombre_rector"] = "Juan Martinez";
    $colegio["id_estado"] = "1";
    $colegio["id_barrio"] = "1";
    $obj[] = $colegio;

    $colegio = array();
    $colegio["id_colegio"] = "3";
    $colegio["nombre_colegio"] = "Colegio 3";
    $colegio["direccion"] = "Calle 35 # 12 45";
    $colegio["telefono"] = "7845122";
    $colegio["fax"] = "8956321";
    $colegio["email"] = "colegio1@pruebacolegio1.com";
    $colegio["nombre_rector"] = "Juan Martinez";
    $colegio["id_estado"] = "1";
    $colegio["id_barrio"] = "1";
    $obj[] = $colegio;

    $colegio = array();
    $colegio["id_colegio"] = "4";
    $colegio["nombre_colegio"] = "Colegio 4";
    $colegio["direccion"] = "Calle 35 # 12 45";
    $colegio["telefono"] = "7845122";
    $colegio["fax"] = "8956321";
    $colegio["email"] = "colegio1@pruebacolegio1.com";
    $colegio["nombre_rector"] = "Juan Martinez";
    $colegio["id_estado"] = "1";
    $colegio["id_barrio"] = "1";
    $obj[] = $colegio;

    $return = new stdClass();
    $return->data = array();

    if (is_array($obj)) {
        $return->success = true;
        $return->errorMessage = "";
        $return->data = $obj;
    } else {
        $return->success = false;
        $return->errorMessage = " El momento al realizar la transacion";
        $return->data = array();
    }

    //Encode the stdClass object containing information and return data as a json string
    $json = json_encode($return);

    //Return the json string to the JavaScript
    echo $json;
}

/* function () {
  global $object_json;

  $obj = array();

  $return = new stdClass();
  $return->data = array();

  if (is_array($obj)) {
  $return->success = true;
  $return->errorMessage = "";
  $return->data['usuario'] = $obj;
  } else {
  $return->success = false;
  $return->errorMessage = " El momento al realizar la transacion";
  $return->data['usuario'] = array();
  }

  //Encode the stdClass object containing information and return data as a json string
  $json = json_encode($return);

  //Return the json string to the JavaScript
  echo $json;
  } */
?>
