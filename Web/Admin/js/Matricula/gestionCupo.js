var cuposCollection;
var jtable;

/* Eilimina el cupo */
function eliminarCupo() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("cupoSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_cupo: vacios(data_user.idCupo)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "CupoService", methodName: "eliminarCupo", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los cupos consultados por la busqueda*/
function listarCupos() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_cupo: vacios($('#txtBuscarIdCupo').val()).toLocaleLowerCase(), fecha_creacion: vacios($('#txtBuscarFechaCreacion').val()), cedula: vacios($('#txtBuscarCedula').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "CupoService", methodName: "listarCupo", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            cuposCollection = dataMiembros;
            //var jtable = $('#tblCupo').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var cupo = v;
                jtable.oApi._fnAddData(oSettings, [cupo.idCupo, cupo.fechaRegistro, cupo.idAlumno, cupo.alumno, cupo.nivel, cupo.jornada, cupo.estado]);
            });
            
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarCupo() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Matricula/formularioCupo.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosCupo(dataCupo) {
    $.each(cuposCollection, function (k, v) {
        var cupo = v;
        if (dataCupo == cupo.idCupo) {
            if ($.cookie("cupoSelected") != null)
                $.removeCookie("cupoSelected");
            $.cookie("cupoSelected", JSON.stringify(cupo));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Cupo") {
            permiso_crear = true;
        }
        if (permiso == "Editar Cupo") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Cupo") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearCupo').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarCupo').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarCupo').hide();
    }
}

function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

$(document).ready(function () {

    if ($.cookie("cupoSelected") != null)
        $.removeCookie("cupoSelected");

    validar_permisos();

    var table = $('#tblCupo').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblCupo').dataTable();
    
    $("#favoriteFoodTable .deleteFile").on("click",function() {
        var tr = $(this).closest('tr');
        tr.css("background-color","#FF3700");
        tr.fadeOut(400, function(){
            tr.remove();
        });
        return false;
    });

    $('#tblCupo tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarCupos();
            $('#tblCupo tbody').find('tr').removeClass('selected');
            $.removeCookie("cupoSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarCupos();
            $('#tblCupo tbody').find('tr').removeClass('selected');
            $.removeCookie("cupoSelected");
        });

        if (typeof data != "undefined")
            cargarDatosCupo(data[0]);
    });

    $('#btnCrearCupo').click(function (event) {
        $.removeCookie("cupoSelected");
        editarCupo();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarCupo').click(function (event) {
        if ($.cookie("cupoSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el cupo que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarCupo();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarCupo').click(function (event) {
        if ($.cookie("cupoSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el cupo que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("cupoSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Cupo</h4> \n\
<p>¿Desea Eliminar el cupo ' + data_user["idCupo"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarCupo').click(function (event) {
        eliminarCupo();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdCupo').val() == "" && $('#txtBuscarFechaCreacion').val() == "" && $('#txtBuscarCedula').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarCupos();
    });
});