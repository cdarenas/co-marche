var files = []

/* Guarda o Actualiza la data del usuarioDocumento */
function gestionarDocumento(acutaliza) {
    var object, ajax;

    var nivel = [];
    var jornada = [];
    var sede = [];

    object = {
        id_documento: vacios($('#txt_id_documento').val()),
        nombre_documento: vacios($('#txt_nombre_documento').val()),
        archivos: files
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "DocumentoService", methodName: "gestionarDocumento", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function handleFileSelect(evt) {
    var array_files;
    evt.stopPropagation();
    evt.preventDefault();

    $('#list tbody').empty();
    $('#drop_zone').removeClass('_hover');
    $('#drop_zone').addClass('_drag');
    files = [];

    if (evt.type == "change") {
        array_files = evt.target.files;
    } else if (evt.type == "drop") {
        array_files = evt.dataTransfer.files;
    } else
        return;

    $.each(array_files, function (index, file) {
        var continuo = false;

        if (file.size <= 0)
            return true;

        $.each(files, function (i, obj) {
            var texto = convertString(file.name);
            if (obj.filename == texto && obj.size == file.size) {
                continuo = true;
                return false;
            }
        });

        if (continuo)
            return true;

        var fileReader = new FileReader();
        fileReader.onload = (function (file) {
            return function (e) {
                if (files.length == 1)
                    return true;

                var texto = convertString(file.name);
                var tr = ['<tr><td>', texto, '</th><td style="text-align: center">', file.type || 'n/a', '</td><td style="text-align: center">', file.size, '</td><td style="text-align: center">', file.lastModifiedDate.toLocaleDateString(), '</td><td><button type="button" id="" item="', texto, '" tipo="data" size="', file.size, '" class="removebutton">Eliminar Archivo</button></td></tr>'].join('');
                $(tr).appendTo($("#list"));
            };
        })(file);

        fileReader.readAsDataURL(file);
        fileReader.onloadend = function () {
            if (files.length == 1)
                return true;

            var texto = convertString(file.name);
            var base64data = fileReader.result;
            base64data = base64data.replace("data:", "");
            base64data = base64data.replace(";base64,", "");
            files.push({filename: texto, bytes64: base64data, size: file.size, lastModified: file.lastModifiedDate.toLocaleDateString(), typeFile: "file"});
            $("#txt_nombre_documento").val(texto);
        };

    });

    $("#list").fadeIn(400, function () {
        $(this).show();
    });

    $('#drop_zone').removeClass('_drag');
}


function selectFileSelect() {
    var array_files;

    $('#list tbody').empty();
    $('#drop_zone').removeClass('_hover');
    $('#drop_zone').addClass('_drag');
    files = [];

    var data = JSON.parse($.cookie("documentoSelected"));

    if (files.length == 1)
        return true;

    var texto = convertString(data.nombreDocumento);
    var tr = ['<tr><td>', texto, '</th><td style="text-align: center"></td><td style="text-align: center">', data.tamanio, '</td><td style="text-align: center">', data.fechaRegistro, '</td><td><button type="button" item="', texto, '" tipo="link" size="', data.tamanio, '" id="', data.idDocumento, '" class="removebutton">Eliminar Archivo</button></td></tr>'].join('');
    $(tr).appendTo($("#list"));
    files.push({filename: texto, bytes64: "", size: data.size, lastModified: data.fecha_registro, typeFile: "link"});
    $("#txt_nombre_documento").val(texto);

    $("#list").fadeIn(400, function () {
        $(this).show();
    });

    $('#drop_zone').removeClass('_drag');
}


function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    $('#drop_zone').addClass('_hover');
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function handleDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    $('#drop_zone').removeClass('_hover');
}

function convertString(texto) {
    if (vacios(texto) == "")
        return texto

    while (texto.indexOf(" ") > -1) {
        texto = texto.replace(" ", "_");
    }
    return texto.toLowerCase();
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}
$(document).ready(function () {
    var data_user, actualizar;

    $.event.props.push('dataTransfer');

    $('#drop_zone').on('dragover', handleDragOver)
            .on('drop', handleFileSelect)
            .on('dragleave', handleDragLeave)
            .click(function () {
                $('#my_file').click();
            });

    $('#my_file').change(handleFileSelect);

    $("#list").hide();
    $("#list").on("click", "button.removebutton", function () {
        var nombre_archivo = $(this).attr('item');
        var size = $(this).attr('size');
        var tipo = $(this).attr('tipo');
        var id_documento = $(this).attr('id');

        var tr = $(this).closest('tr');
        tr.css("background-color", "#FF3700");
        tr.fadeOut(400, function () {
            tr.remove();

            files = $.grep(files, function (value) {
                return value.filename != nombre_archivo && value.size != size;
            });
        });

        return false;
    });

    $('#my_file').change(handleFileSelect);
    $('#txt_id_documento').prop('disabled', true);
    $('#txt_nombre_documento').prop('disabled', true);

    if ($.cookie("documentoSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("documentoSelected"));

        $('#titulo_modulo').html('Actualizacion de Documento');
        $('#txt_id_documento').val(vacios(data_user["idDocumento"]));
        $('#txt_nombre_documento').val(vacios(data_user["nombreDocumento"]));

        $("#btnGuardarData").html('Actualizar Documento');
        $("#contUserRegister").hide();
        selectFileSelect();
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Documento");
        $("#btnGuardarData").html('Guardar');

        $("#contUserRegister").show();
    }

    $('#btnGuardarData').click(function (event) {

        if ($('#txt_email').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el email del documento',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_nombre_documento').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre del Documento',
                styling: 'bootstrap3'});
            return;
        }

        if (files.length <= 0) {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta adjuntar los archivos',
                styling: 'bootstrap3'});
            return;
        }

        gestionarDocumento(actualizar ? "true" : "false");
    });
});


