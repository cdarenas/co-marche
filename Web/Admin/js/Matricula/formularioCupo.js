var localidad, barrio, selectedLocalidad;
var files = [];


/* Guarda o Actualiza la data del usuarioCupo */
function gestionarCupo(acutaliza) {
    var object, ajax;

    var jornada = [];
    var nivel = [];
    var alumno = [];
    var estado = [];


    $.each($("#txt_jornada option:selected"), function () {
        jornada.push($(this).val());
    });

    $.each($("#txt_nivel option:selected"), function () {
        nivel.push($(this).val());
    });

    $.each($("#txt_alumno option:selected"), function () {
        alumno.push($(this).val());
    });

    $.each($("#txt_estado option:selected"), function () {
        estado.push($(this).val());
    });


    object = {
        id_cupo: vacios($('#txt_id_cupo').val()),
        alumno: alumno.join(","),
        jornada: jornada.join(","),
        estado: estado.join(","),
        nivel: nivel.join(","),
        archivos: files
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "CupoService", methodName: "gestionarCupo", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Estado*/
function cargarEstado() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_tabla: 3};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "EstadoService", methodName: "listarEstado", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idEstado;
                }
                $("#txt_estado").append("<option value=\"" + data.idEstado + "\">" + data.nombreEstado + "</option>");
            });

            if ($.cookie("cupoSelected") != null) {
                var data_user = JSON.parse($.cookie("cupoSelected"));
                selectItem("#txt_estado", data_user["idEstado"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_estado", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar estados - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Jornada*/
function cargarJornada() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_jornada: "", nombre_jornada: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "JornadaService", methodName: "listarJornada", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idJornada;
                }
                $("#txt_jornada").append("<option value=\"" + data.idJornada + "\">" + data.nombreJornada + "</option>");
            });

            if ($.cookie("cupoSelected") != null) {
                var data_user = JSON.parse($.cookie("cupoSelected"));
                selectItem("#txt_jornada", data_user["idJornada"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_jornada", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar jornadas - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Nivel*/
function cargarNivel() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_nivel: "", nombre_nivel: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "listarNivel", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idNivel;
                }
                $("#txt_nivel").append("<option value=\"" + data.idNivel + "\">" + data.nombreNivel + "</option>");
            });

            if ($.cookie("cupoSelected") != null) {
                var data_user = JSON.parse($.cookie("cupoSelected"));
                selectItem("#txt_nivel", data_user["idNivel"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_nivel", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar niveles - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Alumno*/
function cargarAlumno() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {cedula: "", nombres: "", email: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuarioAlumno", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.cedula;
                }
                $("#txt_alumno").append("<option value=\"" + data.cedula + "\">" + data.nombres + " " + data.apellidos + "</option>");
            });

            if ($.cookie("cupoSelected") != null) {
                var data_user = JSON.parse($.cookie("cupoSelected"));
                selectItem("#txt_alumno", data_user["idAlumno"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_alumno", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los Alumnos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Eilimina el documento cupo */
function eliminarDocumentoCupo(id_documento) {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_documento: id_documento};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "CupoService", methodName: "eliminarDocumentoCupo", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function convertString(texto) {
    if (vacios(texto) == "")
        return texto

    while (texto.indexOf(" ") > -1) {
        texto = texto.replace(" ", "_");
    }
    return texto.toLowerCase();
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function handleFileSelect(evt) {
    var array_files;
    evt.stopPropagation();
    evt.preventDefault();

    $('#list tbody').empty();
    $('#drop_zone').removeClass('_hover');
    $('#drop_zone').addClass('_drag');
    files = [];

    if (evt.type == "change") {
        array_files = evt.target.files;
    } else if (evt.type == "drop") {
        array_files = evt.dataTransfer.files;
    } else
        return;

    $.each(array_files, function (index, file) {
        var continuo = false;

        if (file.size <= 0)
            return true;

        $.each(files, function (i, obj) {
            var texto = convertString(file.name);
            if (obj.filename == texto && obj.size == file.size) {
                continuo = true;
                return false;
            }
        });

        if (continuo)
            return true;

        var fileReader = new FileReader();
        fileReader.onload = (function (file) {
            return function (e) {

                var texto = convertString(file.name);
                var tr = ['<tr><td>', texto, '</th><td style="text-align: center">', file.type || 'n/a', '</td><td style="text-align: center">', file.size, '</td><td style="text-align: center">', file.lastModifiedDate.toLocaleDateString(), '</td><td><button type="button" id="" item="', texto, '" tipo="data" size="', file.size, '" class="removebutton">Eliminar Archivo</button></td></tr>'].join('');
                $(tr).appendTo($("#list"));
            };
        })(file);

        fileReader.readAsDataURL(file);
        fileReader.onloadend = function () {
            var base64data = fileReader.result;
            var texto = convertString(file.name);
            base64data = base64data.replace("data:", "");
            base64data = base64data.replace(";base64,", "");
            files.push({filename: texto, bytes64: base64data, size: file.size, lastModified: file.lastModifiedDate.toLocaleDateString(), typeFile: "file"});
        };
    });

    $("#list").fadeIn(400, function () {
        $(this).show();
    });

    $('#drop_zone').removeClass('_drag');
}


function selectFileSelect() {
    var array_files;

    $('#list tbody').empty();
    $('#drop_zone').removeClass('_hover');
    $('#drop_zone').addClass('_drag');
    files = [];

    var data = JSON.parse($.cookie("cupoSelected"));

    $.each(data["documentos"], function (index, file) {
        var texto = convertString(data.nombre_documento);
        var tr = ['<tr><td>', texto, '</th><td style="text-align: center"></td><td style="text-align: center">', file.size, '</td><td style="text-align: center">', file.fecha_registro, '</td><td><button type="button" item="', texto, '" tipo="link" size="', file.size, '" id="', file.id_documento, '" class="removebutton">Eliminar Archivo</button></td></tr>'].join('');
        $(tr).appendTo($("#list"));
        files.push({filename: texto, bytes64: "", size: file.size, lastModified: file.fecha_registro, typeFile: "link"});
    });

    $("#list").fadeIn(400, function () {
        $(this).show();
    });

    $('#drop_zone').removeClass('_drag');
}


function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    $('#drop_zone').addClass('_hover');
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function handleDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    $('#drop_zone').removeClass('_hover');
}

$(document).ready(function () {
    var data_user, actualizar;

    $.event.props.push('dataTransfer');

    $('#drop_zone').on('dragover', handleDragOver)
            .on('drop', handleFileSelect)
            .on('dragleave', handleDragLeave)
            .click(function () {
                $('#my_file').click();
            });

    $('#my_file').change(handleFileSelect);

    $("#list").hide();
    $("#list").on("click", "button.removebutton", function () {
        var nombre_archivo = $(this).attr('item');
        var size = $(this).attr('size');
        var tipo = $(this).attr('tipo');
        var id_documento = $(this).attr('id');

        var tr = $(this).closest('tr');
        tr.css("background-color", "#FF3700");
        tr.fadeOut(400, function () {
            tr.remove();
            if (tipo == "link") {
                eliminarDocumentoCupo(id_documento)
            }
            files = $.grep(files, function (value) {
                return value.filename != nombre_archivo && value.size != size;
            });
        });

        return false;
    });

    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };
    $(".selectFormulario").select2();
    $('#txt_id_cupo').prop('disabled', true);

    cargarEstado();
    cargarJornada();
    cargarNivel();
    cargarAlumno();

    if ($.cookie("cupoSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("cupoSelected"));

        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_id_cupo').val(vacios(data_user["idCupo"]));

        $("#btnGuardarData").html('Actualizar Cupo');
        $("#contUserRegister").hide();
        selectFileSelect();
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Cupo");
        $("#btnGuardarData").html('Guardar');

        $("#contUserRegister").show();
    }

    $('#btnGuardarData').click(function (event) {
        if (files.length <= 0 && actualizar == "false") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta adjuntar los archivos',
                styling: 'bootstrap3'});
            return;
        }

        gestionarCupo(actualizar ? "true" : "false");
    });
});


