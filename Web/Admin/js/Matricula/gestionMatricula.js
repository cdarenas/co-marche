var matriculasCollection;
var jtable;

/* Eilimina el matricula */
function eliminarMatricula() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("matriculaSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_matricula: vacios(data_user.idMatricula)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "MatriculaService", methodName: "eliminarMatricula", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los matriculas consultados por la busqueda*/
function listarMatriculas() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_matricula: vacios($('#txtBuscarIdMatricula').val()).toLocaleLowerCase(), fecha_creacion: vacios($('#txtBuscarFechaCreacion').val()), id_cupo: vacios($('#txtBuscarIdCupo').val()).toLocaleLowerCase(), id_colegio: vacios($('#txtBuscarColegio option:selected').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "MatriculaService", methodName: "listarMatricula", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            matriculasCollection = dataMiembros;
            //var jtable = $('#tblMatricula').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var matricula = v;
                jtable.oApi._fnAddData(oSettings, [matricula.idMatricula, matricula.fechaRegistro, matricula.cupo, matricula.alumno, matricula.colegio, matricula.nivel, matricula.jornada, matricula.sede]);
            });
            
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Colegio*/
function cargarColegio() {
    var object, ajax;

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    object = {id_colegio: "", nombre_colegio: "", localidad: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ColegioService", methodName: "listarColegio", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;                
                if (primero == "") {
                    $("#txtBuscarColegio").append("<option value=\"\"></option>");
                    primero = "";
                }                
                $("#txtBuscarColegio").append("<option value=\"" + data.idColegio + "\">" + data.nombreColegio + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txtBuscarColegio", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            deshabilitar_funciones();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los colegios - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        deshabilitar_funciones();
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarMatricula() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Matricula/formularioMatricula.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosMatricula(dataMatricula) {
    $.each(matriculasCollection, function (k, v) {
        var matricula = v;
        if (dataMatricula == matricula.idMatricula) {
            if ($.cookie("matriculaSelected") != null)
                $.removeCookie("matriculaSelected");
            $.cookie("matriculaSelected", JSON.stringify(matricula));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Matricula") {
            permiso_crear = true;
        }
        if (permiso == "Editar Matricula") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Matricula") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearMatricula').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarMatricula').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarMatricula').hide();
    }
}

function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

$(document).ready(function () {

    if ($.cookie("matriculaSelected") != null)
        $.removeCookie("matriculaSelected");

    $(".js-example-basic-single").select2();
    cargarColegio();

    validar_permisos();

    var table = $('#tblMatricula').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblMatricula').dataTable();
    
    $("#favoriteFoodTable .deleteFile").on("click",function() {
        var tr = $(this).closest('tr');
        tr.css("background-color","#FF3700");
        tr.fadeOut(400, function(){
            tr.remove();
        });
        return false;
    });

    $('#tblMatricula tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarMatriculas();
            $('#tblMatricula tbody').find('tr').removeClass('selected');
            $.removeCookie("matriculaSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarMatriculas();
            $('#tblMatricula tbody').find('tr').removeClass('selected');
            $.removeCookie("matriculaSelected");
        });

        if (typeof data != "undefined")
            cargarDatosMatricula(data[0]);
    });

    $('#btnCrearMatricula').click(function (event) {
        $.removeCookie("matriculaSelected");
        editarMatricula();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarMatricula').click(function (event) {
        if ($.cookie("matriculaSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el matricula que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarMatricula();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarMatricula').click(function (event) {
        if ($.cookie("matriculaSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el matricula que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("matriculaSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Matricula</h4> \n\
<p>¿Desea Eliminar el matricula ' + data_user["idMatricula"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarMatricula').click(function (event) {
        eliminarMatricula();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdMatricula').val() == "" && $('#txtBuscarFechaCreacion').val() == "" && $('#txtBuscarIdCupo').val() == "" && $('#txtBuscarColegio option:selected').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarMatriculas();
    });
});