var matriculaCollection;
var jtable;

/* Eilimina el documento */
function eliminarDocumento() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("documentoSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_documento: vacios(data_user.idDocumento)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "DocumentoService", methodName: "eliminarDocumento", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los documentos consultados por la busqueda*/
function listarDocumentos() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_documento: vacios($('#txtBuscarIdDocumento').val()).toLocaleLowerCase(), nombre_documento: vacios($('#txtBuscarNombreDocumento').val()).toLocaleLowerCase(), localidad: vacios($('#txtBuscarLocalidad').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "DocumentoService", methodName: "listarDocumento", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            matriculaCollection = dataMiembros;
            //var jtable = $('#tblDocumento').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var documento = v;
                jtable.oApi._fnAddData(oSettings, [documento.idDocumento, documento.nombreDocumento, documento.tamanio, documento.fechaRegistro, documento.extension]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarDocumento() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Matricula/formularioDocumento.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosDocumento(docu) {

    $.each(matriculaCollection, function (k, v) {
        var documento = v;
        if (docu == documento.idDocumento) {
            if ($.cookie("documentoSelected") != null)
                $.removeCookie("documentoSelected");
            $.cookie("documentoSelected", JSON.stringify(documento));
            return;
        }
    });
}

function deshabilitar_funciones() {
    new PNotify({
        title: 'Error!',
        text: 'No hay Localidades para gestionar los documentos asociados',
        type: 'error',
        styling: 'bootstrap3'});

    $('#btnBuscar').prop('disabled', true);
    $('#btnEliminarDocumento').prop('disabled', true);
    $('#btnActualizarDocumento').prop('disabled', true);
    $('#btnCrearDocumento').prop('disabled', true);
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Documento") {
            permiso_crear = true;
        }
        if (permiso == "Editar Documento") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Documento") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearDocumento').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarDocumento').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarDocumento').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("documentoSelected") != null)
        $.removeCookie("documentoSelected");

    validar_permisos();

    var table = $('#tblDocumento').DataTable({
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblDocumento').dataTable();

    $('#tblDocumento tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarDocumentos();
            $('#tblDocumento tbody').find('tr').removeClass('selected');
            $.removeCookie("documentoSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarDocumentos();
            $('#tblDocumento tbody').find('tr').removeClass('selected');
            $.removeCookie("documentoSelected");
        });

        if (typeof data != "undefined")
            cargarDatosDocumento(data[0]);
    });

    $('#btnCrearDocumento').click(function (event) {
        $.removeCookie("documentoSelected");
        editarDocumento();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarDocumento').click(function (event) {
        if ($.cookie("documentoSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el documento que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarDocumento();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarDocumento').click(function (event) {
        if ($.cookie("documentoSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el documento que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("documentoSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Documento</h4> \n\
<p>¿Desea Eliminar el documento ' + data_user["nombreDocumento"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarDocumento').click(function (event) {
        eliminarDocumento();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdDocumento').val() == "" && $('#txtBuscarNombreDocumento').val() == "" && $('#txtBuscarLocalidad option:selected').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarDocumentos();
    });
});