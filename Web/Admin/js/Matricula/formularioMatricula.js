var localidad, barrio, selectedLocalidad;
var files = [];


/* Guarda o Actualiza la data del usuarioMatricula */
function gestionarMatricula(acutaliza) {
    var object, ajax;

    var jornada = [];
    var nivel = [];
    var cupo = [];
    var sede = [];
    var colegio = [];

    $.each($("#txt_jornada option:selected"), function () {
        jornada.push($(this).val());
    });

    $.each($("#txt_nivel option:selected"), function () {
        nivel.push($(this).val());
    });

    $.each($("#txt_cupo option:selected"), function () {
        cupo.push($(this).val());
    });

    $.each($("#txt_sede option:selected"), function () {
        sede.push($(this).val());
    });

    $.each($("#txt_colegio option:selected"), function () {
        colegio.push($(this).val());
    });


    object = {
        id_matricula: vacios($('#txt_id_matricula').val()),
        cupo: cupo.join(","),
        jornada: jornada.join(","),
        sede: sede.join(","),
        colegio: colegio.join(","),
        nivel: nivel.join(",")
    };

    console.log(object);

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "MatriculaService", methodName: "gestionarMatricula", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Sede*/
function cargarSede() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_sede: "", nombre_sede: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "SedeService", methodName: "listarSede", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idSede;
                }
                $("#txt_sede").append("<option value=\"" + data.idSede + "\">" + data.nombreSede + "</option>");
            });

            if ($.cookie("matriculaSelected") != null) {
                var data_user = JSON.parse($.cookie("matriculaSelected"));
                selectItem("#txt_sede", data_user["idSede"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_sede", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar sedes - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Jornada*/
function cargarJornada() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_jornada: "", nombre_jornada: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "JornadaService", methodName: "listarJornada", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idJornada;
                }
                $("#txt_jornada").append("<option value=\"" + data.idJornada + "\">" + data.nombreJornada + "</option>");
            });

            if ($.cookie("matriculaSelected") != null) {
                var data_user = JSON.parse($.cookie("matriculaSelected"));
                selectItem("#txt_jornada", data_user["idJornada"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_jornada", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar jornadas - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Nivel*/
function cargarNivel() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_nivel: "", nombre_nivel: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "listarNivel", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idNivel;
                }
                $("#txt_nivel").append("<option value=\"" + data.idNivel + "\">" + data.nombreNivel + "</option>");
            });

            if ($.cookie("matriculaSelected") != null) {
                var data_user = JSON.parse($.cookie("matriculaSelected"));
                selectItem("#txt_nivel", data_user["idNivel"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_nivel", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar niveles - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Cupo*/
function cargarCupo() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_cupo: "", fecha_creacion: "", cedula: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "CupoService", methodName: "listarCupo", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idCupo;
                }
                $("#txt_cupo").append("<option value=\"" + data.idCupo + "\">Cupo Nro " + data.idCupo + " - (" + data.fechaRegistro + ")</option>");
            });

            if ($.cookie("matriculaSelected") != null) {
                var data_user = JSON.parse($.cookie("matriculaSelected"));
                selectItem("#txt_cupo", data_user["idCupo"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_cupo", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los Alumnos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}


/* Cargar Colegio*/
function cargarColegio() {
    var object, ajax;

    object = {id_colegio: "", nombre_colegio: "", localidad: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ColegioService", methodName: "listarColegio", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idColegio;
                }
                $("#txt_colegio").append("<option value=\"" + data.idColegio + "\">" + data.nombreColegio + "</option>");
            });

            if ($.cookie("matriculaSelected") != null) {
                var data_user = JSON.parse($.cookie("matriculaSelected"));
                selectItem("#txt_colegio", data_user["idColegio"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_colegio", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar tipos documentos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, actualizar;

    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };
    $(".selectFormulario").select2();
    $('#txt_id_matricula').prop('disabled', true);

    cargarSede();
    cargarJornada();
    cargarNivel();
    cargarCupo();
    cargarColegio();

    if ($.cookie("matriculaSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("matriculaSelected"));

        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_id_matricula').val(vacios(data_user["idMatricula"]));

        $("#btnGuardarData").html('Actualizar Matricula');
        $("#contUserRegister").hide();
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Matricula");
        $("#btnGuardarData").html('Guardar');

        $("#contUserRegister").show();
    }

    $('#btnGuardarData').click(function (event) {
        if (files.length <= 0 && actualizar == "false") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta adjuntar los archivos',
                styling: 'bootstrap3'});
            return;
        }

        gestionarMatricula(actualizar ? "true" : "false");
    });
});


