var localidad, barrio, selectedLocalidad;

/* Guarda o Actualiza la data del acudiente */
function gestonarAcudiente(acutaliza) {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    object = {
        id_tipo_documento: vacios($('#txt_tipo_documento option:selected').val()),
        cedula: vacios($('#txt_cedula').val()),
        nombres: vacios($('#txt_nombre').val()),
        apellidos: vacios($('#txt_apellidos').val()),
        direccion: vacios($('#txt_direccion').val()),
        telefono: vacios($('#txt_telefono').val()),
        genero: vacios($('#txt_genero option:selected').val()),
        id_barrio: vacios($('#txt_barrio option:selected').val()),
        email: vacios($('#txt_email').val()),
        fecha_nacimiento: formatDate(vacios($('#txt_fecha_nacimiento').val())),
        usuario: vacios($('#txt_usuario').val()),
        contrasena: vacios($('#txt_password').val()),
        id_perfil: "1"
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "gestionarUsuario", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Tipo Documento*/
function cargarTipoDocumento() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "TipoDocumentoService", methodName: "listarTipoDocumento"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idTipoDocumento;
                }
                $("#txt_tipo_documento").append("<option value=\"" + data.idTipoDocumento + "\">" + data.nombreTipoDocumento + "</option>");
            });

            if ($.cookie("acudienteSelected") != null) {
                var data_user = JSON.parse($.cookie("acudienteSelected"));
                selectItem("#txt_tipo_documento", data_user["idTipoDocumento"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_tipo_documento", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar tipos documentos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar localidades*/
function cargarDataLocalidad() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = [906];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LocalidadService", methodName: "listarLocalidad", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            localidad = response.data;
            cargarLocalidad();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar las localidades - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Barrios*/
function cargarDataBarrio() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = [selectedLocalidad];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "BarrioService", methodName: "listarBarrio", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            barrio = response.data;
            cargarBarrio();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los barrios ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* llena el array del genero*/
function cargarGenero() {

    var tipo = [{idGenero: "Masculino", nombreGenero: "Masculino"}, {idGenero: "Femenino", nombreGenero: "Femenino"}];
    var primero = "";

    $.each(tipo, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idGenero;
        }
        $("#txt_genero").append("<option value=\"" + data.idGenero + "\">" + data.nombreGenero + "</option>");
    });

    if ($.cookie("usuarioSelected") != null) {
        var data_user = JSON.parse($.cookie("usuarioSelected"));
        selectItem("#txt_genero", data_user["genero"]);
    } else if(tipo.length > 0){
        selectItem("#txt_genero", primero);
    }
}

function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function cargarLocalidad() {
    var primero = "";
    $.each(localidad, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idLocalidad;
        }
        $("#txt_localidad").append("<option value=\"" + data.idLocalidad + "\">" + data.nombreLocalidad + "</option>");
    });

    if ($.cookie("acudienteSelected") != null) {
        var data_user = JSON.parse($.cookie("acudienteSelected"));
        selectItem("#txt_localidad", data_user["idLocalidad"]);
    } else if (localidad.length > 0) {
        selectItem("#txt_localidad", primero);
    }
}

function cargarBarrio() {
    $("#txt_barrio").find('option').remove();
    $('#txt_barrio').prop('disabled', false);
    var primero = "";

    $.each(barrio, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idBarrio;
        }
        $("#txt_barrio").append("<option value=\"" + data.idBarrio + "\">" + data.nombreBarrio + "</option>");
    });

    if ($.cookie("acudienteSelected") != null) {
        var data_user = JSON.parse($.cookie("acudienteSelected"));
        selectItem("#txt_barrio", data_user["idBarrio"]);
    } else if (barrio.length > 0) {
        selectItem("#txt_barrio", primero);
    }

}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, actualizar;

    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    $(".selectFormulario").select2();
    $('#txt_barrio').prop('disabled', true);

    cargarDataLocalidad();
    cargarTipoDocumento();
    cargarGenero();

    if ($.cookie("acudienteSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("acudienteSelected"));

        $('#txt_cedula').prop('disabled', true);
        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_cedula').val(vacios(data_user["cedula"]));
        $('#txt_nombre').val(vacios(data_user["nombres"]));
        $('#txt_apellidos').val(vacios(data_user["apellidos"]));
        $('#txt_direccion').val(vacios(data_user["direccion"]));
        $('#txt_telefono').val(vacios(data_user["telefono"]));
        $('#txt_email').val(vacios(data_user["email"]));
        $('#txt_fecha_nacimiento').val(vacios(formatDate(data_user["fecha_nacimiento"])));

        $("#btnGuardarData").html('Actualizar');
        $("#contUserRegister").hide();

        selectedLocalidad = data_user["idLocalidad"];
        cargarDataBarrio();
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Acudiente");
        $("#btnGuardarData").html('Guardar');

        $('#txt_fecha_nacimiento').val(formatDate(""));
        $("#contUserRegister").show();
    }

    $("#txt_localidad").change(function () {
        selectedLocalidad = this.value;
        cargarDataBarrio();
    });

    $('#btnGuardarData').click(function (event) {

        if ($('#txt_cedula').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el documento',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_nombre').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_apellidos').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el apellido',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_direccion').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el direccion',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_telefono').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el telefono',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_password').val() != $('#txt_password_confirm').val()) {
            new PNotify({
                title: 'Advertencia!',
                text: " No coinciden las contraseñas, digitelas Nuevamentes ",
                styling: 'bootstrap3'});
            return;
        }
        
        gestonarAcudiente(actualizar ? "true" : "false");
    });
});


