var acudientesCollection;
var jtable;

/* Eilimina el usuario */
function eliminarUsuario() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("usuarioColegioSelected"));

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios(data_user.cedula)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "eliminarUsuario", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los acudientes consultados por la busqueda*/
function listarAcudientes() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios($('#txtBuscarCedula').val()).toLocaleLowerCase(), nombres: vacios($('#txtBuscarNombres').val()).toLocaleLowerCase(), email: vacios($('#txtBuscarEmail').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuarioAcudiente", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            acudientesCollection = dataMiembros;
            //var jtable = $('#tblAcudiente').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var acudiente = v;
                jtable.oApi._fnAddData(oSettings, [acudiente.cedula, acudiente.nombres, acudiente.apellidos, acudiente.direccion, acudiente.email, acudiente.id_tipo_documento]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function editarAcudiente() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Usuario/formularioAcudiente.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosAcudiente(documento) {
    $.each(acudientesCollection, function (k, v) {
        var acudiente = v;
        if (documento === acudiente.cedula) {
            if ($.cookie("acudienteSelected") !== null)
                $.removeCookie("acudienteSelected");
            $.cookie("acudienteSelected", JSON.stringify(acudiente));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Usuario Acudiente") {
            permiso_crear = true;
        }
        if (permiso == "Editar Usuario Acudiente") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Usuario Acudiente") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearAcudiente').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarAcudiente').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarAcudiente').hide();
    }
}


$(document).ready(function () {

    if ($.cookie("acudienteSelected") != null)
        $.removeCookie("acudienteSelected");

    validar_permisos();

    var table = $('#tblAcudiente').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblAcudiente').dataTable()

    $('#tblAcudiente tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarAcudientes();
            $('#tblAcudiente tbody').find('tr').removeClass('selected');
            $.removeCookie("acudienteSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarAcudientes();
            $('#tblAcudiente tbody').find('tr').removeClass('selected');
            $.removeCookie("acudienteSelected");
        });

        if (typeof data != "undefined")
            cargarDatosAcudiente(data[0]);
    });

    $('#btnCrearAcudiente').click(function (event) {
        $.removeCookie("acudienteSelected");
        editarAcudiente();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarAcudiente').click(function (event) {
        if ($.cookie("acudienteSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el acudiente que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarAcudiente();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarAcudiente').click(function (event) {
        if ($.cookie("acudienteSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el acudiente que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("acudienteSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Acudiente</h4> \n\
<p>¿Desea Eliminar el acudiente ' + data_user["nombres"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarAcudiente').click(function (event) {
        eliminarAcudiente();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarCedula').val() === "" && $('#txtBuscarNombres').val() === "" && $('#txtBuscarEmail').val() === "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarAcudientes();
    });
});


