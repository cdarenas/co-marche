var usuarioColegiosCollection;
var jtable;

/* Eilimina el usuarioColegio */
function eliminarUsuarioColegio() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("usuarioColegioSelected"));

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios(data_user.cedula)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "eliminarUsuario", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los usuarioColegios consultados por la busqueda*/
function listarUsuarioColegios() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios($('#txtBuscarCedula').val()).toLocaleLowerCase(), nombres: vacios($('#txtBuscarNombres').val()).toLocaleLowerCase(), email: vacios($('#txtBuscarEmail').val()).toLocaleLowerCase(), colegio: vacios($('#txtBuscarColegio option:selected').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuarioColegio", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            usuarioColegiosCollection = dataMiembros;
            //var jtable = $('#tblUsuarioColegio').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var usuarioColegio = v;
                jtable.oApi._fnAddData(oSettings, [usuarioColegio.cedula, usuarioColegio.nombres, usuarioColegio.apellidos, usuarioColegio.direccion, usuarioColegio.email, usuarioColegio.nombreColegio]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Colegio*/
function cargarColegio() {
    var object, ajax;

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    object = {id_colegio: "", nombre_colegio: "", localidad: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ColegioService", methodName: "listarColegio", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    $("#txtBuscarColegio").append("<option value=\"\"></option>");
                    primero = "";
                }     
                $("#txtBuscarColegio").append("<option value=\"" + data.idColegio + "\">" + data.nombreColegio + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txtBuscarColegio", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            deshabilitar_funciones();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los colegios - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        deshabilitar_funciones();
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {

    var $example = $(id_select).select2();
    $example.val(item).trigger("change");
}

function editarUsuarioColegio() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Usuario/formularioUsuarioColegio.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosUsuarioColegio(documento) {
    $.each(usuarioColegiosCollection, function (k, v) {
        var usuarioColegio = v;
        if (documento == usuarioColegio.cedula) {
            if ($.cookie("usuarioColegioSelected") != null)
                $.removeCookie("usuarioColegioSelected");
            $.cookie("usuarioColegioSelected", JSON.stringify(usuarioColegio));
            return;
        }
    });
}

function deshabilitar_funciones() {
    new PNotify({
        title: 'Error!',
        text: 'No hay Colegios para gestionar los usuarios asociados',
        type: 'error',
        styling: 'bootstrap3'});

    $('#btnBuscar').prop('disabled', true);
    $('#btnEliminarUsuarioColegio').prop('disabled', true);
    $('#btnActualizarUsuarioColegio').prop('disabled', true);
    $('#btnCrearUsuarioColegio').prop('disabled', true);

}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Usuario Colegio") {
            permiso_crear = true;
        }
        if (permiso == "Editar Usuario Colegio") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Usuario Colegio") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearUsuarioColegio').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarUsuarioColegio').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarUsuarioColegio').hide();
    }
}

$(document).ready(function () {
    valColegio = true;

    if ($.cookie("usuarioColegioSelected") != null)
        $.removeCookie("usuarioColegioSelected");

    validar_permisos();

    $(".js-example-basic-single").select2();
    cargarColegio();

    var table = $('#tblUsuarioColegio').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblUsuarioColegio').dataTable();

    $('#tblUsuarioColegio tbody').on('click', 'tr', function () {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarUsuarioColegios();
            $('#tblUsuarioColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("usuarioColegioSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarUsuarioColegios();
            $('#tblUsuarioColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("usuarioColegioSelected");
        });

        if (typeof data != "undefined")
            cargarDatosUsuarioColegio(data[0]);
    });

    $('#btnCrearUsuarioColegio').click(function (event) {
        $.removeCookie("usuarioColegioSelected");
        editarUsuarioColegio();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarUsuarioColegio').click(function (event) {
        if ($.cookie("usuarioColegioSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el usuarioColegio que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarUsuarioColegio();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarUsuarioColegio').click(function (event) {
        if ($.cookie("usuarioColegioSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el usuarioColegio que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("usuarioColegioSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar UsuarioColegio</h4> \n\
<p>¿Desea Eliminar el usuarioColegio ' + data_user["nombres"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarUsuarioColegio').click(function (event) {
        eliminarUsuarioColegio();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarCedula').val() == "" && $('#txtBuscarNombres').val() == "" && $('#txtBuscarEmail').val() == "" && $('#txtBuscarColegio option:selected').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarUsuarioColegios();
    });

});


