var usuariosCollection;
var jtable;

/* Eilimina el usuario */
function eliminarUsuario() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("usuarioColegioSelected"));

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios(data_user.cedula)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "eliminarUsuario", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los usuarios consultados por la busqueda*/
function listarUsuarios() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios($('#txtBuscarCedula').val()).toLocaleLowerCase(), nombres: vacios($('#txtBuscarNombres').val()).toLocaleLowerCase(), email: vacios($('#txtBuscarEmail').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuario", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            usuariosCollection = dataMiembros;
            //var jtable = $('#tblUsuario').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var usuario = v;
                jtable.oApi._fnAddData(oSettings, [usuario.cedula, usuario.nombres, usuario.apellidos, usuario.direccion, usuario.email, usuario.nombreTipoDocumento]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function editarUsuario() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Usuario/formularioUsuario.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosUsuario(documento) {
    $.each(usuariosCollection, function (k, v) {
        var usuario = v;
        if (documento == usuario.cedula) {
            if ($.cookie("usuarioSelected") != null)
                $.removeCookie("usuarioSelected");
            $.cookie("usuarioSelected", JSON.stringify(usuario));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Usuario") {
            permiso_crear = true;
        }
        if (permiso == "Editar Usuario") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Usuario") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearUsuario').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarUsuario').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarUsuario').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("usuarioSelected") != null)
        $.removeCookie("usuarioSelected");

    validar_permisos();

    var table = $('#tblUsuario').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblUsuario').dataTable();

    $('#tblUsuario tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarUsuarios();
            $('#tblUsuario tbody').find('tr').removeClass('selected');
            $.removeCookie("usuarioSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarUsuarios();
            $('#tblUsuario tbody').find('tr').removeClass('selected');
            $.removeCookie("usuarioSelected");
        });

        if (typeof data != "undefined")
            cargarDatosUsuario(data[0]);
    });

    $('#btnCrearUsuario').click(function (event) {
        $.removeCookie("usuarioSelected");
        editarUsuario();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarUsuario').click(function (event) {
        if ($.cookie("usuarioSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el usuario que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarUsuario();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarUsuario').click(function (event) {
        if ($.cookie("usuarioSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el usuario que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("usuarioSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Usuario</h4> \n\
<p>¿Desea Eliminar el usuario ' + data_user["nombres"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarUsuario').click(function (event) {
        eliminarUsuario();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarCedula').val() == "" && $('#txtBuscarNombres').val() == "" && $('#txtBuscarEmail').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarUsuarios();
    });
});