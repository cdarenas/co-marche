var alumnosCollection;
var jtable;

/* Eilimina el alumno */
function eliminarAlumno() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("alumnoSelected"));

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios(data_user.cedula)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "eliminarUsuario", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los alumnos consultados por la busqueda*/
function listarAlumnos() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {cedula: vacios($('#txtBuscarCedula').val()).toLocaleLowerCase(), nombres: vacios($('#txtBuscarNombres').val()).toLocaleLowerCase(), email: vacios($('#txtBuscarEmail').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuarioAlumno", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            alumnosCollection = dataMiembros;
            //var jtable = $('#tblAlumno').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var alumno = v;
                jtable.oApi._fnAddData(oSettings, [alumno.cedula, alumno.nombres, alumno.apellidos, alumno.direccion, alumno.email, alumno.ultimoAnioCursado, alumno.nombreNivel, alumno.nombreAcudiente]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Acudiente*/
function cargarAcudiente() {
    var object, ajax;

    object = {cedula: "", nombres: "", email: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuarioAcudiente", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    $("#txtBuscarAcudiente").append("<option value=\"\"></option>");
                    primero = "";
                }     
                $("#txtBuscarAcudiente").append("<option value=\"" + data.cedula + "\">" + data.nombres+" "+data.apellidos + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txtBuscarAcudiente", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            deshabilitar_funciones();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los colegios - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        deshabilitar_funciones();
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarAlumno() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Usuario/formularioAlumno.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosAlumno(documento) {
    $.each(alumnosCollection, function (k, v) {
        var alumno = v;
        if (documento == alumno.cedula) {
            if ($.cookie("alumnoSelected") != null)
                $.removeCookie("alumnoSelected");
            $.cookie("alumnoSelected", JSON.stringify(alumno));
            return;
        }
    });
}

function deshabilitar_funciones() {
    new PNotify({
        title: 'Error!',
        text: 'No hay Acudientes para gestionar los usuarios asociados',
        type: 'error',
        styling: 'bootstrap3'});

    $('#btnBuscar').prop('disabled', true);
    $('#btnEliminarAlumno').prop('disabled', true);
    $('#btnActualizarAlumno').prop('disabled', true);
    $('#btnCrearAlumno').prop('disabled', true);

}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Usuario Alumno") {
            permiso_crear = true;
        }
        if (permiso == "Editar Usuario Alumno") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Usuario Alumno") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearAlumno').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarAlumno').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarAlumno').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("alumnoSelected") != null)
        $.removeCookie("alumnoSelected");

    validar_permisos();

    $(".js-example-basic-single").select2();
    cargarAcudiente();

    var table = $('#tblAlumno').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblAlumno').dataTable()

    $('#tblAlumno tbody').on('click', 'tr', function () {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarAlumnos();
            $('#tblAlumno tbody').find('tr').removeClass('selected');
            $.removeCookie("alumnoSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarAlumnos();
            $('#tblAlumno tbody').find('tr').removeClass('selected');
            $.removeCookie("alumnoSelected");
        });

        if (typeof data != "undefined")
            cargarDatosAlumno(data[0]);
    });

    $('#btnCrearAlumno').click(function (event) {
        $.removeCookie("alumnoSelected");
        editarAlumno();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarAlumno').click(function (event) {
        if ($.cookie("alumnoSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el alumno que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarAlumno();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarAlumno').click(function (event) {
        if ($.cookie("alumnoSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el alumno que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("alumnoSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Alumno</h4> \n\
<p>¿Desea Eliminar el alumno ' + data_user["nombres"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarAlumno').click(function (event) {
        eliminarAlumno();
    });

    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarCedula').val() == "" && $('#txtBuscarNombres').val() == "" && $('#txtBuscarEmail').val() == "" && $('#txtBuscarAcudiente option:selected').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarAlumnos();
    });
});


