var loginsCollection;
var jtable;

/* Eilimina el login */
function eliminarLogin() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("loginSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_login: vacios(data_user.idLogin)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "eliminarLogin", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los logins consultados por la busqueda*/
function listarLogins() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_login: vacios($('#txtBuscarIdLogin').val()).toLocaleLowerCase(),
        cedula: vacios($('#txtBuscarCedula').val()).toLocaleLowerCase(),
        usuario: vacios($('#txtBuscarUsuario').val()).toLocaleLowerCase(),
        id_perfil: vacios($('#txtBuscarPerfil option:selected').val()).toLocaleLowerCase(),
        id_estado: vacios($('#txtBuscarEstado option:selected').val()).toLocaleLowerCase()
    };
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "listarLogin", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            loginsCollection = dataMiembros;
            //var jtable = $('#tblLogin').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var login = v;
                jtable.oApi._fnAddData(oSettings, [login.idLogin, login.cedula, login.nombres, login.usuario, login.perfil, login.estado]);
            });

            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Perfil*/
function cargarPerfil() {
    var object, ajax;

    object = {id_perfil: "", nombre_perfil: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "PerfilService", methodName: "listarPerfil", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    $("#txtBuscarPerfil").append("<option value=\"\"></option>");
                    primero = "";
                }
                $("#txtBuscarPerfil").append("<option value=\"" + data.idPerfil + "\">" + data.nombrePerfil + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txtBuscarPerfil", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los perfiles - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Estado*/
function cargarEstado() {
    var object, ajax;

    object = {id_tabla: "1"};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "EstadoService", methodName: "listarEstado", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    $("#txtBuscarEstado").append("<option value=\"\"></option>");
                    primero = "";
                }
                $("#txtBuscarEstado").append("<option value=\"" + data.idEstado + "\">" + data.nombreEstado + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txtBuscarEstado", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los Estadoes - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}


function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function editarLogin() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Seguridad/formularioLogin.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosLogin(log) {
    $.each(loginsCollection, function (k, v) {
        var login = v;
        if (log == login.idLogin) {
            if ($.cookie("loginSelected") != null)
                $.removeCookie("loginSelected");
            $.cookie("loginSelected", JSON.stringify(login));
            return;
        }
    });
}

function selectItem(id_select, item) {

    var $example = $(id_select).select2();
    $example.val(item).trigger("change");
}


function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Login") {
            permiso_crear = true;
        }
        if (permiso == "Editar Login") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Login") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearLogin').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarLogin').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarLogin').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("loginSelected") != null)
        $.removeCookie("loginSelected");

    validar_permisos();

    $(".js-example-basic-single").select2();
    cargarPerfil();
    cargarEstado();

    var table = $('#tblLogin').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblLogin').dataTable();

    $('#tblLogin tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarLogins();
            $('#tblLogin tbody').find('tr').removeClass('selected');
            $.removeCookie("loginSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarLogins();
            $('#tblLogin tbody').find('tr').removeClass('selected');
            $.removeCookie("loginSelected");
        });

        if (typeof data != "undefined")
            cargarDatosLogin(data[0]);
    });

    $('#btnCrearLogin').click(function (event) {
        $.removeCookie("loginSelected");
        editarLogin();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarLogin').click(function (event) {
        if ($.cookie("loginSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el login que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarLogin();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarLogin').click(function (event) {
        if ($.cookie("loginSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el login que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("loginSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Login</h4> \n\
<p>¿Desea Eliminar el login ' + data_user["usuario"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarLogin').click(function (event) {
        eliminarLogin();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarCedula').val() == "" && $('#txtBuscarIdLogin').val() == "" && $('#Usuario').val() == "" && $('#txtBuscarPerfil option:selected').val() == "" && $('#txtBuscarEstado option:selected').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarLogins();
    });
});