var localidad, barrio, selectedLocalidad;
var files = [];


/* Guarda o Actualiza la data del usuarioLogin */
function gestionarLogin(acutaliza) {
    var object, ajax;

    object = {
        id_login: vacios($('#txt_id_login').val()),
        usuario: vacios($('#txt_user').val()),
        contrasena: vacios($('#txt_password').val()),
        id_perfil: vacios($('#txt_perfil option:selected').val()),
        id_estado: vacios($('#txt_estado option:selected').val()),
        id_usuario: vacios($('#txt_usuario option:selected').val())
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "gestionarLogin", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Estado*/
function cargarEstado() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_tabla: 3};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "EstadoService", methodName: "listarEstado", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idEstado;
                }
                $("#txt_estado").append("<option value=\"" + data.idEstado + "\">" + data.nombreEstado + "</option>");
            });

            if ($.cookie("loginSelected") != null) {
                var data_user = JSON.parse($.cookie("loginSelected"));
                selectItem("#txt_estado", data_user["idEstado"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_estado", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar estados - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Perfil*/
function cargarPerfil() {
    var object, ajax;

    object = {id_perfil: "", nombre_perfil: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "PerfilService", methodName: "listarPerfil", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idPerfil;
                }
                $("#txt_perfil").append("<option value=\"" + data.idPerfil + "\">" + data.nombrePerfil + "</option>");
            });

            if ($.cookie("loginSelected") != null) {
                var data_user = JSON.parse($.cookie("loginSelected"));
                selectItem("#txt_perfil", data_user["idPerfil"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_perfil", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los perfiles - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Usuario*/
function cargarUsuario() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {cedula: "", nombres: "", email: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "listarUsuario", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.cedula;
                }
                $("#txt_usuario").append("<option value=\"" + data.cedula + "\">" + data.nombres + " " + data.apellidos + "</option>");
            });

            if ($.cookie("loginSelected") != null) {
                var data_user = JSON.parse($.cookie("loginSelected"));
                selectItem("#txt_usuario", data_user["cedula"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_usuario", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los Usuarios - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, actualizar;

    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };
    $(".selectFormulario").select2();
    $('#txt_id_login').prop('disabled', true);

    cargarEstado();
    cargarPerfil();
    cargarUsuario();

    if ($.cookie("loginSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("loginSelected"));

        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_id_login').val(vacios(data_user["idLogin"]));
        $('#txt_user').val(vacios(data_user["usuario"]));
        $('#txt_password').val(vacios(data_user["contrasena"]));
        $('#txt_password_confirm').val(vacios(data_user["contrasena"]));

        $("#btnGuardarData").html('Actualizar Login');
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Login");
        $("#btnGuardarData").html('Guardar');

        $("#contUserRegister").show();
    }

    $('#btnGuardarData').click(function (event) {


        if ($('#txt_user').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el usuario',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_password').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar la contraseña',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_password_confirm').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar la confirmacion de la contraseña',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_password').val() != $('#txt_password_confirm').val()) {
            new PNotify({
                title: 'Advertencia!',
                text: " No coinciden las contraseñas, digitelas Nuevamentes ",
                styling: 'bootstrap3'});
            return;
        }

        gestionarLogin(actualizar ? "true" : "false");
    });
});


