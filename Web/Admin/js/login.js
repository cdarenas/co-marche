/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var localidad, barrio, selectedLocalidad;

/* Obtiene la data del usuario a consultar*/
function buscarUsuario() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = [$('#txtUsuario').val(), $('#txtContrasena').val()];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "autenticarUsuario", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            $.cookie("user", JSON.stringify(response.data));
            window.location.href = "principal.html";
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: "Ocurrio un error al momento de validar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}


/* Obtiene la data del usuario a consultar*/
function registrarUsuario() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_tipo_documento: vacios($('#txtTipoDocumentoReg option:selected').val()),
        cedula: vacios($('#txtCedulaReg').val()),
        nombres: vacios($('#txtNombreReg').val()),
        apellidos: vacios($('#txtApellidoReg').val()),
        direccion: vacios($('#txtDireccionReg').val()),
        telefono: vacios($('#txtTelefonoReg').val()),
        genero: vacios($('#txtGeneroReg option:selected').val()),
        id_barrio: vacios($('#txtBarrioReg option:selected').val()),
        email: vacios($('#txtEmailReg').val()),
        fecha_nacimiento: formatDate(vacios($('#txtFechaNacimientoReg').val())),
        usuario: vacios($('#txtUsuarioReg').val()),
        contrasena: vacios($('#txtContrasenaReg').val()),
        id_perfil: vacios($('#txtPerfilReg option:selected').val())
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "registrarUsuarioAdmin", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            new PNotify({
                title: 'Mensaje!',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: "Ocurrio un error al momento de validar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}


/* Cargar Perfil*/
function cargarPerfil() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback


    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "PerfilService", methodName: "listarPerfil"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idPerfil;
                }
                $("#txtPerfilReg").append("<option value=\"" + data.idPerfil + "\">" + data.nombrePerfil + "</option>");
            });
            if (tipo.length > 0) {
                selectItem("#txtPerfilReg", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar tipos documentos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Tipo Documento*/
function cargarTipoDocumento() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "TipoDocumentoService", methodName: "listarTipoDocumento"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idTipoDocumento;
                }
                $("#txtTipoDocumentoReg").append("<option value=\"" + data.idTipoDocumento + "\">" + data.nombreTipoDocumento + "</option>");
            });
            if (tipo.length > 0) {
                selectItem("#txtTipoDocumentoReg", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar tipos documentos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar localidades*/
function cargarDataLocalidad() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback


    //Hardcoded data values for demonstration purposes
    object = [906];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LocalidadService", methodName: "listarLocalidad", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            localidad = response.data;
            cargarLocalidad();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar las localidades - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Barrios*/
function cargarDataBarrio() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = [selectedLocalidad];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "BarrioService", methodName: "listarBarrio", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            barrio = response.data;
            cargarBarrio();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los barrios ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* llena el array del genero*/
function cargarGenero() {
    var tipo = [{idGenero: "Masculino", nombreGenero: "Masculino"}, {idGenero: "Femenino", nombreGenero: "Femenino"}];
    var primero = "";
    $.each(tipo, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idGenero;
        }
        $("#txtGeneroReg").append("<option value=\"" + data.idGenero + "\">" + data.nombreGenero + "</option>");
    });
    if (tipo.length > 0) {
        selectItem("#txtGeneroReg", primero);
    }
}

/* Obtiene la data del usuario a consultar*/
function recuperarPassword() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = [$('#txtEmail').val()];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "recuperarPassword", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            new PNotify({
                title: 'Mensaje!',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: "Ocurrio un error al momento de validar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function selectItem(id_select, item) {

    var $example = $(id_select).select2();
    $example.val(item).trigger("change");
}

function cargarLocalidad() {
    var primero = "";
    $.each(localidad, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idLocalidad;
        }
        $("#txtLocalidadReg").append("<option value=\"" + data.idLocalidad + "\">" + data.nombreLocalidad + "</option>");
    });

    if (localidad.length > 0) {
        selectItem("#txtLocalidadReg", primero);
    }
}

function cargarBarrio() {
    $("#txtBarrioReg").find('option').remove();
    $('#txtBarrioReg').prop('disabled', false);
    var primero = "";

    $.each(barrio, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idBarrio;
        }
        $("#txtBarrioReg").append("<option value=\"" + v.idBarrio + "\">" + v.nombreBarrio + "</option>");
    });

    if (barrio.length > 0) {
        selectItem("#txtBarrioReg", primero);
    }

}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}


$(document).ready(function () {

    $(".js-example-basic-single").select2();
    $('#txtBarrioReg').prop('disabled', true);

    cargarDataLocalidad();
    cargarTipoDocumento();
    cargarPerfil();
    cargarGenero();

    if ($.cookie("user") != null) {
        window.location.href = "principal.html";
    }

    $("#txtLocalidadReg").change(function () {
        selectedLocalidad = this.value;
        cargarDataBarrio();
    });

    $(".mostrar_registro").on("click", function () {
        $('#register').fadeTo("slow", 1);
        $('#login').fadeTo("slow", 0);
        $('#recover').fadeTo("slow", 0);
        $('#register').css('z-index', 22);
        $('#login').css('z-index', 21);
        $('#recover').css('z-index', 20);
    });

    $(".mostrar_login").on("click", function () {
        $('#login').fadeTo("slow", 1); //oculto mediante id
        $('#register').fadeTo("slow", 0); //muestro mediante clase
        $('#recover').fadeTo("slow", 0);
        $('#register').css('z-index', 21);
        $('#login').css('z-index', 22);
        $('#recover').css('z-index', 20);
    });

    $(".mostrar_recuperar").on("click", function () {
        $('#login').fadeTo("slow", 0); //oculto mediante id
        $('#register').fadeTo("slow", 0); //muestro mediante clase
        $('#recover').fadeTo("slow", 1);
        $('#register').css('z-index', 20);
        $('#login').css('z-index', 21);
        $('#recover').css('z-index', 22);
    });

    $('#btnConsultar').click(function (event) {
        if ($('#txtUsuario').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar el Usuario ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtContrasena').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar la Contraseña ",
                styling: 'bootstrap3'});
            return;
        }
        buscarUsuario();
    });

    $('#btnRegistro1').click(function (event) {
        if ($('#txtUsuarioReg').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar el Usuario ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtEmailReg').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar el Email ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtContrasenaReg').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar la Contraseña ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtCedulaReg').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar el Numero de Documento ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtNombreReg').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar el Nombre ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtApellidoReg').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar los Apellidos ",
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txtContrasenaReg').val() != $('#txtConfirmContrasenaReg').val()) {
            new PNotify({
                title: 'Advertencia!',
                text: " No coinciden las contraseñas, digitelas Nuevamentes ",
                styling: 'bootstrap3'});
            return;
        }

        registrarUsuario();
    });

    $('#btnEnviarCont').click(function (event) {
        if ($('#txtEmail').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: " Falta digitar el Email ",
                styling: 'bootstrap3'});
            return;
        }
        recuperarPassword();
    });
});