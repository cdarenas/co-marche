/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var URL;
var ITEM_MENU;
var $BODY;
var $MENU_TOGGLE;
var $SIDEBAR_MENU;
var $SIDEBAR_FOOTER;
var $LEFT_COL;
var $RIGHT_COL;
var $NAV_MENU;
var $FOOTER;

/* Redireccciona la pagina y la trae en el formulario*/
function redirPage(pagina) {
    var ajax;

    $('#content_nav').empty();
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: pagina,
        type: 'POST',
        data: {}
    });

    ajax.done(function (response) {
        jQuery.fx.interval = 100;
        $('#content_nav').empty().fadeOut(100, function () {
            $("#content_nav").html(response).toggle("slow");
        });
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> " + errorThrown);
        $('#alert_error').show();
    });
}

/* Obtiene la data del usuario a consultar*/
function cargarMenu() {
    var object, ajax, idPerfil;
    var data_user = JSON.parse($.cookie("user"));
    var permisos_total = [];

    //Hardcoded data values for demonstration purposes
    object = [data_user.usuario];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "listarMenu", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var listMenu = response.data;
            var id_menu = "";
            
            $.each(listMenu, function (k, v) {
                var menu = v.data;
                var icon = v.icon;
                var nombre_menu = k;
                id_menu = k.replace(" ", "_").toLocaleLowerCase();
                $(".side-menu").append("<li><a><i class=\"" + icon + "\"></i> " + nombre_menu + " <span class=\"fa fa-chevron-down\"></span></a> <ul class=\"nav child_menu\" id=\"ul_" + id_menu + "\"></ul></li>");

                $.each(menu, function (k1, v1) {
                    var nombre_id = v1.id;
                    var nombre_url = v1.url;
                    var permisos = v1.permisos;
                    var nombre_sub_menu = k1;
                    $.merge(permisos_total, permisos);
                    $("#ul_" + id_menu).append("<li id=\"" + nombre_id + "\"><a url=\"" + nombre_url + "\">" + nombre_sub_menu + "</a></li>");
                });
            });

            $.cookie("userPermisos", JSON.stringify(permisos_total));
            configMenu();
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> " + response.errorMessage + " ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de validar los datos - intente mas tarde ");
        $('#alert_error').show();
    });
}

function setContentHeight() {
    // reset height

    $RIGHT_COL.css('min-height', $(window).height());
    var bodyHeight = $BODY.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

    // normalize content
    contentHeight -= $NAV_MENU.height() + $FOOTER.height();

    $RIGHT_COL.css('min-height', contentHeight);
}

function configMenu() {

    $SIDEBAR_MENU.find('a').click(function (ev) {
        var $li = $(this).parent();
        ITEM_MENU = $li.attr('id') !== undefined ? "#" + $li.attr('id') : ITEM_MENU;

        if ($li.is('.active') && $li.attr('id') === undefined) {
            $li.removeClass('active');
            $('ul:first', $li).slideUp(function () {
                setContentHeight();
            });
        } else if ($li.attr('id') === undefined) {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('current-page');
                $SIDEBAR_MENU.find('li').removeClass('active');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }

            $li.addClass('active');

            if (ITEM_MENU != "#") {
                $SIDEBAR_MENU.find(ITEM_MENU).addClass('current-page');
                $SIDEBAR_MENU.find(ITEM_MENU).addClass('active');
            }

            $('ul:first', $li).slideDown(function () {
                setContentHeight();
            });

        } else {

            $('.child_menu').find('li').removeClass('current-page');
            $('.child_menu').find('li').removeClass('active');

            $li.addClass('active');

            // check active menu
            $SIDEBAR_MENU.find(ITEM_MENU).addClass('current-page');

            $SIDEBAR_MENU.find(ITEM_MENU).filter().
                    addClass('current-page').
                    parents('ul').
                    slideDown(function () {
                        setContentHeight();
                    }).parent().addClass('active');

            redirPage($(this).attr('url'));

            $('ul:first', $li).slideDown(function () {
                setContentHeight();
            });
        }
    });

    // toggle small or large menu
    $MENU_TOGGLE.on('click', function () {
        if ($BODY.hasClass('nav-md')) {
            $BODY.removeClass('nav-md').addClass('nav-sm');
            $LEFT_COL.removeClass('scroll-view').removeAttr('style');

            if ($SIDEBAR_MENU.find('li').hasClass('active')) {
                $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
            }
        } else {
            $BODY.removeClass('nav-sm').addClass('nav-md');

            if ($SIDEBAR_MENU.find('li').hasClass('active-sm')) {
                $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
            }
        }

        setContentHeight();
    });
}

function validarNumero(numero) {
    var patron = /^\d*$/;
    return !patron.test(numero);
}

// Sidebar
$(document).ready(function () {
    // TODO: This is some kind of easy fix, maybe we can improve this
    
    if ($.cookie("user") == null) {
        window.location.href = "login.html";
    }

    URL = window.location.href.split('?')[0];
    $BODY = $('body');
    $MENU_TOGGLE = $('#menu_toggle');
    $SIDEBAR_MENU = $('#sidebar-menu');
    $SIDEBAR_FOOTER = $('.sidebar-footer');
    $LEFT_COL = $('.left_col');
    $RIGHT_COL = $('.right_col');
    $NAV_MENU = $('.nav_menu');
    $FOOTER = $('footer');
    ITEM_MENU = "";

    cargarMenu();

    $('.btnCerrarSesion').click(function (event) {
        $.removeCookie("user");
        $.removeCookie("userPermisos");
        window.location.href = "../Admin/login.html";
    });

    $('#button_alert_warning').click(function (event) {
        $('#alert_warning').hide();
        $('#msn_alert_warning').empty().append("");
    });

    $('#button_alert_error').click(function (event) {
        $('#alert_error').hide();
        $('#msn_alert_error').empty().append("");
    });

    $('#button_alert_success').click(function (event) {
        $('#alert_success').hide();
        $('#msn_alert_success').empty().append("");
    });


    setContentHeight();
});
// /Sidebar

