var jornadaCollection;
var jtable;

/* Eilimina el jornada */
function eliminarJornada() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("jornadaSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_jornada: vacios(data_user.idJornada)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "JornadaService", methodName: "eliminarJornada", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al realizar la transaccion - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca las jornadas consultados por la busqueda*/
function listarJornadas() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
  
    //Hardcoded data values for demonstration purposes
    object = {id_jornada: vacios($('#txtBuscarIdJornada').val()).toLocaleLowerCase(), nombre_jornada: vacios($('#txtBuscarNombreJornada').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
        
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "JornadaService", methodName: "listarJornada", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            jornadaCollection = dataMiembros;
            //var jtable = $('#tblColegio').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var jornada = v;
                jtable.oApi._fnAddData(oSettings, [jornada.idJornada, jornada.nombreJornada]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error Durante la consulta!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarJornada() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Colegio/formularioJornada.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosJornada(documento) {
    $.each(jornadaCollection, function (k, v) {
        var jornada = v;

        if (documento == jornada.idJornada) {
            if ($.cookie("jornadaSelected") != null)
                $.removeCookie("jornadaSelected");
        
            $.cookie("jornadaSelected", JSON.stringify(jornada));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Jornada") {
            permiso_crear = true;
        }
        if (permiso == "Editar Jornada") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Jornada") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearJornada').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarJornada').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarJornada').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("jornadaSelected") != null)
        $.removeCookie("jornadaSelected");

    validar_permisos();

    var table = $('#tblJornada').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblJornada').dataTable();

    $('#tblJornada tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarJornadas();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("jornadaSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarJornadas();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("jornadaSelected");
        });

        if (typeof data != "undefined")
            cargarDatosJornada(data[0]);
    });

    $('#btnCrearJornada').click(function (event) {
        $.removeCookie("jornadaSelected");
        editarJornada();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarJornada').click(function (event) {
        if ($.cookie("jornadaSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar la jornada que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarJornada();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarJornada').click(function (event) {
        if ($.cookie("jornadaSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar la jornada que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("jornadaSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Colegio</h4> \n\
<p>¿Desea Eliminar la Jornada ' + data_user["nombreJornada"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarJornada').click(function (event) {
        eliminarJornada();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdJornada').val() == "" && $('#txtBuscarNombreJornada').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarJornadas();
    });
});