var colegiosCollection;
var jtable;

/* Eilimina el colegio */
function eliminarColegio() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("colegioSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_colegio: vacios(data_user.idColegio)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ColegioService", methodName: "eliminarColegio", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca los colegios consultados por la busqueda*/
function listarColegios() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_colegio: vacios($('#txtBuscarIdColegio').val()).toLocaleLowerCase(), nombre_colegio: vacios($('#txtBuscarNombreColegio').val()).toLocaleLowerCase(), localidad: vacios($('#txtBuscarLocalidad').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ColegioService", methodName: "listarColegio", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            colegiosCollection = dataMiembros;
            //var jtable = $('#tblColegio').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var colegio = v;
                jtable.oApi._fnAddData(oSettings, [colegio.idColegio, colegio.nombreColegio, colegio.telefono, colegio.direccion, colegio.email, colegio.nombreRector]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}


/* Cargar localidades*/
function cargarLocalidad() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = [906];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LocalidadService", methodName: "listarLocalidad", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    $("#txtBuscarLocalidad").append("<option value=\"\"></option>");
                    primero = "";
                }
                $("#txtBuscarLocalidad").append("<option value=\"" + data.idLocalidad + "\">" + data.nombreLocalidad + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txtBuscarLocalidad", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            deshabilitar_funciones();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar las localidades - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        deshabilitar_funciones();
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarColegio() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Colegio/formularioColegio.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosColegio(documento) {
    $.each(colegiosCollection, function (k, v) {
        var colegio = v;
        if (documento == colegio.idColegio) {
            if ($.cookie("colegioSelected") != null)
                $.removeCookie("colegioSelected");
            $.cookie("colegioSelected", JSON.stringify(colegio));
            return;
        }
    });
}

function deshabilitar_funciones() {
    new PNotify({
        title: 'Error!',
        text: 'No hay Localidades para gestionar los colegios asociados',
        type: 'error',
        styling: 'bootstrap3'});

    $('#btnBuscar').prop('disabled', true);
    $('#btnEliminarColegio').prop('disabled', true);
    $('#btnActualizarColegio').prop('disabled', true);
    $('#btnCrearColegio').prop('disabled', true);
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Colegio") {
            permiso_crear = true;
        }
        if (permiso == "Editar Colegio") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Colegio") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearColegio').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarColegio').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarColegio').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("colegioSelected") != null)
        $.removeCookie("colegioSelected");

    $(".js-example-basic-single").select2();
    cargarLocalidad();

    validar_permisos();

    var table = $('#tblColegio').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblColegio').dataTable();

    $('#tblColegio tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarColegios();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("colegioSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarColegios();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("colegioSelected");
        });

        if (typeof data != "undefined")
            cargarDatosColegio(data[0]);
    });

    $('#btnCrearColegio').click(function (event) {
        $.removeCookie("colegioSelected");
        editarColegio();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarColegio').click(function (event) {
        if ($.cookie("colegioSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el colegio que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarColegio();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarColegio').click(function (event) {
        if ($.cookie("colegioSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el colegio que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("colegioSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Colegio</h4> \n\
<p>¿Desea Eliminar el colegio ' + data_user["nombreColegio"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarColegio').click(function (event) {
        eliminarColegio();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdColegio').val() == "" && $('#txtBuscarNombreColegio').val() == "" && $('#txtBuscarLocalidad option:selected').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarColegios();
    });
});