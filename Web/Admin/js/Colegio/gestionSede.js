var sedeCollection;
var jtable;

/* Eilimina el sede */
function eliminarSede() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("sedeSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_sede: vacios(data_user.idSede)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "SedeService", methodName: "eliminarSede", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al realizar la transaccion - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca las sedes consultados por la busqueda*/
function listarSedes() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
  
    //Hardcoded data values for demonstration purposes
    object = {id_sede: vacios($('#txtBuscarIdSede').val()).toLocaleLowerCase(), nombre_sede: vacios($('#txtBuscarNombreSede').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
        
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "SedeService", methodName: "listarSede", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            sedeCollection = dataMiembros;
            //var jtable = $('#tblColegio').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var sede = v;
                jtable.oApi._fnAddData(oSettings, [sede.idSede, sede.nombreSede]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error Durante la consulta!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarSede() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Colegio/formularioSede.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosSede(documento) {
    $.each(sedeCollection, function (k, v) {
        var sede = v;

        if (documento == sede.idSede) {
            if ($.cookie("sedeSelected") != null)
                $.removeCookie("sedeSelected");
        
            $.cookie("sedeSelected", JSON.stringify(sede));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Sede") {
            permiso_crear = true;
        }
        if (permiso == "Editar Sede") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Sede") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearSede').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarSede').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarSede').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("sedeSelected") != null)
        $.removeCookie("sedeSelected");

    validar_permisos();

    var table = $('#tblSede').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblSede').dataTable();

    $('#tblSede tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarSedes();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("sedeSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarSedes();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("sedeSelected");
        });

        if (typeof data != "undefined")
            cargarDatosSede(data[0]);
    });

    $('#btnCrearSede').click(function (event) {
        $.removeCookie("sedeSelected");
        editarSede();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarSede').click(function (event) {
        if ($.cookie("sedeSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el sede que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarSede();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarSede').click(function (event) {
        if ($.cookie("sedeSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el sede que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("sedeSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Colegio</h4> \n\
<p>¿Desea Eliminar la Sede ' + data_user["nombreSede"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarSede').click(function (event) {
        eliminarSede();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdSede').val() == "" && $('#txtBuscarNombreSede').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarSedes();
    });
});