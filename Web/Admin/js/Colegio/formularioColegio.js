var localidad, barrio, selectedLocalidad;

/* Guarda o Actualiza la data del usuarioColegio */
function gestionarColegio(acutaliza) {
    var object, ajax;

    var nivel = [];
    var jornada = [];
    var sede = [];
    
    $.each($("#txt_nivel option:selected"), function () {
        nivel.push($(this).val());
    });

    $.each($("#txt_jornada option:selected"), function () {
        jornada.push($(this).val());
    });

    $.each($("#txt_sede option:selected"), function () {
        sede.push($(this).val());
    });

    object = {
        id_colegio: vacios($('#txt_id_colegio').val()),
        nombre_colegio: vacios($('#txt_nombre_colegio').val()),
        direccion: vacios($('#txt_direccion').val()),
        telefono: vacios($('#txt_telefono').val()),
        fax: vacios($('#txt_fax').val()),
        nombre_rector: vacios($('#txt_nombre_rector').val()),
        email: vacios($('#txt_email').val()),
        array_jornada: jornada.join(","),
        array_sede: sede.join(","),
        array_nivel: nivel.join(","),
        id_barrio: vacios($('#txt_barrio option:selected').val()),
        id_estado: vacios($('#txt_estado option:selected').val())
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ColegioService", methodName: "gestionarColegio", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Estado*/
function cargarEstado() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = {id_tabla: 3};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "EstadoService", methodName: "listarEstado", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idEstado;
                }
                $("#txt_estado").append("<option value=\"" + data.idEstado + "\">" + data.nombreEstado + "</option>");
            });

            if ($.cookie("colegioSelected") != null) {
                var data_user = JSON.parse($.cookie("colegioSelected"));
                selectItem("#txt_estado", data_user["idEstado"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_estado", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar estados - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Jornada*/
function cargarJornada() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "JornadaService", methodName: "listarJornada"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idJornada;
                }
                $("#txt_jornada").append("<option value=\"" + data.idJornada + "\">" + data.nombreJornada + "</option>");
            });

            if ($.cookie("colegioSelected") != null) {
                var data_user = JSON.parse($.cookie("colegioSelected"));
                selectItem("#txt_jornada", data_user["idJornada"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_jornada", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar jornadas - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Sede*/
function cargarSede() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "SedeService", methodName: "listarSede"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idSede;
                }
                $("#txt_sede").append("<option value=\"" + data.idSede + "\">" + data.nombreSede + "</option>");
            });

            if ($.cookie("colegioSelected") != null) {
                var data_user = JSON.parse($.cookie("colegioSelected"));
                selectItem("#txt_sede", data_user["idSede"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_sede", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar sedes - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Nivel*/
function cargarNivel() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "listarNivel"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idNivel;
                }
                $("#txt_nivel").append("<option value=\"" + data.idNivel + "\">" + data.nombreNivel + "</option>");
            });

            if ($.cookie("colegioSelected") != null) {
                var data_user = JSON.parse($.cookie("colegioSelected"));
                selectItem("#txt_nivel", data_user["idNivel"]);
            } else if (tipo.length > 0) {
                selectItem("#txt_nivel", primero);
            }
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar nivels - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar localidades*/
function cargarDataLocalidad() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = [906];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LocalidadService", methodName: "listarLocalidad", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            localidad = response.data;
            cargarLocalidad();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar las localidades - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* Cargar Barrios*/
function cargarDataBarrio() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = [selectedLocalidad];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "BarrioService", methodName: "listarBarrio", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            barrio = response.data;
            cargarBarrio();
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar los barrios ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* llena el array del genero*/
function cargarGenero() {
    var tipo = [{idGenero: "Masculino", nombreGenero: "Masculino"}, {idGenero: "Femenino", nombreGenero: "Femenino"}];
    var primero = "";
    $.each(tipo, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idGenero;
        }
        $("#txt_genero").append("<option value=\"" + data.idGenero + "\">" + data.nombreGenero + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_genero", data_user["genero"]);
    } else if (tipo.length > 0) {
        selectItem("#txt_genero", primero);
    }
}

function formatDate(date) {

    if (vacios(date) == "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function cargarLocalidad() {
    var primero = "";
    $.each(localidad, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idLocalidad;
        }
        $("#txt_localidad").append("<option value=\"" + v.idLocalidad + "\">" + v.nombreLocalidad + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_localidad", data_user["idLocalidad"]);
    } else if (localidad.length > 0) {
        selectItem("#txt_localidad", primero);
    }
}

function cargarBarrio(item_localidad) {
    $("#txt_barrio").find('option').remove();
    $('#txt_barrio').prop('disabled', false);
    var primero = "";

    $.each(barrio, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idBarrio;
        }
        $("#txt_barrio").append("<option value=\"" + v.idBarrio + "\">" + v.nombreBarrio + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_barrio", data_user["idBarrio"]);
    } else if (barrio.length > 0) {
        selectItem("#txt_barrio", primero);
    }

}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, actualizar;

    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    $(".selectFormulario").select2();
    $('#txt_id_colegio').prop('disabled', true);
    $('#txt_barrio').prop('disabled', true);

    cargarDataLocalidad();
    cargarEstado();
    cargarJornada();
    cargarSede();
    cargarNivel();

    if ($.cookie("colegioSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("colegioSelected"));

        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_id_colegio').val(vacios(data_user["idColegio"]));
        $('#txt_nombre_colegio').val(vacios(data_user["nombreColegio"]));
        $('#txt_direccion').val(vacios(data_user["direccion"]));
        $('#txt_telefono').val(vacios(data_user["telefono"]));
        $('#txt_fax').val(vacios(data_user["fax"]));
        $('#txt_email').val(vacios(data_user["email"]));
        $('#txt_nombre_rector').val(vacios(formatDate(data_user["nombreRector"])));

        $("#btnGuardarData").html('Actualizar Colegio');
        $("#contUserRegister").hide();

        selectedLocalidad = data_user["idLocalidad"];
        cargarDataBarrio();
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Colegio");
        $("#btnGuardarData").html('Guardar');

        $('#txt_fecha_nacimiento').val(formatDate(""));
        $("#contUserRegister").show();
    }

    $("#txt_localidad").change(function () {
        selectedLocalidad = this.value;
        cargarDataBarrio();
    });


    $('#btnGuardarData').click(function (event) {

        if ($('#txt_email').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el email del colegio',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_nombre_colegio').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre del Colegio',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_nombre_rector').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre del Rector',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_direccion').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el direccion',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_telefono').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el telefono',
                styling: 'bootstrap3'});
            return;
        }

        gestionarColegio(actualizar ? "true" : "false");
    });
});


