
/* Guarda o Actualiza la data del Nivel */
function gestionarNivel(acutaliza) {
    var object, ajax;

    object = {
        id_nivel: vacios($('#txt_id_nivel').val()),
        nombre_nivel: vacios($('#txt_nombre_nivel').val()),
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "gestionarNivel", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function cargarLocalidad() {
    var primero = "";
    $.each(localidad, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idLocalidad;
        }
        $("#txt_localidad").append("<option value=\"" + v.idLocalidad + "\">" + v.nombreLocalidad + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_localidad", data_user["idLocalidad"]);
    } else if (localidad.length > 0) {
        selectItem("#txt_localidad", primero);
    }
}

function cargarBarrio(item_localidad) {
    $("#txt_barrio").find('option').remove();
    $('#txt_barrio').prop('disabled', false);
    var primero = "";

    $.each(barrio, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idBarrio;
        }
        $("#txt_barrio").append("<option value=\"" + v.idBarrio + "\">" + v.nombreBarrio + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_barrio", data_user["idBarrio"]);
    } else if (barrio.length > 0) {
        selectItem("#txt_barrio", primero);
    }

}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, actualizar;

    $('#txt_id_nivel').prop('disabled', true);

    if ($.cookie("nivelSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("nivelSelected"));

        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_id_nivel').val(vacios(data_user["idNivel"]));
        $('#txt_nombre_nivel').val(vacios(data_user["nombreNivel"]));

        $("#btnGuardarData").html('Actualizar');
        $("#contUserRegister").hide();

    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Nivel");
        $("#btnGuardarData").html('Guardar');
        $("#contUserRegister").show();
    }

    $('#btnGuardarData').click(function (event) {

        if ($('#txt_nombre_nivel').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre de la Nivel',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_nombre_nivel').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre de la Nivel',
                styling: 'bootstrap3'});
            return;
        }

        gestionarNivel(actualizar ? "true" : "false");
    });
});
