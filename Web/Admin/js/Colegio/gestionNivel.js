var nivelCollection;
var jtable;

/* Eilimina el nivel */
function eliminarNivel() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    var data_user = JSON.parse($.cookie("nivelSelected"));

    //Hardcoded data values for demonstration purposes
    object = {id_nivel: vacios(data_user.idNivel)};

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "eliminarNivel", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
            $('#modalEliminarDatos').modal('hide');
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al realizar la transaccion - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
        $('#modalEliminarDatos').modal('hide');
    });
}

/* busca las nivels consultados por la busqueda*/
function listarNiveles() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
  
    //Hardcoded data values for demonstration purposes
    object = {id_nivel: vacios($('#txtBuscarIdNivel').val()).toLocaleLowerCase(), nombre_nivel: vacios($('#txtBuscarNombreNivel').val()).toLocaleLowerCase()};
    //Pass the values to the AJAX request and specify function arg for 'done' callback
        
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "listarNivel", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            var dataMiembros = response.data;
            nivelCollection = dataMiembros;
            //var jtable = $('#tblColegio').dataTable();
            var oSettings = jtable.fnSettings();
            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);
            $.each(dataMiembros, function (k, v) {
                var nivel = v;
                jtable.oApi._fnAddData(oSettings, [nivel.idNivel, nivel.nombreNivel]);
            });
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            jtable.fnClearTable(this);
            jtable.fnDraw();
            new PNotify({
                title: 'Error Durante la consulta!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        jtable.fnClearTable(this);
        jtable.fnDraw();
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function editarNivel() {
    var ajax;
    ajax = $.ajax({
        url: '../Admin/Colegio/formularioNivel.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalActualizarDatos .modal-body').html(response);
    });
}

function cargarDatosNivel(documento) {
    $.each(nivelCollection, function (k, v) {
        var nivel = v;

        if (documento == nivel.idNivel) {
            if ($.cookie("nivelSelected") != null)
                $.removeCookie("nivelSelected");
        
            $.cookie("nivelSelected", JSON.stringify(nivel));
            return;
        }
    });
}

function validar_permisos() {
    var permisos = JSON.parse($.cookie("userPermisos"));
    var permiso_crear = false;
    var permiso_editar = false;
    var permiso_eliminar = false;

    $.each(permisos, function (k, v) {
        var permiso = v;

        if (permiso == "Crear Nivel") {
            permiso_crear = true;
        }
        if (permiso == "Editar Nivel") {
            permiso_editar = true;
        }
        if (permiso == "Inactivar Nivel") {
            permiso_eliminar = true;
        }
    });

    if (!permiso_crear) {
        $('#btnCrearNivel').hide();
    }
    if (!permiso_editar) {
        $('#btnActualizarNivel').hide();
    }
    if (!permiso_eliminar) {
        $('#btnEliminarNivel').hide();
    }
}

$(document).ready(function () {

    if ($.cookie("nivelSelected") != null)
        $.removeCookie("nivelSelected");

    validar_permisos();

    var table = $('#tblNivel').DataTable({
        /*scrollY: '50vh',*/
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblNivel').dataTable();

    $('#tblNivel tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        $('#modalActualizarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarNiveles();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("nivelSelected");
        });

        $('#modalEliminarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarNiveles();
            $('#tblColegio tbody').find('tr').removeClass('selected');
            $.removeCookie("nivelSelected");
        });

        if (typeof data != "undefined")
            cargarDatosNivel(data[0]);
    });

    $('#btnCrearNivel').click(function (event) {
        $.removeCookie("nivelSelected");
        editarNivel();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnActualizarNivel').click(function (event) {
        if ($.cookie("nivelSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el nivel que desea Editar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }

        editarNivel();
        $('#modalActualizarDatos').modal('show');
    });

    $('#btnEliminarNivel').click(function (event) {
        if ($.cookie("nivelSelected") == null) {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar el nivel que desea Eliminar',
                type: 'error',
                styling: 'bootstrap3'});
            return;
        }
        var data_user = JSON.parse($.cookie("nivelSelected"));
        $('#modalEliminarDatos .modal-body').html('<h4>Elimiar Colegio</h4> \n\
<p>¿Desea Eliminar la Nivel ' + data_user["nombreNivel"] + ' ?</p>');
        $('#modalEliminarDatos').modal('show');
    });

    $('#btnConfirmEliminarNivel').click(function (event) {
        eliminarNivel();
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();
        if ($('#txtBuscarIdNivel').val() == "" && $('#txtBuscarNombreNivel').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta Digitar algun parametro de Busqueda',
                styling: 'bootstrap3'});
            return;
        }

        listarNiveles();
    });
});