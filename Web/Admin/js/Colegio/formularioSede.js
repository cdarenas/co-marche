
/* Guarda o Actualiza la data del Sede */
function gestionarSede(acutaliza) {
    var object, ajax;

    object = {
        id_sede: vacios($('#txt_id_sede').val()),
        nombre_sede: vacios($('#txt_nombre_sede').val()),
    };

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "SedeService", methodName: "gestionarSede", parameters: [object, acutaliza]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            new PNotify({
                title: 'Mensaje',
                text: "Se realizo Exitosamente la transaccion, " + response.errorMessage,
                type: 'success',
                styling: 'bootstrap3'
            });

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al consultar los datos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function cargarLocalidad() {
    var primero = "";
    $.each(localidad, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idLocalidad;
        }
        $("#txt_localidad").append("<option value=\"" + v.idLocalidad + "\">" + v.nombreLocalidad + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_localidad", data_user["idLocalidad"]);
    } else if (localidad.length > 0) {
        selectItem("#txt_localidad", primero);
    }
}

function cargarBarrio(item_localidad) {
    $("#txt_barrio").find('option').remove();
    $('#txt_barrio').prop('disabled', false);
    var primero = "";

    $.each(barrio, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idBarrio;
        }
        $("#txt_barrio").append("<option value=\"" + v.idBarrio + "\">" + v.nombreBarrio + "</option>");
    });

    if ($.cookie("colegioSelected") != null) {
        var data_user = JSON.parse($.cookie("colegioSelected"));
        selectItem("#txt_barrio", data_user["idBarrio"]);
    } else if (barrio.length > 0) {
        selectItem("#txt_barrio", primero);
    }

}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, actualizar;

    $('#txt_id_sede').prop('disabled', true);

    if ($.cookie("sedeSelected") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("sedeSelected"));

        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_id_sede').val(vacios(data_user["idSede"]));
        $('#txt_nombre_sede').val(vacios(data_user["nombreSede"]));

        $("#btnGuardarData").html('Actualizar');
        $("#contUserRegister").hide();

    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Sede");
        $("#btnGuardarData").html('Guardar');
        $("#contUserRegister").show();
    }

    $('#btnGuardarData').click(function (event) {

        if ($('#txt_nombre_sede').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre de la Sede',
                styling: 'bootstrap3'});
            return;
        }

        if ($('#txt_nombre_sede').val() == "") {
            new PNotify({
                title: 'Advertencia!',
                text: 'Falta digitar el nombre de la Sede',
                styling: 'bootstrap3'});
            return;
        }

        gestionarSede(actualizar ? "true" : "false");
    });
});
