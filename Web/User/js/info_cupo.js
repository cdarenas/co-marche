var cupoCollection;
var jtable;

/* busca los miembros consultados por la busqueda*/
function listarCupo() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = JSON.stringify({documento: $('#txtBuscarDocumento').val().trim().toLocaleUpperCase(), nombres: $('#txtBuscarNombres').val().trim().toLocaleUpperCase(), nro_cupo: $('#txtBuscarNroCupo').val().trim().toLocaleUpperCase()});

    ajax = $.ajax({
        url: '../prueba.php',
        type: 'POST',
        data: {funcion: "listarCupo", obj_json: object}
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var data = response.data;
            cupoCollection = data;
            //var jtable = $('#tblCupo').dataTable();
            var oSettings = jtable.fnSettings();

            $('.dataTables_processing').css('visibility', 'visible').show();
            jtable.fnClearTable(this);

            $.each(data, function (k, v) {
                var cupo = v;
                jtable.oApi._fnAddData(oSettings, [cupo.nro_cupo, cupo.documento, cupo.nombres, cupo.apellidos, cupo.fecha_solicitud, cupo.nivel, cupo.jornada, cupo.estado, cupo.observacion]);
            });
            
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            jtable.fnDraw();
            $('.dataTables_processing').hide();
        } else {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> " + response.errorMessage);
            $('#alert_warning1').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al consultar los datos - intente mas tarde ");
        $('#alert_error1').show();
    });
}

function editarCupo() {
    var ajax;

    ajax = $.ajax({
        url: '../Web/datos_cupo.html',
        type: 'GET'
    });
    ajax.done(function (response) {
        $('#modalConsultarDatos .modal-body').html(response);
        // $('#modalConsultarDatos').modal('toggle');
    });
}

function cargarDatosCupo(nro_cupo) {    
    $.each(cupoCollection, function (k, v) {
        var cupo = v;                        
        if (nro_cupo === cupo.nro_cupo) {
            if ($.cookie("userCupo") !== null)
                $.removeCookie("userCupo");

            $.cookie("userCupo", JSON.stringify(cupo));
            editarCupo();
        }
    });

}

$(document).ready(function () {

    $('#alert_warning1').hide();
    $('#alert_error1').hide();

    var table = $('#tblCupo').DataTable({
        scrollY: '50vh',
        scrollCollapse: true,
        paging: false,
        language: {
            search: "Buscar:",
            zeroRecords: "No hay registros para visualizar",
            info: "Pagina _PAGE_ de _PAGES_"
        }
    });
    jtable = $('#tblCupo').dataTable()

    $('#tblCupo tbody').on('click', 'tr', function () {
        var data = table.row(this).data();

        $("#modalConsultarDatos").on("show.bs.modal", function () {
            //var height = $(window).height() - 200;
            //$(this).find(".modal-body").css("max-height", height);
            //console.log("Abre Modal ... ");
        });

        $('#modalConsultarDatos').on('hidden.bs.modal', function () {
            //console.log("Cierra Modal ... ");
            listarCupo();
        });

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        cargarDatosCupo(data[0]);
        $('#modalConsultarDatos').modal('show');
    });


    $('#btnRegistrarMiembro').click(function (event) {
        $.removeCookie("userCupo");
        editarCupo();
        $('#modalConsultarDatos').modal('show');
    });


    $('#btnBuscar').click(function (event) {
        $('#alert_warning').hide();
        $('#alert_error').hide();

        if ($('#txtBuscarDocumento').val() === "" && $('#txtBuscarNombres').val() === "" && $('#txtBuscarNroCupo').val() === "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar algun parametro de busqueda ");
            $('#alert_warning1').show();
            return;
        }

        listarCupo();
    });
});


