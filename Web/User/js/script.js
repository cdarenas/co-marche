/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var $SIDEBAR_MENU;
var $SIDEBAR_MENU_2;
var ITEM_MENU;

/* Redireccciona la pagina y la trae en el formulario*/
function redirPage(pagina) {
    var ajax;

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: pagina,
        type: 'POST',
        data: {}
    });


    ajax.done(function (response) {
        jQuery.fx.interval = 100;
        $('#content_nav').empty().fadeOut(100, function () {
            $("#content_nav").html(response).toggle("slow");
        });
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> " + errorThrown);
        $('#alert_error').show();
    });
}

function configMenu() {

    $SIDEBAR_MENU.find('a').click(function (ev) {
        var $li = $(this).parent();
        ITEM_MENU = $li.attr('id') !== undefined ? "#" + $li.attr('id') : ITEM_MENU;
        $('.navbar-nav').find('li').removeClass('active');

        if (ITEM_MENU === '#menu_cerrar_session') {
            $.removeCookie("user");
            $.removeCookie("userAcudiente");
            $('#menu_navg').hide();
            $('#menu_cerrar_session').hide();
            $('#menu_inicio_session').show();
            $('#menu_registrar').show();
            redirPage("../User/inicio.html");
        } else if($(this).attr('url') !== undefined) {
            $li.addClass('active');
            redirPage($(this).attr('url'));
        }
    });
    
    $SIDEBAR_MENU_2.find('a').click(function (ev) {
        var $li = $(this).parent();
        
        if($(this).attr('url') !== undefined) {
            redirPage($(this).attr('url'));
        }
    });
}

function validarNumero(numero) {
    var patron = /^\d*$/;
    return !patron.test(numero);
}

$(document).ready(function () {
    $('#alert_warning').hide();
    $('#alert_error').hide();


    $SIDEBAR_MENU = $('#sidebar-menu');
    $SIDEBAR_MENU_2 = $('#sidebar-menu-2');
    redirPage("../User/inicio.html");
    configMenu();

    if ($.cookie("user") != null) {
        $('#menu_navg').show();
        $('#menu_cerrar_session').show();
        $('#menu_inicio_session').hide();
        $('#menu_registrar').hide();
    } else {
        $('#menu_navg').hide();
        $('#menu_cerrar_session').hide();
        $('#menu_inicio_session').show();
        $('#menu_registrar').show();
    }

    $('#main-slider.carousel').carousel({
        interval: 20000
    });

    $('#button_alert_warning').click(function (event) {
        $('#alert_warning').hide();
        $('#msn_alert_warning').empty().append("");
    });

    $('#button_alert_error').click(function (event) {
        $('#alert_error').hide();
        $('#msn_alert_error').empty().append("");
    });

    $('#button_alert_success').click(function (event) {
        $('#alert_success').hide();
        $('#msn_alert_success').empty().append("");
    });

}
);