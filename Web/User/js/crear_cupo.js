var data_user, barrio, localidad, actualizar, selectedLocalidad;
var files = [];

/* Guarda o Actualiza la data del usuario */
function gestionarCupo(data_user) {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    var data_user = JSON.parse($.cookie("user"));

    object = {
        id_tipo_documento: vacios($('#txt_tipo_documento option:selected').val()),
        cedula: vacios($('#txt_cedula').val()),
        nombres: vacios($('#txt_nombre').val()),
        apellidos: vacios($('#txt_apellidos').val()),
        direccion: vacios($('#txt_direccion').val()),
        telefono: vacios($('#txt_telefono').val()),
        genero: vacios($('#txt_genero option:selected').val()),
        id_barrio: vacios($('#txt_barrio option:selected').val()),
        email: vacios($('#txt_email').val()),
        fecha_nacimiento: formatDate(vacios($('#txt_fecha_nacimiento').val())),
        cedula_acudiente: vacios(data_user["cedula"]),
        ultimo_anio_cursado: vacios($('#txt_ultimo_anio_cursado').val()),
        id_parentesco: vacios($('#txt_parentesco option:selected').val()),
        id_nivel: vacios($('#txt_nivel option:selected').val()),
        id_jornada: vacios($('#txt_jornada option:selected').val()),
        archivos: files
    };


    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "UsuarioService", methodName: "registrarUsuarioCupo", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            $('#msn_alert_success').empty().append("<strong>Mensaje</strong> Se realizo Exitosamente la transaccion, " + response.data + " ");
            $('#alert_success').show();
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> " + response.errorMessage + " ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de validar los datos del Acudiente - intente mas tarde ");
        $('#alert_error').show();
    });
}


/* Cargar Tipo Documento*/
function cargarTipoDocumento() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "TipoDocumentoService", methodName: "listarTipoDocumento"})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idTipoDocumento;
                }
                $("#txt_tipo_documento").append("<option value=\"" + data.idTipoDocumento + "\">" + data.nombreTipoDocumento + "</option>");
            });

           if (tipo.length > 0) {
                selectItem("#txt_tipo_documento", primero);
            }
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar tipos documentos - intente mas tarde ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar tipos documentos - intente mas tarde ");
        $('#alert_error').show();
    });
}

/* Cargar localidades*/
function cargarDataLocalidad() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = [906];
    localidad = [];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LocalidadService", methodName: "listarLocalidad", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            localidad = response.data;
            cargarLocalidad();
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar las localidades - intente mas tarde ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar las localidades - intente mas tarde ");
        $('#alert_error').show();
    });
}

/* Cargar Barrios*/
function cargarDataBarrio() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = [selectedLocalidad];
    barrio = [];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "BarrioService", methodName: "listarBarrio", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            barrio = response.data;
            cargarBarrio();
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar los barrios - intente mas tarde ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar los barrios - intente mas tarde ");
        $('#alert_error').show();
    });
}


/* Cargar Nivel*/
function cargarDataNivel() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    //Hardcoded data values for demonstration purposes
    object = {id_nivel: "", nombre_nivel: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "NivelService", methodName: "listarNivel", parameters: [object]})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idNivel;
                }
                $("#txt_nivel").append("<option value=\"" + data.idNivel + "\">" + data.nombreNivel + "</option>");
            });

            if (tipo.length > 0) {
                selectItem("#txt_nivel", primero);
            }
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar los niveles - intente mas tarde ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar los niveles - intente mas tarde ");
        $('#alert_error').show();
    });
}

/* Cargar Jornada*/
function cargarDataJornada() {
    var object, ajax;    //Pass the values to the AJAX request and specify function arg for 'done' callback

    object = {id_jornada: "", nombre_jornada: ""};
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "JornadaService", methodName: "listarJornada", parameters: [object]})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";
            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idJornada;
                }
                $("#txt_jornada").append("<option value=\"" + data.idJornada + "\">" + data.nombreJornada + "</option>");
            });

 if (tipo.length > 0) {
                selectItem("#txt_jornada", primero);
            }
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar las jornadas - intente mas tarde ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar las jornadas - intente mas tarde ");
        $('#alert_error').show();
    });
}

/* Cargar Parentesco*/
function cargarParentesco() {
    var object, ajax;

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "ParentescoService", methodName: "listarParentesco"})
    });

    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            var primero = "";

            $.each(tipo, function (k, v) {
                var data = v;
                if (primero == "") {
                    primero = data.idParentesco;
                }
                $("#txt_parentesco").append("<option value=\"" + data.idParentesco + "\">" + data.nombreParentesco + "</option>");
            });

if (tipo.length > 0) {
                selectItem("#txt_parentesco", primero);
            }

        } else {
            new PNotify({
                title: 'Error!',
                text: response.errorMessage,
                type: 'error',
                styling: 'bootstrap3'});
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        new PNotify({
            title: 'Error!',
            text: " Ocurrio un error al momento de listar tipos documentos - intente mas tarde ",
            type: 'error',
            styling: 'bootstrap3'});
    });
}

/* llena el array del genero*/
function cargarGenero() {
    var tipo = [{idGenero: "Masculino", nombreGenero: "Masculino"}, {idGenero: "Femenino", nombreGenero: "Femenino"}];
    var primero = "";
    $.each(tipo, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idGenero;
        }
        $("#txt_genero").append("<option value=\"" + data.idGenero + "\">" + data.nombreGenero + "</option>");
    });

 if (tipo.length > 0) {
        selectItem("#txt_genero", primero);
    }
}

function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function selectItem(id_select, item) {
    $(id_select).val(item).trigger("change");
}

function convertString(texto) {
    if (vacios(texto) == "")
        return texto

    while (texto.indexOf(" ") > -1) {
        texto = texto.replace(" ", "_");
    }
    return texto.toLowerCase();
}

function cargarLocalidad() {
    var primero = "";
    $.each(localidad, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idLocalidad;
        }
        $("#txt_localidad").append("<option value=\"" + data.idLocalidad + "\">" + data.nombreLocalidad + "</option>");
    });

 if (localidad.length > 0) {
        selectItem("#txt_localidad", primero);
        selectedLocalidad = primero;
        cargarDataBarrio();
    }
}

function cargarBarrio() {
    $("#txt_barrio").find('option').remove();
    $('#txt_barrio').prop('disabled', false);
    var primero = "";

    $.each(barrio, function (k, v) {
        var data = v;
        if (primero == "") {
            primero = data.idBarrio;
        }
        $("#txt_barrio").append("<option value=\"" + data.idBarrio + "\">" + data.nombreBarrio + "</option>");
    });

if (barrio.length > 0) {
        selectItem("#txt_barrio", primero);
    }

}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function handleFileSelect(evt) {
    var array_files;
    evt.stopPropagation();
    evt.preventDefault();

    $('#list tbody').empty();
    $('#drop_zone').removeClass('_hover');
    $('#drop_zone').addClass('_drag');
    files = [];

    if (evt.type == "change") {
        array_files = evt.target.files;
    } else if (evt.type == "drop") {
        array_files = evt.dataTransfer.files;
    } else
        return;

    $.each(array_files, function (index, file) {
        var continuo = false;

        if (file.size <= 0)
            return true;

        $.each(files, function (i, obj) {
            var texto = convertString(file.name);
            if (obj.filename == texto && obj.size == file.size) {
                continuo = true;
                return false;
            }
        });

        if (continuo)
            return true;

        var fileReader = new FileReader();
        fileReader.onload = (function (file) {
            return function (e) {

                var texto = convertString(file.name);
                var tr = ['<tr><td>', texto, '</th><td style="text-align: center">', file.type || 'n/a', '</td><td style="text-align: center">', file.size, '</td><td style="text-align: center">', file.lastModifiedDate.toLocaleDateString(), '</td><td><button type="button" id="" item="', texto, '" tipo="data" size="', file.size, '" class="removebutton">Eliminar Archivo</button></td></tr>'].join('');
                $(tr).appendTo($("#list"));
            };
        })(file);

        fileReader.readAsDataURL(file);
        fileReader.onloadend = function () {
            var base64data = fileReader.result;
            var texto = convertString(file.name);
            base64data = base64data.replace("data:", "");
            base64data = base64data.replace(";base64,", "");
            files.push({filename: texto, bytes64: base64data, size: file.size, lastModified: file.lastModifiedDate.toLocaleDateString(), typeFile: "file"});
        };
    });

    $("#list").fadeIn(400, function () {
        $(this).show();
    });

    $('#drop_zone').removeClass('_drag');
}

function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    $('#drop_zone').addClass('_hover');
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function handleDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    $('#drop_zone').removeClass('_hover');
}

$(document).ready(function () {

    $.event.props.push('dataTransfer');

    $('#drop_zone').on('dragover', handleDragOver)
            .on('drop', handleFileSelect)
            .on('dragleave', handleDragLeave)
            .click(function () {
                $('#my_file').click();
            });

    $('#my_file').change(handleFileSelect);

    $("#list").hide();
    $("#list").on("click", "button.removebutton", function () {
        var nombre_archivo = $(this).attr('item');
        var size = $(this).attr('size');
        var tipo = $(this).attr('tipo');
        var id_documento = $(this).attr('id');

        var tr = $(this).closest('tr');
        tr.css("background-color", "#FF3700");
        tr.fadeOut(400, function () {
            tr.remove();

            files = $.grep(files, function (value) {
                return value.filename != nombre_archivo && value.size != size;
            });
        });

        return false;
    });

    $(".js-example-basic-single").select2();

    $('#txt_barrio').prop('disabled', true);
    $('#alert_warning').hide();
    $('#alert_error').hide();
    $('#alert_success').hide();

    cargarDataLocalidad();
    cargarTipoDocumento();
    cargarDataNivel();
    cargarDataJornada();
    cargarGenero();
    cargarParentesco();

    $('#titulo_modulo').html("Registro Alumno - Cupo");
    $("#btnGuardarCupo").html('Guardar');
    $('#txt_fecha_nacimiento').val(formatDate(""));

    $("#txt_localidad").change(function () {
        selectedLocalidad = this.value;
        cargarDataBarrio()
    });

    $('#btnGuardarCupo').click(function (event) {

        if ($('#txt_cedula').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el documento ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_nombre').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el nombre ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_apellidos').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el apellido ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_direccion').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el direccion ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_telefono').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el telefono ");
            $('#alert_warning').show();
            return;
        }

        if (files.length <= 0) {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta adjuntar los archivos ");
            $('#alert_warning').show();
        }

        gestionarCupo();
    });
});


