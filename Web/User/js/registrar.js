/* Guarda o Actualiza la data del usuario */
function gestionarUsuarioAcudiente(data_user, acutaliza) {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback
    data_user = JSON.stringify(data_user);

    ajax = $.ajax({
        url: '../prueba.php',
        type: 'POST',
        data: {funcion: "gestionarUsuarioAcudiente", obj_json: data_user, acutaliza: acutaliza}
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
        if (response.success) {
            $('#msn_alert_success').empty().append("<strong>Mensaje</strong> Se realizo Exitosamente la transaccion, " + response.data + " ");
            $('#alert_success').show();

            $.cookie("user", JSON.stringify(response.data));
            redirPage("../Web/inicio.html");
            $('#menu_navg').show();
            $('#menu_cerrar_session').show();
            $('#menu_inicio_session').hide();
            $('#menu_registrar').hide();
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> " + response.errorMessage + " ");
            $('#alert_error').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de validar los datos del Acudiente - intente mas tarde ");
        $('#alert_error').show();
    });
}


/* Cargar Tipo Documento*/
function cargarTipoDocumento() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../prueba.php',
        type: 'POST',
        data: {funcion: "cargarTipoDocumento", obj_json: ""}
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            var tipo = response.data;
            $.each(tipo, function (k, v) {
                var data = v
                $("#txt_tipo_documento").append("<option value=\"" + data.id_tipo_documento + "\">" + data.nombre_tipo_documento + "</option>");
            })

            if ($.cookie("userAcudiente") != null) {
                var data_user = JSON.parse($.cookie("userAcudiente"));
                selectItem("#txt_tipo_documento", data_user["id_tipo_documento"])
            }
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar tipos documentos - intente mas tarde ");
        $('#alert_error').show();
    });
}

/* Cargar localidades*/
function cargarDataLocalidad() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../prueba.php',
        type: 'POST',
        data: {funcion: "cargarLocalidad", obj_json: ""}
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            localidad = response.data;
            cargarLocalidad();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar las localidades - intente mas tarde ");
        $('#alert_error').show();
    });
}

/* Cargar Barrios*/
function cargarDataBarrio() {
    var object, ajax;
    //Pass the values to the AJAX request and specify function arg for 'done' callback

    ajax = $.ajax({
        url: '../prueba.php',
        type: 'POST',
        data: {funcion: "cargarBarrio", obj_json: ""}
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            barrio = response.data;

            if ($.cookie("userAcudiente") != null) {
                var data_user = JSON.parse($.cookie("userAcudiente"));
                cargarBarrio(data_user["id_localidad"]);
            }
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de listar los barrios - intente mas tarde ");
        $('#alert_error').show();
    });
}


function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function selectItem(id_select, item) {
    $(id_select + " option").each(function () {
        if ($(this).text() == item)
            $(this).attr("selected", "selected");
    });

}

function cargarLocalidad() {
    $.each(localidad, function (k, v) {
        $("#txt_localidad").append("<option value=\"" + v.id_localidad + "\">" + v.nombre_localidad + "</option>");
    });

    if ($.cookie("userAcudiente") != null) {
        var data_user = JSON.parse($.cookie("userAcudiente"));
        selectItem("#txt_localidad", data_user["id_barrio"]["id_localidad"])
    }
}


function cargarBarrio(item_localidad) {
    $("#txt_barrio").find('option').remove();

    if (barrio.length > 0) {
        $('#txt_barrio').prop('disabled', false);
        $.each(barrio, function (k, v) {
            if (v.id_localidad === item_localidad)
                $("#txt_barrio").append("<option value=\"" + v.id_barrio + "\">" + v.nombre_barrio + "</option>");
        })

        if ($.cookie("userAcudiente") != null) {
            var data_user = JSON.parse($.cookie("userAcudiente"));
            selectItem("#txt_barrio", data_user["id_barrio"])
        }
    }
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

$(document).ready(function () {
    var data_user, barrio, localidad, actualizar;

    $(".js-example-basic-single").select2();

    $('#txt_barrio').prop('disabled', true);
    $('#alert_warning').hide();
    $('#alert_error').hide();
    $('#alert_success').hide();

    cargarDataLocalidad();
    cargarDataBarrio();
    cargarTipoDocumento();

    if ($.cookie("userAcudiente") != null) {
        actualizar = true;
        data_user = JSON.parse($.cookie("userAcudiente"));

        $('#txt_documento').prop('disabled', true);
        $('#titulo_modulo').html('Actualizacion de Datos');
        $('#txt_documento').val(vacios(data_user["documento"]));
        $('#txt_nombre').val(vacios(data_user["nombres"]));
        $('#txt_apellidos').val(vacios(data_user["apellidos"]));
        $('#txt_direccion').val(vacios(data_user["direccion"]));
        $('#txt_telefono').val(vacios(data_user["telefono"]));
        $('#txt_barrio').val(vacios(data_user["id_barrio"]));
        $('#txt_localidad').val(vacios(data_user["id_barrio"]["id_localidad"]));
        $('#txt_email').val(vacios(data_user["email"]));
        $('#txt_fecha_nacimiento').val(vacios(formatDate(data_user["fecha_nacimiento"])));

        $("#btnGuardarData").html('Actualizar');
        $("#contUserRegister").hide();
    } else {
        actualizar = false;
        $('#titulo_modulo').html("Registro Acudiente");
        $("#btnGuardarData").html('Guardar');

        $('#txt_fecha_nacimiento').val(formatDate(""));
        $("#contUserRegister").show();
    }

    $("#txt_estado_civil").change(function () {
        validarTitulosEstados($(this).val());
    });

    $("#txt_localidad").change(function () {
        cargarBarrio(this.value)
    });


    $('#btnGuardarData').click(function (event) {

        if ($('#txt_documento').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el documento ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_nombre').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el nombre ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_apellidos').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el apellido ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_direccion').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el direccion ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txt_telefono').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el telefono ");
            $('#alert_warning').show();
            return;
        }

        data_user = {
            id_tipo_documento: vacios($('#txt_tipo_documento option:selected').val()),
            documento: vacios($('#txt_documento').val()),
            nombres: vacios($('#txt_nombre').val()),
            apellidos: vacios($('#txt_apellidos').val()),
            direccion: vacios($('#txt_direccion').val()),
            telefono: vacios($('#txt_telefono').val()),
            id_barrio: vacios($('#txt_barrio option:selected').val()),
            email: vacios($('#txt_email').val()),
            fecha_nacimiento: formatDate(vacios($('#txt_fecha_nacimiento').val())),
            usuario: vacios($('#txt_usuario').val()),
            password: vacios($('#txt_password').val()),
            confirm_password: vacios($('#txt_password_confirm').val())
        };

        gestionarUsuarioAcudiente(data_user, actualizar ? "true" : "false");
    });
});


