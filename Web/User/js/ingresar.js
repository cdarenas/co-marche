
/* Obtiene la data del usuario a consultar*/
function validarUsuarioAcudiente() {
    var object, ajax;

    //Hardcoded data values for demonstration purposes
    object = [$('#txtUsuario').val(), $('#txtContrasena').val()];

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../../index.php?contentType=application/json',
        type: 'POST',
        data: JSON.stringify({serviceName: "LoginService", methodName: "autenticarUsuario", parameters: object})
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);

        if (response.success) {
            $.cookie("user", JSON.stringify(response.data));

            $('#menu_navg').show();
            $('#menu_cerrar_session').show();
            $('#menu_inicio_session').hide();
            $('#menu_registrar').hide();
            redirPage("../User/inicio.html");           
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> " + response.errorMessage + " ");
            $('#alert_error').show();
            $('#btnRegistro').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de validar los datos - intente mas tarde ");
        $('#alert_error').show();
    });
}

$(document).ready(function () {
    $('#alert_warning').hide();
    $('#alert_error').hide();

    $('#btnConsultar').click(function (event) {
        if ($('#txtUsuario').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar el Usuario ");
            $('#alert_warning').show();
            return;
        }

        if ($('#txtContrasena').val() == "") {
            $('#msn_alert_warning').empty().append("<strong>Advertencia!</strong> Falta digitar la Contraseña ");
            $('#alert_warning').show();
            return;
        }
        validarUsuarioAcudiente();
    });
});


