
function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function () {
    var data_cupo;

    if ($.cookie("userCupo") != null) {
        data_cupo = JSON.parse($.cookie("userCupo"));

        $('#titulo_modulo').html('Informacion Del Cupo');
        
        $('#lblNroCupo').html(data_cupo["nro_cupo"]);
        $('#lblDocumento').html(data_cupo["documento"]);
        $('#lblNombres').html(data_cupo["nombres"]);
        $('#lblApellidos').html(data_cupo["apellidos"]);
        $('#lblFechaSolicitud').html(formatDate(data_cupo["fecha_solicitud"]));
        $('#lblNivel').html(data_cupo["nivel"]);
        $('#lblJornada').html(data_cupo["jornada"]);
        $('#lblEstado').html(data_cupo["estado"]);
        $('#lblObervaciones').html(data_cupo["observacion"]);
    } 
});

