
function buscarUsuario() {
    var object, ajax;


    //Hardcoded data values for demonstration purposes
    object = JSON.stringify({usuario: $.cookie("user").cedula});

    //Pass the values to the AJAX request and specify function arg for 'done' callback
    ajax = $.ajax({
        url: '../prueba.php',
        type: 'POST',
        data: {funcion: "buscarUsuario", obj_json: object}
    });
    ajax.done(function (response) {
        var response = JSON.parse(response);
            console.log(response);
            
        if (response.success) {
            var data_usuario = response.data["0"];
         
            $('#titulo_modulo').html('Informacion Del Usuario');

            $('#lblTipoDocumento').html(formatDate(data_usuario["id_tipo_documento"]["nombre_tipo_documento"]));
            $('#lblCedula').html(data_usuario["cedula"]);
            $('#lblNombres').html(data_usuario["nombres"]);
            $('#lblApellidos').html(data_usuario["apellidos"]);
            $('#lblTelefono').html(data_usuario["telefono"]);
            $('#lblDireccion').html(data_usuario["direccion"]);
            $('#lblEmail').html(data_usuario["email"]);
            $('#lblFechaNacimiento').html(formatDate(data_usuario["fecha_nacimiento"]));
            $('#lblBarrio').html(formatDate(data_usuario["id_barrio"]["nombre_barrio"]));
        } else {
            $('#msn_alert_error').empty().append("<strong>Error!</strong> " + response.errorMessage + " ");
            $('#alert_error').show();
            $('#btnRegistro').show();
        }
    });
    ajax.fail(function (jqXHR, textStatus, errorThrown) {
        $('#msn_alert_error').empty().append("<strong>Error!</strong> Ocurrio un error al momento de validar los datos - intente mas tarde ");
        $('#alert_error').show();
    });
}

function formatDate(date) {

    if (vacios(date) === "") {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        return now.getFullYear() + "-" + (month) + "-" + (day);
    }

    if (date.indexOf("/") > -1) {
        var split = date.split("/");
        return split[2] + "-" + split[1] + "-" + split[0];
    } else
        return date;
}

function vacios(value) {
    return value == undefined ? "" : value.trim();
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function () {
    if ($.cookie("user") != null) {
        buscarUsuario();
    }
});

