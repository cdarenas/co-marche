<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Datasource {

    var $dbLink;

    /**
     * Constructor. Call this once when initializing system core.
     * Then save the instance of this class in $connection variable 
     * and pass it as an argument when using services from core.
     */
    function Datasource() {

        // Change this line to reflect whatever Database system you are using:
        $this->dbLink = mysqli_connect('localhost', 'root', '', 'secretaria_educacion'); //mysql_connect("localhost", "root", "");
        // Change this line to reflect whatever Database system you are using:
        //exit(var_dump($this->dbLink));//mysql_connect("mysql11.000webhost.com","a9505413_newsapp","cda7124032") or die(mysql_error)
    }

    /**
     * Function to execute SQL-commands. Use this thin wrapper to avoid 
     * MySQL dependency in application code.
     */
    function execute($sql) {

        // Change this line to reflect whatever Database system you are using:
        $result = mysqli_query($this->dbLink, $sql); //mysql_query($sql, $this->dbLink);
        $this->checkErrors($sql);

        return $result;
    }

    /**
     * Function to "blindly" execute SQL-commands. This will not put up 
     * any notifications if SQL fails, so make sure this is not used for 
     * normal operations.
     */
    function executeBlind($sql) {

        // Change this line to reflect whatever Database system you are using:
        $result = mysqli_query($this->dbLink, $sql);
        return $result;
    }

    /**
     * Function to iterate trough the resultset. Use this thin wrapper to 
     * avoid MySQL dependency in application code.
     */
    function nextRow($result) {

        // Change this line to reflect whatever Database system you are using:
        $row = mysqli_fetch_array($result);

        return $row;
    }

    /**
     * Check if sql-queries triggered errors. This will be called after an 
     * execute-command. Function requires attempted SQL string as parameter 
     * since it can be logged to application spesific log if errors occurred.
     * This whole method depends heavily from selected Database-system. Make
     * sure you change this method when using some other than MySQL database.
     */
    function checkErrors($sql) {

        //global $systemLog;
        // Only thing that we need todo is define some variables
        // And ask from RDBMS, if there was some sort of errors.
        $err = mysqli_error($this->dbLink);
        $errno = mysqli_errno($this->dbLink);

        if ($errno) {
            // SQL Error occurred. This is FATAL error. Error message and 
            // SQL command will be logged and aplication will teminate immediately.
            $message = "The following SQL command " . $sql . " caused Database error: " . $err . ".";

            //$message = addslashes("SQL-command: ".$sql." error-message: ".$message);
            //$systemLog->writeSystemSqlError ("SQL Error occurred", $errno, $message);

            print "Unrecowerable error has occurred. All data will be logged.";
            print "Please contact System Administrator for help! \n";
            print "<!-- " . $message . " -->\n";
            exit;
        } else {
            // Since there was no error, we can safely return to main program.
            return;
        }
    }

}

?>